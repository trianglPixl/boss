#ifndef __GameHeaders_H__
#define __GameHeaders_H__

#include "../../Libraries/lodepng/lodepng.h"
#include "../../Libraries/Box2D/Box2D/Box2D/Box2D.h"
#include "../../Libraries/cJSON/cJSON.h"
#include "../../Libraries/Bullet/src/btBulletCollisionCommon.h"
#include "../../Libraries/Bullet/src/btBulletDynamicsCommon.h"

#pragma warning(push)
#pragma warning(disable : 4305)
#include "../../Libraries/Bullet/src/btBulletDynamicsCommon.h"
#include "../../Libraries/Bullet/src/btBulletCollisionCommon.h"
#pragma warning(pop)

#include "Core/CoreHeaders.h"

#include "Game/Events/Event.h"
#include "Game/Events/InputEvent.h"

#include "Game/Mesh/VertexFormat.h"
#include "Game/Mesh/Mesh.h"

#include "Game/GameObjects/Light.h"

#include "Game/Physics/BulletWorld/BulletWorld.h"

#include "Game/GameObjects/PlayerController.h"
#include "Game/GameObjects/GameObject.h"
#include "Game/GameObjects/CameraObject.h"
#include "Game/GameObjects/WavePlane.h"
#include "Game/GameObjects/SkyBoxObject.h"
#include "Game/GameObjects/RibbonObject.h"
#include "Game/GameObjects/BoltObject.h"

#include "Game/Text/TextRender.h"

#include "Game/Base/ResourceManager.h"
#include "Game/Base/Scene.h"
#include "Game/Base/SceneManager.h"
#include "Game/Base/ObjectPooling.h"
#include "Game/SoundManager.h"

#include "Game/Menu/HighScore.h"

#include "Game\GameObjects\Collectible.h"
#include "Game/Menu/MenuObject.h"
#include "Game/Menu/MenuSlate.h"
#include "Game\GameObjects\NumberDisplay.h"
#include "Game/GameObjects/GameObjectPool.h"
#include "Game\Menu\MenuButton.h"
#include "Game/Menu/PlayButton.h"
#include "Game/Menu/QuitButton.h"
#include "Game/Menu/ResumeButton.h"
#include "Game\Menu\ReturnMenuButton.h"
#include "Game/Menu/RestartButton.h"

#include "Game/Physics/BoxWorld/ContactListener.h"
#include "Game/Physics/BoxWorld/PhysicsWorld.h"
#include "Game/Physics/BoxWorld/DebugDraw.h"

#include "BulletCollision/CollisionDispatch/btGhostObject.h"
#include "BulletDynamics/Character/btKinematicCharacterController.h"

#include "Game/GameObjects/BulletPlayer.h"

#include "Game/Menu/BaseMenuScene.h"

#include "Game/GameCore.h"
#include "Game/LightingScene.h"
#include "Game/PauseScene.h"
#include "Game/GameOverScene.h"
#include "Game/PlayerObject.h"
#include "Game/BossObject.h"

#endif //__GameHeaders_H__
