#ifndef __CoreHeaders_H__
#define __CoreHeaders_H__

//#include "Utility/MyMemory.h"
#include "Utility/Helpers.h"
#include "Utility/ShaderProgram.h"

#include "Math/MathHelpers.h"
#include "Math/Vector.h"
#include "Math/MyQuaternion.h"
#include "Math/MyMatrix.h"

#include "Utility/VectorShim.h"
#include "Utility/FBODefinition.h"
#include "Utility/TweenFuncs.h"

typedef MyMatrix mat4;
typedef Vector2 vec2;
typedef Vector3 vec3;
typedef Vector4 vec4;
typedef Vector2Int ivec2;
typedef Vector3Int ivec3;
typedef Vector4Int ivec4;

#endif //__CoreHeaders_H__
