#pragma once

#ifndef __VectorShim_H__
#define __VectorShim_H__

namespace vs
{
    template <typename v1, typename v2>
    v1 v2v2(const v2& source)
    {
        return v1(source.x, source.y);
    }

    template <typename v1, typename v2>
    v1 v3v3(const v2& source)
    {
        return v1(source.x, source.y, source.z);
    }

	template <typename T>
	T v3v3(const btVector3& source)
	{
		return T(source.x(), source.y(), source.z());
	}

    template <typename v1, typename v2>
    v1 v2v3(const v2& source)
    {
        return v1(source.x, source.y, 0.0f);
    }

    template <typename v1, typename v2>
    v1 v3v2(const v2& source)
    {
        return v1(source.x, source.y);
    }
}

#endif