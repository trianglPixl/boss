#include "pch.h"
#include "ShaderProgram.h"

ShaderProgram::ShaderProgram()
{
    m_VertShaderString = "";
    m_FragShaderString = "";

    m_VertShader = 0;
    m_FragShader = 0;
    m_Program = 0;
}

ShaderProgram::ShaderProgram(const char* vertfilename, const char* fragfilename)
{
    m_VertShaderString = "";
    m_FragShaderString = "";

    m_VertShader = 0;
    m_FragShader = 0;
    m_Program = 0;

    Init( vertfilename, fragfilename);
}

ShaderProgram::~ShaderProgram()
{
    Cleanup();
}

void ShaderProgram::Cleanup()
{
    /*if( m_VertShaderString )
        delete[] m_VertShaderString;
    if( m_FragShaderString )
        delete[] m_FragShaderString;*/

    glDetachShader( m_Program, m_VertShader );
    glDetachShader( m_Program, m_FragShader );

    if( m_VertShader )
        glDeleteShader( m_VertShader );
    if( m_FragShader )
        glDeleteShader( m_FragShader );
    if( m_Program )
        glDeleteProgram( m_Program );

    m_VertShaderString = "";
    m_FragShaderString = "";

    m_VertShader = 0;
    m_FragShader = 0;
    m_Program = 0;
}

GLuint ShaderProgram::CompileShader(GLenum shaderType, const char* shaderstring)
{
    GLuint shaderhandle = glCreateShader( shaderType );

    const char* strings[] = { shaderstring };
    glShaderSource( shaderhandle, 1, strings, 0 );

    glCompileShader( shaderhandle );

    //GLenum errorcode = glGetError();

    int compiled = 0;
    glGetShaderiv( shaderhandle, GL_COMPILE_STATUS, &compiled );
    if( compiled == 0 )
    {
        int infolen = 0;
        glGetShaderiv( shaderhandle, GL_INFO_LOG_LENGTH, &infolen );

        char* infobuffer = new char[infolen+1];
        glGetShaderInfoLog( shaderhandle, infolen+1, 0, infobuffer );
        OutputMessage( infobuffer );
        assert( false );
        delete infobuffer;

        glDeleteShader( shaderhandle );
        return 0;
    }

    return shaderhandle;
}

bool ShaderProgram::Init(const char* vertfilename, const char* fragfilename)
{
    m_VertShaderString = LoadCompleteFile( vertfilename, 0 );
    m_FragShaderString = LoadCompleteFile( fragfilename, 0 );

    assert( m_VertShaderString.size() != 0 && m_FragShaderString.size() != 0 );
    if( m_VertShaderString.size() == 0 || m_FragShaderString.size() == 0 )
        return false;

	//LoadDependencies
	LoadDependencies(m_VertShaderString);
	LoadDependencies(m_FragShaderString);

    m_VertShader = CompileShader( GL_VERTEX_SHADER, m_VertShaderString.data() );
    m_FragShader = CompileShader( GL_FRAGMENT_SHADER, m_FragShaderString.data() );

    if( m_VertShader == 0 || m_FragShader == 0 )
    {
        Cleanup();
        return false;
    }

    m_Program = glCreateProgram();
    glAttachShader( m_Program, m_VertShader );
    glAttachShader( m_Program, m_FragShader );

    glLinkProgram( m_Program );

    int linked = 0;
    glGetProgramiv( m_Program, GL_LINK_STATUS, &linked );
	int infolen = 0;
	glGetProgramiv(m_Program, GL_INFO_LOG_LENGTH, &infolen);

	char* infobuffer = (char*)malloc(infolen + 1);
	glGetProgramInfoLog(m_Program, infolen + 1, 0, infobuffer);
	OutputMessage(infobuffer);
	

    if( linked == 0 )
    {
		free(infobuffer);
        assert( false );

        Cleanup();
        return false;
    }

	free(infobuffer);
    return true;
}

void ShaderProgram::LoadDependencies(std::string & given)
{
	given = "#version 450\n" + given;
	size_t includePos = 0;
	size_t startLinePos = 0;
	size_t endLinePos = 0;

	includePos = given.find("#include ", includePos);

	while(includePos != std::string::npos)
	{
		//Possible that there could be more than one space between the end of include and the start of the definition
		startLinePos = given.find( "\"", includePos + 8 );
		endLinePos   = given.find( "\"", startLinePos + 1 );

		//We now have the full string
		if (startLinePos != std::string::npos && endLinePos != std::string::npos)
		{
			std::string substring = given.substr(startLinePos + 1, endLinePos - startLinePos - 1);

			//Load shader function
			substring = LoadCompleteFile(substring.data(), 0);

			given.replace(includePos, endLinePos - includePos + 1, substring);
		}

		else
		{
			//Failed to find file path declaration
			// IE: "Data/Shaders/Name.vert" was not found next to #include in loaded file
			assert(false);
		}

		includePos = given.find("#include ", includePos - 1);
	}

}
