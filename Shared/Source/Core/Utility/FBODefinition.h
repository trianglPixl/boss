//
// Copyright (c) 2012-2016 Jimmy Lord http://www.flatheadgames.com
//
// This software is provided 'as-is', without any express or implied warranty.  In no event will the authors be held liable for any damages arising from the use of this software.
// Permission is granted to anyone to use this software for any purpose, including commercial applications, and to alter it and redistribute it freely, subject to the following restrictions:
// 1. The origin of this software must not be misrepresented; you must not claim that you wrote the original software. If you use this software in a product, an acknowledgment in the product documentation would be appreciated but is not required.
// 2. Altered source versions must be plainly marked as such, and must not be misrepresented as being the original software.
// 3. This notice may not be removed or altered from any source distribution.

#ifndef __FBODefinition_H__
#define __FBODefinition_H__

class FBODefinition
{
protected:
    bool m_HasValidResources;
    bool m_FullyLoaded;

    bool m_FailedToInit;
    bool m_OnlyFreeOnShutdown;

    GLuint m_ColorTextureID;
    GLuint m_DepthTextureID;
    GLuint m_FrameBufferID;

    unsigned int m_Width; // size requested, mainly used by glViewport call.
    unsigned int m_Height;

    unsigned int m_TextureWidth; // generally will be power of 2 bigger than requested width/height
    unsigned int m_TextureHeight;

    int m_MinFilter;
    int m_MagFilter;

    bool m_NeedColorTexture;
    int m_DepthBits;
    bool m_DepthIsTexture;
	bool m_IsCubeMap;

    int m_LastFrameBufferID;

public:
    FBODefinition();
    virtual ~FBODefinition();

    bool IsFullyLoaded() { return m_FullyLoaded; }

    // returns true if a new texture needs to be created.
    bool Setup(unsigned int width, unsigned int height, int minfilter, int magfilter, bool needcolor, int depthbits, bool depthreadable, bool cubemap);

	void Resize(unsigned int width, unsigned int height);

    void Bind();
    void Unbind();

	//Passed var will be added to GL_TEXTURE_CUBE_MAP_POSITIVE_X 
	//Must have cubemaps enabled
	void SetCubeTexture(int face, Vector4 clearColor = Vector4(0.0f, 0.0f, 0.0f, 1.0f));
	void ClearCube(Vector4 color, GLbitfield clearBits = GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    void Invalidate(bool cleanglallocs);

    bool Create();

    GLuint GetColorTextureHandle() { return m_ColorTextureID; }
    GLuint GetDepthTextureHandle() { return m_DepthTextureID; }
};

#endif //__FBODefinition_H__
