#include "pch.h"

ReturnMenuButton::ReturnMenuButton(vec3 pos, float rot, float scale, CameraObject * cam,
	std::string scene, const char * texture, const char * vert, const char * frag,
	PlayerControllerButtons button) :
	MenuButton(pos, rot, scale, cam, scene,
		texture,
		vert,
		frag, button)
{

}

ReturnMenuButton::~ReturnMenuButton()
{

}

void ReturnMenuButton::Press()
{
	SceneManager::GetInstance()->PopStack();
	SceneManager::GetInstance()->PopStack();
}
