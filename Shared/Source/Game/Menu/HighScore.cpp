#include "pch.h"
#include "HighScore.h"

//Score is time elapsed. So lower times are better.
HighScore::HighScore()
{
	for (int i = 0; i < 5; i++)
	{
		Players[i].name = "....";
		Players[i].score = 9999.0f;
	}
}

HighScore::~HighScore()
{
}

void HighScore::LoadScores(std::string file)
{
	FILE* filehandle;
	errno_t error = fopen_s(&filehandle, file.c_str(), "rb");
	if (filehandle)
	{
		fseek(filehandle, 0, SEEK_END);
		long size = ftell(filehandle);
		rewind(filehandle);

		char * buffer = new char[size];
		fread(buffer, size, 1, filehandle);

		//Parse with cJson
		cJSON * root;

		root = cJSON_Parse(buffer);

		cJSON * player;
		std::string name = "Player ";

		//Read Players
		for (int i = 0; i < 5; i++)
		{
			player = cJSON_GetObjectItem(root, (name + std::to_string(i)).c_str());

			cJSON * rName = cJSON_GetObjectItem(player, "Name");
			cJSON * rScore = cJSON_GetObjectItem(player, "Score");

			Players[i].name = rName->valuestring;
			Players[i].score = rScore->valuedouble;
		}

		delete [] buffer;
		cJSON_Delete(root);

		fclose(filehandle);
	}
}

void HighScore::SaveScores(std::string file)
{
	FILE * filehandle;
	errno_t error = fopen_s(&filehandle, file.c_str(), "wb");
	if (filehandle)
	{
		cJSON * root;

		root = cJSON_CreateObject();

		cJSON * player;
		std::string name = "Player ";

		//Write Players
		for (int i = 0; i < 5; i++)
		{
			cJSON_AddItemToObject(root, (name + std::to_string(i)).c_str(), player = cJSON_CreateObject());
			cJSON_AddStringToObject(player, "Name", Players[i].name.c_str());
			cJSON_AddNumberToObject(player, "Score", Players[i].score);
		}

		char * data = cJSON_Print(root);

		fprintf_s(filehandle, data);

		free(data);

		cJSON_Delete(root);

		fclose(filehandle);
	}
}
