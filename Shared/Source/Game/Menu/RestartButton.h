#pragma once

class RestartButton : public MenuButton
{

public:
	RestartButton(vec3 pos, float rot, float scale,
		CameraObject * cam, std::string scene, 
		const char * texture, const char * vert, 
		const char * frag, PlayerControllerButtons button);

	~RestartButton();

	virtual void Press() override;
};