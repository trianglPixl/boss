#pragma once

class PlayButton : public MenuButton
{

public:
	PlayButton(vec3 pos, float rot, float scale,
		CameraObject * cam, std::string scene, 
		const char * texture, const char * vert, 
		const char * frag, PlayerControllerButtons button);

	~PlayButton();

	virtual void Press() override;
};