#include "pch.h"
#include "PlayButton.h"

PlayButton::PlayButton(vec3 pos, float rot, float scale, CameraObject * cam,
	std::string scene, const char * texture, const char * vert, const char * frag,
	PlayerControllerButtons button) :
	MenuButton(pos, rot, scale, cam, scene,
		texture,
		vert,
		frag, button)
{

}

PlayButton::~PlayButton()
{
}

void PlayButton::Press()
{
	SceneManager::GetInstance()->PushSceneToStack(m_sceneName);
	SceneManager::GetInstance()->RestartScene(m_sceneName);
}
