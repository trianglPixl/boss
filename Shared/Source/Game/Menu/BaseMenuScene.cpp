#include "pch.h"

BaseMenuScene::BaseMenuScene(std::string name, GameCore* pGame, ResourceManager* pResources)
	: Scene(name, pGame, pResources)
{
}

BaseMenuScene::~BaseMenuScene()
{
}

void BaseMenuScene::OnSurfaceChanged(unsigned int width, unsigned int height)
{
	Scene::OnSurfaceChanged(width, height);
}

void BaseMenuScene::LoadContent()
{
	Scene::LoadContent();
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LESS);

}

void BaseMenuScene::Update(float deltatime)
{
	Scene::Update(deltatime);

	if (m_pGame->GetController(1)->IsButtonJustPressed(PCB_Up))
	{
		m_currentButton++;
		if (m_currentButton > m_numButtons)
			m_currentButton = 1;
	}

	if (m_pGame->GetController(1)->IsButtonJustPressed(PCB_Down))
	{
		m_currentButton--;
		if (m_currentButton < 1)
			m_currentButton = m_numButtons;
	}
}

void BaseMenuScene::Draw()
{
	Scene::Draw();
}