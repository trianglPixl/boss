#pragma once

class ResumeButton : public MenuButton
{

public:
	ResumeButton(vec3 pos, float rot, float scale,
		CameraObject * cam, std::string scene, 
		const char * texture, const char * vert, 
		const char * frag, PlayerControllerButtons button);

	~ResumeButton();

	virtual void Press() override;
};