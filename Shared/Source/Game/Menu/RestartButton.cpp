#include "pch.h"

RestartButton::RestartButton(vec3 pos, float rot, float scale, CameraObject * cam,
	std::string scene, const char * texture, const char * vert, const char * frag,
	PlayerControllerButtons button) :
	MenuButton(pos, rot, scale, cam, scene,
		texture,
		vert,
		frag, button)
{

}

RestartButton::~RestartButton()
{

}

void RestartButton::Press()
{
	SceneManager::GetInstance()->PopStack();
	SceneManager::GetInstance()->RestartScene(m_sceneName);
}
