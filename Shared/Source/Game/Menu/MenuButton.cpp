#include "pch.h"
#include "MenuButton.h"

MenuButton::MenuButton(vec3 pos, float rot, float scale, CameraObject * cam,
	std::string scene, const char * texture, const char * vert, const char * frag,
	PlayerControllerButtons button) :
	MenuObject(pos, rot, scale, cam,
		texture,
		vert,
		frag),
	m_sceneName(scene),
	m_button(button)
{
	m_vRenderVertices.push_back(VertexFormat(	0.0f, 0.0f, 0.0f,
												255,255,255,255,
												0, 0,
												0 , 0, -1));

	m_vRenderVertices.push_back(VertexFormat(	0.0f, 1.0f, 0.0f,
												255, 255, 255, 255,
												0, 1,
												0, 0, -1));

	m_vRenderVertices.push_back(VertexFormat(	m_buttonWidth, 1.0f, 0.0f,
												255, 255, 255, 255,
												1, 1,
												0, 0, -1));

	m_vRenderVertices.push_back(VertexFormat(	m_buttonWidth, 0.0f, 0.0f,
												255, 255, 255, 255,
												1, 0,
												0, 0, -1));

	m_vIndeces.push_back(0);
	m_vIndeces.push_back(1);
	m_vIndeces.push_back(2);
	m_vIndeces.push_back(2);
	m_vIndeces.push_back(3);
	m_vIndeces.push_back(0);

	//Build VBO and IBO
	glBindBuffer(GL_ARRAY_BUFFER, m_VBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(VertexFormat) * m_vRenderVertices.size(), m_vRenderVertices.data(), GL_STATIC_DRAW);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_IBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(unsigned int) * m_vIndeces.size(), m_vIndeces.data(), GL_STATIC_DRAW);

	CheckForGLErrors();
}

MenuButton::~MenuButton()
{
}

void MenuButton::UseText(vec3 pos, int pointSize, float resolution, std::string text, vec3 textColor)
{
	m_text.reset(new TextRender("Data/TypeFaces/Amble-Regular.ttf", pointSize, resolution, resolution, m_buttonWidth * m_scale, 1.0f * m_scale, TextRender::AP_BOTTOMLEFT, m_pCamera,
		"Data/Shaders/Text.vert",
		"Data/Shaders/Text.frag"));
	m_text->SetTextColor(textColor);
	m_text->SetAttachedToCamera(false);
	m_text->ChangeString(text);
	m_text->SetTranslation(pos, vec3(0));
}

void MenuButton::ChangeText(std::string newText)
{
	if (m_text != nullptr)
		m_text->ChangeString(newText);
}

void MenuButton::SetTextPos(vec3 pos)
{
	if (m_text != nullptr)
		m_text->SetTranslation(pos, vec3(0));
}

void MenuButton::SetTextColor(vec3 color)
{
	if (m_text != nullptr)
		m_text->SetTextColor(color);
}

void MenuButton::Update(float delta)
{
	MenuObject::Update(delta);
	if (m_pController == nullptr)
		return;

	if (m_pController->IsButtonPressed(m_button))
	{
		Press();
	}
}

void MenuButton::Press()
{
	
}

void MenuButton::Draw()
{
	MenuObject::Draw();
	m_text->Draw();
}
