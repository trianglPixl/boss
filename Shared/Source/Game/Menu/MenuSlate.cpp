#include "pch.h"

MenuSlate::MenuSlate(vec3 pos, float rot, float scale,
	CameraObject * cam, float width, float height, const char * texture,
	const char * vert, const char * frag) :
	MenuObject(pos, rot, scale, cam,
		texture,
		vert,
		frag)
{
	m_vRenderVertices.push_back(VertexFormat(	0.0f, 0.0f, 0.0f,
												255,255,255,255,
												0, 0,
												0 , 0, -1));

	m_vRenderVertices.push_back(VertexFormat(	0.0f, height, 0.0f,
												255, 255, 255, 255,
												0, 1,
												0, 0, -1));

	m_vRenderVertices.push_back(VertexFormat(	width, height, 0.0f,
												255, 255, 255, 255,
												1, 1,
												0, 0, -1));

	m_vRenderVertices.push_back(VertexFormat(   width, 0.0f, 0.0f,
												255, 255, 255, 255,
												1, 0,
												0, 0, -1));

	m_vIndeces.push_back(0);
	m_vIndeces.push_back(1);
	m_vIndeces.push_back(2);
	m_vIndeces.push_back(2);
	m_vIndeces.push_back(3);
	m_vIndeces.push_back(0);

	//Build VBO and IBO
	glBindBuffer(GL_ARRAY_BUFFER, m_VBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(VertexFormat) * m_vRenderVertices.size(), m_vRenderVertices.data(), GL_STATIC_DRAW);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_IBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(unsigned int) * m_vIndeces.size(), m_vIndeces.data(), GL_STATIC_DRAW);

	CheckForGLErrors();
}

MenuSlate::~MenuSlate()
{
}