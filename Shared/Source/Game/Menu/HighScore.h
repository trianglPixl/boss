#pragma once

class HighScore
{
public:
	struct PlayerInfo
	{
		std::string name;
		float score;
	};

	HighScore();
	~HighScore();

	PlayerInfo Players[5];

	void LoadScores(std::string file);
	void SaveScores(std::string file);
};