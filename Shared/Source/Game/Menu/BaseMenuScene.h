#pragma once
class GameCore;

class BaseMenuScene : public Scene
{
protected:
	int m_numButtons = 0;
	int m_currentButton = 0;

public:
	BaseMenuScene(std::string name, GameCore* pGame, ResourceManager* pResources);
    virtual ~BaseMenuScene();

    virtual void OnSurfaceChanged(unsigned int width, unsigned int height) override;
    virtual void LoadContent()				override;

    virtual void Update(float deltatime)	override;
    virtual void Draw()						override;
};