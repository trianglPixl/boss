#pragma once

class QuitButton : public MenuButton
{

public:
	QuitButton(vec3 pos, float rot, float scale,
		CameraObject * cam, std::string scene, 
		const char * texture, const char * vert, 
		const char * frag, PlayerControllerButtons button);

	~QuitButton();

	virtual void Press() override;
};