#pragma once

class MenuSlate : public MenuObject
{

public:
	MenuSlate( vec3 pos, float rot, float scale,
		CameraObject * cam, float width, float height, const char * texture,
		const char * vert, const char * frag);

	~MenuSlate();
};