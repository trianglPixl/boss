#pragma once
#ifndef __MENUBUTTON_H
#define __MENUBUTTON_H

class MenuButton : public MenuObject
{
protected:
	const float m_buttonWidth = 2.65f;

	PlayerControllerButtons m_button;

	bool m_isPressed = false;
	PlayerController * m_pController = nullptr;

	std::string m_sceneName;

	std::unique_ptr<TextRender> m_text;

public:
	MenuButton(vec3 pos, float rot, float scale,
		CameraObject * cam, std::string scene, 
		const char * texture, const char * vert, 
		const char * frag, PlayerControllerButtons button);

	~MenuButton();

	void UseText(vec3 pos, int pointSize, float resolution, std::string text, vec3 textColor);

	void ChangeText(std::string newText);

	void SetTextPos(vec3 pos);

	void SetTextColor(vec3 color);

	virtual void Update(float delta) override;

	void SetController(PlayerController * con) { m_pController = con; };

	void SetActivateButton(PlayerControllerButtons button) { m_button = button; };

	float GetButtonWidth() { return m_buttonWidth; };

	float GetScale() { return m_scale; };

	virtual void Press();

	void Draw() override;
};

#endif