#include "pch.h"

QuitButton::QuitButton(vec3 pos, float rot, float scale, CameraObject * cam,
	std::string scene, const char * texture, const char * vert, const char * frag,
	PlayerControllerButtons button) :
	MenuButton(pos, rot, scale, cam, scene,
		texture,
		vert,
		frag, button)
{

}

QuitButton::~QuitButton()
{

}

void QuitButton::Press()
{
	PostQuitMessage(0);
}
