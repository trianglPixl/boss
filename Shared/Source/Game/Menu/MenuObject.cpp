#include "pch.h"
#include "MenuObject.h"

MenuObject::MenuObject(vec3 pos, float rot, float scale, CameraObject * cam, 
	const char * texture, const char * vert, const char * frag) :
	m_position(pos),
	m_rotation(rot),
	m_scale(scale),
	m_pCamera(cam)
{
	glGenBuffers(1, &m_VBO);
	glGenBuffers(1, &m_IBO);

	m_texture = LoadTexture(texture);
	m_ShaderProgram.reset(new ShaderProgram(vert, frag));
	CheckForGLErrors();
}

void MenuObject::Draw()
{
	glUseProgram(m_ShaderProgram->GetProgram());
	glDepthMask(GL_FALSE);

	if (m_VBO != 0 && m_IBO != 0)
	{
		glBindBuffer(GL_ARRAY_BUFFER, m_VBO);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_IBO);

		GLint pos = glGetAttribLocation(m_ShaderProgram->GetProgram(), "a_Position");
		if (pos != -1)
		{
			glVertexAttribPointer(pos, 3, GL_FLOAT, GL_FALSE, sizeof(VertexFormat), (void*)offsetof(VertexFormat, pos));
			glEnableVertexAttribArray(pos);
		}

		GLint aUV = glGetAttribLocation(m_ShaderProgram->GetProgram(), "a_UV");
		if (aUV != -1)
		{
			glVertexAttribPointer(aUV, 2, GL_FLOAT, GL_FALSE, sizeof(VertexFormat), (void*)offsetof(VertexFormat, uv));
			glEnableVertexAttribArray(aUV);
		}

		GLint col = glGetAttribLocation(m_ShaderProgram->GetProgram(), "a_Color");
		if (col != -1)
		{
			glVertexAttribPointer(col, 4, GL_UNSIGNED_BYTE, GL_TRUE, sizeof(VertexFormat), (void*)offsetof(VertexFormat, color));
			glEnableVertexAttribArray(col);
		}

		GLint mvp = glGetUniformLocation(m_ShaderProgram->GetProgram(), "u_MVP");
		if (mvp != -1)
		{
			mat4 matrix;
			matrix.SetIdentity();
			matrix.CreateSRT(vec3(m_scale, m_scale, 1.0f), vec3(0.0f, 0.0f, m_rotation), m_position);

			matrix = m_pCamera->GetProjMat() * matrix;

			glUniformMatrix4fv(mvp, 1, GL_FALSE, &matrix.m11);
		}

		GLint uTexture = glGetUniformLocation(m_ShaderProgram->GetProgram(), "u_Texture");
		if (uTexture != -1)
		{
			glActiveTexture(GL_TEXTURE0 + 0);
			glBindTexture(GL_TEXTURE_2D, m_texture);
			glUniform1i(uTexture, 0);
		}

		glDrawElements(GL_TRIANGLES, m_vIndeces.size(), GL_UNSIGNED_INT, 0);
	}
	glDepthMask(GL_TRUE);
	CheckForGLErrors();
}
