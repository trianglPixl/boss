#pragma once

class ReturnMenuButton : public MenuButton
{

public:
	ReturnMenuButton(vec3 pos, float rot, float scale,
		CameraObject * cam, std::string scene, 
		const char * texture, const char * vert, 
		const char * frag, PlayerControllerButtons button);

	~ReturnMenuButton();

	virtual void Press() override;
};