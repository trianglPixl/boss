#pragma once
#ifndef __MENUOBJECT_H
#define __MENUOBJECT_H

class MenuObject
{

protected:
	vec3	m_position;
	float	m_rotation;
	float	m_scale;

	GLuint								m_VBO;
	GLuint								m_IBO;

	GLuint								m_texture;
	std::unique_ptr<ShaderProgram>		m_ShaderProgram;
	std::vector<VertexFormat>			m_vRenderVertices;
	std::vector<unsigned int>			m_vIndeces;

	CameraObject * m_pCamera;
public:
	MenuObject(vec3 pos, float rot, float scale, 
		CameraObject * cam, const char * texture, 
		const char * vert, const char * frag);

	~MenuObject() {};

	virtual void Draw();
	virtual void Update(float delta) {};

	void SetPosition(vec3 pos) { m_position = pos; }
	vec3 GetPosition() { return m_position; }
};

#endif