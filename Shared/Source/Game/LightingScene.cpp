#include "pch.h"
#include "LightingScene.h"



void HalfCollisionCallback(btManifoldPoint &cp, void* body0, void* body1)
{
	btRigidBody * firstBody = (btRigidBody *)body0;
	btRigidBody * secondBody = (btRigidBody *)body1;

	GameObject * firstObj = (GameObject *)firstBody->getUserPointer();
	GameObject * secondObj = (GameObject *)secondBody->getUserPointer();

	if (firstObj != nullptr && secondObj != nullptr)
	{
		std::string firstName = firstObj->GetName();
		if (firstName.substr(0, 10) == "PlayerBolt")
		{
			((BoltObject*)firstObj)->Hit(secondBody);
		}
	}
}

bool BCollisionCallback(btManifoldPoint& cp, void* body0, void* body1)
{
	HalfCollisionCallback(cp, body0, body1);
	HalfCollisionCallback(cp, body1, body0);
	//This literally does nothing, so says the mighty bullet documentation
	return true;
}

LightingScene::LightingScene(std::string name, GameCore* pGame, ResourceManager* pResources)
: Scene(name, pGame, pResources )
{
}

LightingScene::~LightingScene()
{
}

void LightingScene::OnSurfaceChanged(unsigned int width, unsigned int height)
{
    Scene::OnSurfaceChanged( width, height );
}

void LightingScene::LoadContent()
{
    Scene::LoadContent();
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LESS);

#pragma region Materials
	Material cubeMaterial;
	cubeMaterial.ambient = vec4(1.0f, 1.0f, 1.0f, 1.0f);
	cubeMaterial.specular = vec4(1.0f, 1.0f, 1.0f, 0.2f);
	cubeMaterial.shine = 100.0f;

	Material wallMaterial;
	wallMaterial.ambient = vec4(1.0f, 1.0f, 1.0f, 1.0f);
	wallMaterial.specular = vec4(1.0f, 1.0f, 1.0f, 0.05f);
	wallMaterial.shine = 32.0f;

	Material stoneMaterial;
	stoneMaterial.ambient = vec4(1.0f, 1.0f, 1.0f, 1.0f);
	stoneMaterial.specular = vec4(1.0f, 1.0f, 1.0f, 0.01f);
	stoneMaterial.shine = 4.0f;

	Material shinyStoneMaterial;
	shinyStoneMaterial.ambient = vec4(1.0f, 1.0f, 1.0f, 0.8f);
	shinyStoneMaterial.specular = vec4(1.0f, 1.0f, 1.0f, 0.2f);
	shinyStoneMaterial.shine = 128.0f;

#pragma endregion

#pragma region Shaders
	//This shader uses functions
    m_pResources->AddShader( "Lit", new ShaderProgram( "Data/Shaders/Complex.vert", "Data/Shaders/Complex.frag" ) );

	m_pResources->AddShader( "Texture", new ShaderProgram("Data/Shaders/Texture.vert", "Data/Shaders/Texture.frag"));
	m_pResources->AddShader( "CubeShader", new ShaderProgram("Data/Shaders/CubeTexture.vert", "Data/Shaders/CubeTexture.frag"));
	m_pResources->AddShader( "DepthCube", new ShaderProgram("Data/Shaders/CubeDepth.vert", "Data/Shaders/CubeDepth.frag"));
	m_pResources->AddShader( "SkyBox", new ShaderProgram("Data/Shaders/SkyBox.vert", "Data/Shaders/SkyBox.frag" ));
	m_pResources->AddShader( "Depth", new ShaderProgram("Data/Shaders/Misc/Position.vert", "Data/Shaders/Misc/DepthColor.frag"));
    m_pResources->AddShader("BillboardRibbon", new ShaderProgram("Data/Shaders/Ribbon.vert", "Data/Shaders/Ribbon.frag"));

	// post shaders
	ShaderProgram* gammaShader = new ShaderProgram("Data/Shaders/Postprocess/TexturePassthrough.vert", "Data/Shaders/Postprocess/GammaCorrection.frag");
	m_pResources->AddShader("Gamma", gammaShader);
	m_PostprocessStack.push_back(gammaShader->GetProgram());
#pragma endregion

#pragma region Meshes
	m_pResources->AddMesh("InvertedCube", Mesh::CreateCube(vec3(100, 100, 100), true));
	m_pResources->GetMesh("InvertedCube")->SetRenderOrder(0);
	m_pResources->AddMesh("CUBE", Mesh::CreateCube(vec3(1,1,1)));

	m_pResources->AddMesh("CUBE", Mesh::CreateCube(vec3(1, 1, 1)));
	m_pResources->GetMesh("CUBE")->SetRenderOrder(1);

	m_pResources->AddMesh("PLANE", Mesh::CreatePlane(vec2(20, 20), ivec2(4, 4), vec2(0, 0), vec2(4, 4)));
	m_pResources->AddMesh("TestLevel", Mesh::LoadSDM("Data/Models/world.sdm"));
	m_pResources->AddMesh("TestLevel", Mesh::LoadSDM("Data/Models/world.sdm"));
	m_pResources->AddMesh("Pillar", Mesh::LoadSDM("Data/Models/Pillar.sdm"));
	m_pResources->AddMesh("Ring", Mesh::LoadSDM("Data/Models/PillarRing.sdm"));
	m_pResources->AddMesh("Gem", Mesh::LoadSDM("Data/Models/PillarRingGem.sdm"));
    m_pResources->AddMesh("Bolt", Mesh::LoadSDM("Data/Models/bolt.sdm"));

	m_pResources->AddMesh("Sphere", Mesh::LoadSDM("Data/Models/Sphere.sdm"));
	m_pResources->GetMesh("Sphere")->SetRenderOrder(1);
#pragma endregion
    
#pragma region Textures

	m_pResources->AddTexture("BlankNM", LoadTexture("Data/Textures/BlankNM.png"));
	m_pResources->AddTexture("BlankNM", LoadTexture("Data/Textures/BlankNM.png", true));
	m_pResources->AddTexture( "DiceDI", LoadTexture("Data/Textures/SteelCube.png"));
	m_pResources->AddTexture( "DiceNM", LoadTexture("Data/Textures/SteelCubeNMTileFix.png", true));
	m_pResources->AddTexture("WallDI", LoadTexture("Data/Textures/Tile.png"));
	m_pResources->AddTexture("WallNM", LoadTexture("Data/Textures/TileNM.png", true));
	m_pResources->AddTexture("StoneWallDI", LoadTexture("Data/Textures/StoneWall.png"));
	m_pResources->AddTexture("StoneWallNM", LoadTexture("Data/Textures/StoneWallNM.png", true));
	m_pResources->AddTexture("Flat", LoadTexture("Data/Textures/Flat.png"));

	const char * CubeNames[] = {
		"Data/Textures/CubeDebug/posx.png",
		"Data/Textures/CubeDebug/negx.png",
		"Data/Textures/CubeDebug/posy.png",
		"Data/Textures/CubeDebug/negy.png",
		"Data/Textures/CubeDebug/posz.png",
		"Data/Textures/CubeDebug/negz.png"
	};
	m_pResources->AddTexture("CubeMap", LoadTextureCubemap(CubeNames));
		
#pragma endregion

#pragma region Sounds


	m_pSoundManager->StartUp();
	m_pResources->AddSound("WeakHit", Mix_LoadWAV("Data/Sounds/WeakHit.wav"));
	m_pResources->AddSound("Hit", Mix_LoadWAV("Data/Sounds/Hit.wav"));
	m_pResources->AddSound("BossDeath", Mix_LoadWAV("Data/Sounds/Explosion.wav"));
	m_pResources->AddSound("PlayerHit", Mix_LoadWAV("Data/Sounds/PlayerHit.wav"));
	m_pResources->AddSound("PlayerDeath", Mix_LoadWAV("Data/Sounds/Explosion.wav"));

#pragma endregion

#pragma region GameObjects
        
    RibbonObject* ribbon = new RibbonObject(this, "Ribbon", 0.2f,
        m_pResources->GetShader("BillboardRibbon"), 0);
    m_pGameObjects["Ribbon"] = ribbon;
    ribbon->SetIsUsingAdditiveBlending(true);
    ribbon->SetColor(vec4(0.8f, 0.1f, 0.1f, 1.0f));

    RibbonObject* playerLaser = new RibbonObject(this, "PlayerLaser", 0.2f, 
        m_pResources->GetShader("BillboardRibbon"), 0);
    m_pGameObjects["PlayerLaser"] = playerLaser;
    playerLaser->SetIsUsingAdditiveBlending(true);
    playerLaser->SetColor(vec4(0.1f, 0.8f, 0.1f, 1.0f));

	GameObject* testLevel = new GameObject(this, "TestLevel", vec3(0.0f, 0.0f, 0.0f), vec3(0.0f, 0.0f, 0.0f), vec3(1.0f, 1.0f, 1.0f),
		m_pResources->GetMesh("TestLevel"), m_pResources->GetShader("Lit"), m_pResources->GetTexture("StoneWallDI"),
		m_pResources->GetTexture("StoneWallNM"));
	m_pGameObjects["TestLevel"] = testLevel;
	testLevel->SetMaterial(stoneMaterial);
	testLevel->CreateBGRigidBody("Data/Geometry/world.bg", BulletCollisionFilter(BCF_World, BCF_All));

	BulletPlayer* player = new BulletPlayer(this, "Player", vec3(0.0f, 0.92f, -19.f), vec3(0.0f, 0.0f, 0.0f), vec3(1.0f), 0.3f, 1.2f,
		nullptr, 0, 0, 0);
	player->SetMaterial(wallMaterial);
	player->SetController(m_pGame->GetController(0));
	m_pGameObjects["Player"] = player;


	//GameObject* pillar = new GameObject(this, "Pillar", vec3(0, 0, 0), vec3(0, 0, 0), vec3(1, 1, 1),
	//	m_pResources->GetMesh("Pillar"), m_pResources->GetShader("Lit"), m_pResources->GetTexture("StoneWallDI"),
	//	m_pResources->GetTexture("StoneWallNM"));
	//m_pGameObjects["Pillar"] = pillar;
	//pillar->SetMaterial(stoneMaterial);
 //   btCylinderShape* cylinder = new btCylinderShape(btVector3(2.0f, 12.0f, 2.0f));
 //   pillar->CreateBulletRigidBody(cylinder, 0);

	GameObject* gem = new GameObject(this, "Gem", vec3(0, 1.6f, 0), vec3(0, 0, 0), vec3(1, 1, 1),
		m_pResources->GetMesh("Gem"), m_pResources->GetShader("Lit"), m_pResources->GetTexture("StoneWallDI"),
		m_pResources->GetTexture("StoneWallNM"));
	m_pGameObjects["Gem"] = gem;
	gem->SetMaterial(stoneMaterial);

	BossObject* boss = new BossObject(this, "Boss", vec3(0, 1.6f, 0), vec3(0, 0, 0), vec3(1, 1, 1),
		m_pResources->GetMesh("Ring"), m_pResources->GetShader("Lit"), m_pResources->GetTexture("StoneWallDI"),
		m_pResources->GetTexture("StoneWallNM"));
	m_pGameObjects["Boss"] = boss;
	boss->SetMaterial(stoneMaterial);
	boss->SetDamageMaterials(stoneMaterial, shinyStoneMaterial);
	boss->SetLaserJitterTweenFunction(TweenFunc_SineEaseOut);
	boss->CreateBGRigidBody("Data/Geometry/PillarRing.bg", BulletCollisionFilter(BCF_Enemy, BCF_All));


    // camera
	{
		CameraObject* camera = new CameraObject(this, "Camera", vec3(0.0f, -0.3f, -5.0f), vec3(0.0f, 0.0f, 0.0f), 5.0f, 0, 0, 0);
		m_pGameObjects["Camera"] = camera;
		camera->SetFollowMode(CameraFollowMode::CFM_DIRFOLLOW);
		camera->SetFollowObject3D(player);
		camera->SetEyeOffset3D(vec3(0.0f, 0.7f, 0.0f));
		player->SetCamera(camera);
	}
#pragma endregion

#pragma region Lights

	// ceiling light
	m_lights[0].ambient = vec4(1.0f, 0.0f, 0.0f, 0.001f);
	m_lights[0].diffuse = vec4(1.0f, 0.0f, 0.0f, 1.0f);
	m_lights[0].specular = vec4(0.8f, 0.8f, 0.8f, 1.0f);
	m_lights[0].position = vec4(0.0f, 16.0f, 0.0, 1.0f);
	m_lights[0].direction = vec3(0, 0, 0);
	m_lights[0].angle = 45.0f;
	m_lights[0].angleCos = 0.7071067812f;
	m_lights[0].lightRadius = 0.0f;
	m_lights[0].attenuationCutoff = 6.0f;
	//m_lights[0].castsShadow = true;

	// wall lights

	const float wallLightStrength = 2.0f;
	const vec4 wallLightColor = vec4(0.2f, 0.0f, 0.8f, 1.0f);
	//const vec4 wallLightColor = vec4(0.0f, 0.0f, 0.0f, 0.0f);

#pragma region Text
		text.reset(new TextRender("Data/TypeFaces/Amble-Regular.ttf", 48, 1024, 1024, 10, 2, TextRender::AP_TOPLEFT, (CameraObject *)m_pGameObjects["Camera"], "Data/Shaders/Text.vert", "Data/Shaders/Text.frag"));
		text->ChangeString("Hello Everyone!!");
		text->SetTextColor(vec3(0,1,0));
		text->SetTranslation(vec3(-1.5,2,7), vec3(0));
        
#pragma endregion

	m_lights[1].ambient  = vec4(wallLightColor.XYZ(), 0.001f);
	m_lights[1].diffuse  = wallLightColor;
	m_lights[1].specular = vec4(0.8f, 0.8f, 0.8f, 1.0f);
	m_lights[1].position = vec4(0.0f, 4.0f, -9.0f, 1.0f);
	m_lights[1].direction = vec3(0, 0, 0);
	m_lights[1].angle = 45.0f;
	m_lights[1].angleCos = 0.7071067812f;
	m_lights[1].lightRadius = 0.0f;
	m_lights[1].attenuationCutoff = wallLightStrength;
	m_lights[1].castsShadow = true;

	m_lights[2].ambient = vec4(wallLightColor.XYZ(), 0.001f);
	m_lights[2].diffuse = wallLightColor;
	m_lights[2].specular = vec4(0.8f, 0.8f, 0.8f, 1.0f);
	m_lights[2].position = vec4(0.0f, 4.0f, 9.0f, 1.0f);
	m_lights[2].direction = vec3(0, 0, 0);
	m_lights[2].angle = 45.0f;
	m_lights[2].angleCos = 0.7071067812f;
	m_lights[2].lightRadius = 0.0f;
	m_lights[2].attenuationCutoff = wallLightStrength;
	//m_lights[2].castsShadow = true;

	m_lights[3].ambient = vec4(wallLightColor.XYZ(), 0.001f);
	m_lights[3].diffuse = wallLightColor;
	m_lights[3].specular = vec4(0.8f, 0.8f, 0.8f, 1.0f);
	m_lights[3].position = vec4(-9.0f, 4.0f, 0.0f, 1.0f);
	m_lights[3].direction = vec3(0, 0, 0);
	m_lights[3].angle = 45.0f;
	m_lights[3].angleCos = 0.7071067812f;
	m_lights[3].lightRadius = 0.0f;
	m_lights[3].attenuationCutoff = wallLightStrength;
	//m_lights[3].castsShadow = true;

	m_lights[4].ambient = vec4(wallLightColor.XYZ(), 0.001f);
	m_lights[4].diffuse = wallLightColor;
	m_lights[4].specular = vec4(0.8f, 0.8f, 0.8f, 1.0f);
	m_lights[4].position = vec4(9.0f, 4.0f, 0.0f, 1.0f);
	m_lights[4].direction = vec3(0, 0, 0);
	m_lights[4].angle = 45.0f;
	m_lights[4].angleCos = 0.7071067812f;
	m_lights[4].lightRadius = 0.0f;
	m_lights[4].attenuationCutoff = wallLightStrength;
	//m_lights[4].castsShadow = true;

	// laser light
	m_lights[5].ambient = vec4(0.8f, 0.0f, 0.0f, 0.001f);
	m_lights[5].diffuse = vec4(0.8f, 0.0f, 0.0f, 1.0f);
	m_lights[5].specular = vec4(0.8f, 0.8f, 0.8f, 1.0f);
	m_lights[5].position = vec4(-4.0f, 0.0f, -4.0f, 1.0f);
	m_lights[5].direction = vec3(0, 0, 0);
	m_lights[5].angle = 45.0f;
	m_lights[5].angleCos = 0.7071067812f;
	m_lights[5].lightRadius = 0.0f;
	m_lights[5].attenuationCutoff = 5.0f;
	m_lights[5].castsShadow = true;

	((BossObject*)m_pGameObjects["Boss"])->SetLaserLight(&m_lights[5]);

	ACTIVELIGHTS = 6;

	//Setting shadowmap render shader 
	SetRenderPointShader(m_pResources->GetShader("Depth"));

#pragma endregion

	//Set function pointer for process callback
	gContactProcessedCallback = BCollisionCallback;


	m_pResources->AddSound("whoosh", Mix_LoadWAV("Data/Sounds/whoosh.wav"));
	m_pSoundManager->PlaySound(m_pResources->GetSound("whoosh"));
}

void LightingScene::Update(float deltatime)
{
	// update physics
	//static float physicsStepTimer = 0.0f;
	//const static float physicsStepDeltaTime = 1.0f / 60.0f;
	//physicsStepTimer += deltatime;

	//while (physicsStepTimer > physicsStepDeltaTime)
	//{
	//	m_pDynamicsWorld->stepSimulation(physicsStepDeltaTime, 10, physicsStepDeltaTime);
	//	physicsStepTimer -= physicsStepDeltaTime;
	//}

	// Bullet takes care of the delta time thing
	// see https://web.archive.org/web/20160211005122/http://bulletphysics.org/mediawiki-1.5.8/index.php/Stepping_the_World
	// (the wiki is broken but the Archive.org link above should work basically forever)
	BulletWorld::GetInstance()->GetWorld()->stepSimulation(deltatime, 5);
	BulletWorld::GetInstance()->GetWorld()->getSolverInfo().m_numIterations = 20;

	
	CheckForGLErrors();
    Scene::Update( deltatime );
	RenderShadowmaps(512);

	if (m_pGame->GetController(1)->IsButtonJustPressed(PCB_ESCAPE))
	{
		//Not pausing this scene will cause this to still check an cause an overflow.
		//Also rendering issue present
		SceneManager::GetInstance()->PushSceneToStackAndSetNewParent("PauseScene", true);
	}
}

void LightingScene::Draw()
{
    Scene::Draw();
}

void LightingScene::Restart()
{
	Scene::Restart();

	GameObject* camera = m_pGameObjects["Camera"];
	if (camera != nullptr)
	{
		camera->SetRotation(vec3(0.0f));
	}

	BulletPlayer* player = (BulletPlayer*)m_pGameObjects["Player"];
	if (player != nullptr)
	{
		player->SetPosition(vec3(0.0f, 0.92f, -19.f));
		btTransform playerTransform;
		playerTransform.setIdentity();
		playerTransform.setOrigin(btVector3(0.0f, 0.92f, -19.f));
		player->MoveControllerTo(vec3(0.0f, 0.92f, -19.f));
		player->SetHealth(100.0f);
		GameObjectPool* playerBolts = player->GetBolts();
		playerBolts->ClearActivePool();
	}

	BossObject* boss = (BossObject*)m_pGameObjects["Boss"];
	if (boss != nullptr)
	{
		boss->SetRotation(vec3(0.0f));
		boss->SetHealth(100.0f);
	}
}
void LightingScene::OnEvent(Event* pEvent)
{
	if (pEvent->GetEventType() == EventType_Input)
	{
		InputEvent* pInput = (InputEvent*)pEvent;
		switch (pInput->GetInputDeviceType())
		{
		case InputDeviceType_Mouse:
			if (pInput->GetInputState() == InputState_Moved)
			{
				float sensitivity = 0.075f;
				CameraObject* camera = (CameraObject*)m_pGameObjects["Camera"];
				vec3 newRotation = camera->GetRotation();
				newRotation += vec3(-pInput->GetPosition().y, -pInput->GetPosition().x) * sensitivity;
				if (newRotation.x >= 90.0f)
				{
					newRotation.x = 89.9f;
				}
				else if (newRotation.x < -90.0f)
				{
					newRotation.x = -89.9f;
				}
				camera->SetRotation(newRotation);
			}
			break;
		}
	}
}

void LightingScene::SetupCustomRenderPointUniforms(Mesh * pMesh, GameObject * object, vec3 pos, mat4 viewMat, mat4 projMat, Light * light)
{

	glUniform1f(glGetUniformLocation(m_pRenderPointShader->GetProgram(), "u_Near"), 0.01f);
	glUniform1f(glGetUniformLocation(m_pRenderPointShader->GetProgram(), "u_Far"), light->attenuationCutoff * 15.0f);
	CheckForGLErrors();
}