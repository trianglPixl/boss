#include "pch.h"
#include "BossObject.h"

float ShortestAngle(float source, float other);

BossObject::BossObject(Scene* pScene, std::string name, vec3 pos, vec3 rot, vec3 scale, Mesh* pMesh, ShaderProgram* pShader, GLuint texture, GLuint textureNm):
GameObject(pScene, name, pos, rot, scale, pMesh, pShader, texture, textureNm)
{
	m_RotationSpeed = 20.0f;

	m_pLaserLight = nullptr;
    m_Health = 100.0f;
    m_DamageTimer = 0.0f;

	m_pWeakHitSound = m_pScene->GetResourceManager()->GetSound("WeakHit");
	m_pHitSound = m_pScene->GetResourceManager()->GetSound("Hit");
	m_pDeathSound = m_pScene->GetResourceManager()->GetSound("BossDeath");
}

BossObject::~BossObject()
{

}

void BossObject::Update(float deltatime)
{
	if (m_IsEnabled == false)
	{
		return;
	}
	if (m_DamageTimer > 0.0f)
	{
		m_DamageTimer -= deltatime;
		m_material = m_InvulnerableMaterial;
		m_RotationSpeed = 180.0f;
	}
	else
	{
		m_material = m_VulnerableMaterial;
		m_RotationSpeed = 20.0f;
	}

	GameObject* player = m_pScene->GetGameObject("Player");

    vec2 playerPos = vec2(player->GetPosition().x, player->GetPosition().z);

    vec2 deltaPos = vec2(playerPos.x - m_Position.x, playerPos.y - m_Position.z);

    float distance = deltaPos.LengthSquared();
    float detectionDistance = 10.0f;

	
	float playerAngle = atan2(deltaPos.y, deltaPos.x) * (180 / PI) - 90;
	
    if (playerAngle < -180)
    {
        playerAngle = playerAngle + 360;
    }
    
    //OutputMessage("Player Angle: %f\n", playerAngle);

    float yAngle = fmod(m_Rotation.y, 360.0f);

    //OutputMessage("Angle: %f\n", yAngle);

    if (yAngle > 180)
    {
        yAngle = yAngle - 360;
    }
    else if (yAngle < -180)
    {
        yAngle = yAngle + 360;
    }

	if (distance < detectionDistance * detectionDistance)
	{

		float deg = ShortestAngle(yAngle, playerAngle);

        //OutputMessage("Angle between: %f\n", deg);

        float rotationThisFrame = m_RotationSpeed * deltatime;

        if (fabs(deg) < rotationThisFrame)
        {
            rotationThisFrame = deg;
        }
        else if (deg < 0.0f)
        {
            rotationThisFrame = -rotationThisFrame;
        }

        m_Rotation.y += rotationThisFrame;
        m_pScene->GetGameObject("Gem")->SetRotation(m_Rotation);
	}

    Shoot(deltatime);

    if (m_Health <= 0.0f)
    {
        //Boss die
		//m_pScene->GetGameObject("Gem")->Disable();
		//Disable();
		//m_pLaserLight->attenuationCutoff = 0.0f;
		//m_pLaserLight->castsShadow = false;
		((RibbonObject*)m_pScene->GetGameObject("Ribbon"))->ClearVertices();

		m_pScene->GetSoundManager()->PlaySound(m_pDeathSound);
		// register high score
		float score = (float)MyGetSystemTime() - (float)m_pScene->GetSceneStartTime();

		HighScore::PlayerInfo* players = m_pScene->GetGameCore()->GameScores.Players;

		// Thanks, Antonio.

		int place = 0;

		for (int i = 4; i >= 0; i--)
		{
			if (score > players[i].score)
			{
				place = i + 1;
				break;
			}
		}

		if (place < 5)
		{
			for (int j = 4; j > place; j--)
			{
				players[j] = players[j - 1];
			}
		}

		players[place].name = "YOU";
		players[place].score = score;

		m_pScene->GetGameCore()->GameScores.SaveScores("TestSave.txt");

		SceneManager::GetInstance()->PopStack();
		SceneManager::GetInstance()->RestartScene("MenuScene");
    }


	//if (playerAngle + 180 == m_Rotation.y)
	//{
	//	
	//}
	//else if (playerAngle - 180 == m_Rotation.y)
	//{

	//}
}

void BossObject::Hurt(float damage)
{
    if (m_DamageTimer <= 0.0f)
    {
        m_Health -= damage;
		m_DamageTimer = 0.75f;
		m_pScene->GetSoundManager()->PlaySound(m_pHitSound);
    }
	else
	{
		m_pScene->GetSoundManager()->PlaySound(m_pWeakHitSound);
	}
}

void BossObject::SetDamageTimer(float time)
{
    m_DamageTimer = time;
}

void BossObject::SetDamageMaterials(Material vulnerableMaterial, Material invulnerableMaterial)
{
	m_VulnerableMaterial = vulnerableMaterial;
	m_InvulnerableMaterial = invulnerableMaterial;
}

void BossObject::SetHealth(float health)
{
	m_Health = health;
}

void BossObject::SetLaserLight(Light* pLight)
{
	m_pLaserLight = pLight;
}

void BossObject::SetLaserJitterTweenFunction(TweenFunc tween)
{
	m_pLaserLightJitter = tween;
}

void BossObject::Shoot(float deltatime)
{
    const float gemRadius = 2.0f;
    const float laserLength = 8.0f;
    const float angle = (m_Rotation.y) * (PI/180.0f);
    RibbonObject* laser = (RibbonObject*)m_pScene->GetGameObject("Ribbon");
    laser->ClearVertices();

    vec3 laserForward = vec3(-sin(angle), 0.0f, cos(angle));
    
    vec3 laserStart = m_Position + (laserForward * gemRadius);
    laser->AddRibbonVertex(laserStart);
	laser->AddRibbonVertex(laserStart + laserForward * laserLength);

	btVector3 rayStart = vs::v3v3<btVector3>(laserStart);
	btVector3 rayEnd = vs::v3v3<btVector3>(laserStart + laserForward * laserLength);

	btTransform rayTransform = btTransform(btQuaternion::getIdentity(), rayStart);
	btTransform rayEndTransform = btTransform(btQuaternion::getIdentity(), rayEnd);

	BulletPlayer* player = (BulletPlayer*)m_pScene->GetGameObject("Player");
	
	btQuaternion playerRotation;
	playerRotation.setEuler(0.0f, PI / 2.0f, 0.0f);
	btTransform playerTransform = btTransform(playerRotation, vs::v3v3<btVector3>(player->GetPosition()));

	btCollisionWorld::ClosestRayResultCallback rayCallback = btCollisionWorld::ClosestRayResultCallback(rayStart, rayEnd);

	btCollisionWorld::rayTestSingle(rayTransform, rayEndTransform, player->GetControllerGhostObject(), player->GetBulletShape(), playerTransform, rayCallback);

	if (rayCallback.hasHit())
	{
		OutputMessage("Player hit at %f\n", rayCallback.m_closestHitFraction);
		player->Hurt(25.0f);
	}

	if (m_pLaserLight != nullptr)
	{
		// move the light and make it pulse forward and backward
		double period = 0.75;
		double time = fmod(MyGetSystemTime(), period * 2.0);
		if (time > period)
		{
			time -= period;
			time = period - time;
		}
		float laserLightOffset = 1.2f + 0.3f * m_pLaserLightJitter(0.0f, 1.0f, time, period);
		m_pLaserLight->position = vec4(laserStart, 1.0f) + vec4(laserForward, 0.0f) * laserLightOffset;
	}
}

float ShortestAngle(float source, float other)
{
	if (fabs(other - source) < 180.0f)
	{
		return other - source;
	}
	else if (other > source)
	{
		return other - source - 360.0f;
	}
	return other - source + 360.0f;
}