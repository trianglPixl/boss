#include "pch.h"
#include "SkyBoxObject.h"

SkyBoxObject::SkyBoxObject(Scene * pScene, std::string name, vec3 pos, vec3 rot, vec3 scale, Mesh * pMesh, ShaderProgram * pShader, GLuint texture) :
	GameObject(pScene, name, pos, rot, scale, pMesh, pShader, texture)
{

}

SkyBoxObject::SkyBoxObject(GameObject & obj) :
	GameObject(obj)
{

}

SkyBoxObject::~SkyBoxObject()
{
}

void SkyBoxObject::Update(float deltatime)
{
	GameObject::Update(deltatime);
}

void SkyBoxObject::Draw(int renderorder)
{
	glDisable(GL_DEPTH_TEST);
	GameObject::Draw(renderorder);
	glEnable(GL_DEPTH_TEST);
}
