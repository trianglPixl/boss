#pragma once
#ifndef __SKYBOXOBJECT_H
#define __SKYBOXOBJECT_H

class SkyBoxObject : public GameObject
{
public:
	SkyBoxObject(Scene* pScene, std::string name, vec3 pos, vec3 rot,
		vec3 scale, Mesh* pMesh, ShaderProgram* pShader, GLuint texture);

	SkyBoxObject(GameObject & obj);

	~SkyBoxObject();

	void Update(float deltatime)	override;
	void Draw(int renderorder)		override;
};

#endif