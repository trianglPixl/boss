#include "pch.h"
#include "CameraObject.h"
#include <algorithm>
#include <math.h>

void CameraObject::buildProjectionMatrix()
{
	m_projectionMat.SetIdentity();

	if (m_usePerspective)
	{
		m_projectionMat.CreatePerspectiveVFoV(m_fov, m_aspectRatio, 0.1f, 10000.0f);
	}
	else
	{
		if(m_aspectRatio >= 0.0f)
			m_projectionMat.CreateOrtho(-m_orthoScale * m_aspectRatio, m_orthoScale * m_aspectRatio, -m_orthoScale, m_orthoScale, 0.0f, 500.0f);
		else
		{
			m_projectionMat.CreateOrtho(-m_orthoScale, m_orthoScale, -m_orthoScale / m_aspectRatio, m_orthoScale / m_aspectRatio, 0.0f, 500.0f);
		}
	}
}


CameraObject::CameraObject(Scene* pScene, std::string name, vec3 pos, vec3 rot, float scale, Mesh* pMesh, ShaderProgram* pShader, GLuint texture, bool debug)
	: GameObject(pScene, name, pos, rot, vec3(1.0f, 1.0f, 1.0f), pMesh, pShader, texture)
{
	m_debugMode = debug;
	m_orthoScale = scale;

	m_pFollowObject2D = nullptr;
	m_pFollowObject3D = nullptr;
	m_followOffset3D = 0.0f;
	m_eyeOffset3D = vec3(0.0f);

	buildProjectionMatrix();
}

CameraObject::~CameraObject()
{
}

//http://www.rorydriscoll.com/2016/03/07/frame-rate-independent-damping-using-lerp/ Article on lerping
void CameraObject::Update(float deltatime)
{
	if (m_debugMode)
	{
		HandleInput(deltatime);
	}

	else if (m_pFollowObject2D != nullptr)
	{

		if (m_followMode == CFM_LERP)
		{
			vec3 TempPos = m_pFollowObject2D->GetPosition() - m_Position;
			float modifier = 1 - exp(log(1.0f - m_lerpFactor) * deltatime);
			MyClamp(modifier, 0.0f, 1.0f);
			TempPos *= modifier;
			m_Position += vec3(TempPos.x, TempPos.y, 0.0f);
		}

		else if (m_followMode == CFM_DIRFOLLOW)
		{
			vec3 TempPos = m_pFollowObject2D->GetPosition();
			m_Position = vec3(TempPos.x, TempPos.y, m_Position.z);
		}
	}
	else if (m_pFollowObject3D != nullptr)
	{
		vec3 followPos = m_pFollowObject3D->GetPosition() + m_eyeOffset3D;
		vec3 cameraOffsetDirection = vec3(0.0f);
		cameraOffsetDirection.y = sin(m_Rotation.x / 180.0f * PI);
		float horizontalOrbitRadius = cos(m_Rotation.x / 180.0f * PI);
		cameraOffsetDirection.x = sin(-m_Rotation.y / 180.0f * PI) * horizontalOrbitRadius;
		cameraOffsetDirection.z = cos(-m_Rotation.y / 180.0f * PI) * horizontalOrbitRadius;

		followPos += cameraOffsetDirection * m_followOffset3D;

		if (m_followMode == CFM_LERP)
		{
			vec3 distance = followPos - m_Position;
			float modifier = 1 - exp(log(1.0f - m_lerpFactor) * deltatime);
			MyClamp(modifier, 0.0f, 1.0f);
			
			m_Position += distance * modifier;
		}
		else if (m_followMode == CFM_DIRFOLLOW)
		{
			m_Position = followPos;
		}
	}

    GameObject::Update( deltatime );
}

void CameraObject::SetFOV(float fov)
{
	m_fov = fov;
	buildProjectionMatrix();
}

float CameraObject::GetFOV()
{
	return m_fov;
}

void CameraObject::SetController(PlayerController * con)
{
	m_pController = con;
}

vec3 CameraObject::MoveInDirection(float rotaboutY, float speed)
{
	vec3 offset;
	float radRot = (rotaboutY * PI) / 180.0f;
	offset.x = speed * cosf(radRot);
	offset.z = speed * sinf(radRot);
	offset.y = 0.0f;
	return offset;
}

bool CameraObject::GetDebugMode()
{
	return m_debugMode;
}

void CameraObject::SetDebugMode(bool mode)
{
	m_debugMode = mode;
}

void CameraObject::SetFollowObject2D(GameObject* obj)
{
	if (m_pFollowObject3D != nullptr)
	{
		m_pFollowObject3D = nullptr;
	}

	m_pFollowObject2D = obj;
}

void CameraObject::SetFollowObject3D(GameObject* object)
{
	if (m_pFollowObject2D != nullptr)
	{
		m_pFollowObject2D = nullptr;
	}

	m_pFollowObject3D = object;
}

void CameraObject::ClearFollowObject(GameObject* obj)
{
	m_pFollowObject2D = nullptr;
}

void CameraObject::SetFollowMode(const CameraFollowMode& mode)
{
	m_followMode = mode;
}

void CameraObject::SetFollowOffset3D(float offset)
{
	m_followOffset3D = offset;
}

void CameraObject::SetEyeOffset3D(vec3 offset)
{
	m_eyeOffset3D = offset;
}

void CameraObject::AspectRatioChanged(float ar)
{
	m_aspectRatio = ar;

	buildProjectionMatrix();
}

void CameraObject::HandleInput(float delta)
{
	if (m_pController != nullptr)
	{
		if (m_pController->IsButtonPressed(PCB_CLeft))
		{
			m_Position += MoveInDirection(m_Rotation.y + 180.0f, m_movementSpeed) * delta;
		}

		if (m_pController->IsButtonPressed(PCB_CRight))
		{
			m_Position += MoveInDirection(m_Rotation.y, m_movementSpeed) * delta;
		}

		if (m_pController->IsButtonPressed(PCB_CUp))
		{
			m_Position.y += m_movementSpeed * delta;
		}

		if (m_pController->IsButtonPressed(PCB_CDown))
		{
			m_Position.y -= m_movementSpeed * delta;
		}

		if (m_pController->IsButtonPressed(PCB_CFORWARD))
		{
			m_Position += MoveInDirection(m_Rotation.y + 90.0f, m_movementSpeed) * delta;
		}

		if (m_pController->IsButtonPressed(PCB_CBACKWARD))
		{
			m_Position += MoveInDirection(m_Rotation.y - 90.0f, m_movementSpeed) * delta;
		}

		if (m_pController->IsButtonPressed(PCB_CROTL))
		{
			m_Rotation.y += m_rotationSpeed * delta;
		}

		if (m_pController->IsButtonPressed(PCB_CROTR))
		{
			m_Rotation.y -= m_rotationSpeed * delta;
		}

		if (m_pController->IsButtonPressed(PCB_CROTDOWN))
		{
			m_Rotation.x -= m_rotationSpeed * delta;
		}

		if (m_pController->IsButtonPressed(PCB_CROTUP))
		{
			m_Rotation.x += m_rotationSpeed * delta;
		}
	}
}
