#ifndef __GameObject_H__
#define __GameObject_H__

#include <memory>
#include <string>
#include "Box2D\Box2D.h"

class ShaderProgram;
class Mesh;
class Scene;
class SoundManager;

class GameObject
{
protected:

	std::string m_Name;

	vec3 m_Position;
	vec3 m_Rotation;
	vec3 m_Scale;

	Mesh* m_pMesh;

	ShaderProgram* m_pShaderProgram;
	GLuint m_TextureHandle;
	GLuint m_TextureNormalHandle;

	SoundManager* m_pSoundManager;
	int m_SoundChannelUsed;

	bool m_IsEnabled;
	bool m_IsPendingDisable;

	Material m_material;


	btRigidBody* m_pBulletRigidBody;
	btCollisionShape* m_pBulletCollisionShape;
	btMotionState* m_pBulletMotionState;
	BulletCollisionFilter m_BulletFilter;

	// internal function used to create a default motion state based on the world transform of the game object
	btMotionState* CreateMotionStateFromTransform();
	// internal function used to create an indexed mesh shape
	btBvhTriangleMeshShape* CreateTriangleMeshShapeFromBG(const char* filename);

public:
	enum RenderMode
	{
		RM_TEXTURE,
		RM_LIGHTING
	};

	RenderMode m_renderMode = RM_LIGHTING;

	Material & GetMaterial() { return m_material; };
	void SetMaterial(const Material & material) { m_material.ambient = material.ambient; m_material.specular = material.specular; m_material.shine = material.shine; };

	Scene* m_pScene;
	GameObject(Scene* pScene, std::string name, vec3 pos, vec3 rot,
		vec3 scale, Mesh* pMesh, ShaderProgram* pShader, GLuint texture, GLuint textureNm = 0);

	GameObject(GameObject & obj);

	virtual ~GameObject();

	virtual void Init(Scene* pScene, std::string name, vec3 pos, vec3 rot,
		vec3 scale, Mesh* pMesh, ShaderProgram* pShader, GLuint texture, GLuint textureNm);

	virtual void Update(float deltatime);
	virtual void Draw(int renderorder);
	virtual void Draw(int renderorder, vec3 pos, mat4 proj, mat4 view);

	void SetMesh(Mesh* pMesh) { m_pMesh = pMesh; }
	void SetShader(ShaderProgram* pShader) { m_pShaderProgram = pShader; }
	void SetTexture(GLuint texturehandle) { m_TextureHandle = texturehandle; }

	Mesh* GetMesh() { return m_pMesh; }
	ShaderProgram* GetShader() { return m_pShaderProgram; }
	GLuint GetTexture() { return m_TextureHandle; }
	GLuint GetNormal() { return m_TextureNormalHandle; }

	void SetPosition(vec3 pos) { m_Position = pos; }
	void SetRotation(vec3 rot) { m_Rotation = rot; }
	void SetScale(vec3 scale) { m_Scale = scale; }

	vec3 GetPosition() { return m_Position; }
	vec3 GetRotation() { return m_Rotation; }
	vec3 GetScale() { return m_Scale; }

	virtual void Enable();
	virtual void Disable();
	virtual void DisableDeferred();

	bool GetIsEnabled();

	//Box2D implementation
private:
	//Enable Physics toggle
	bool m_enablePhysics = false;

	//Base pointer for body
	b2Body * m_pBody = nullptr;

	//Used for storing fixtures
	std::map<std::string, b2Fixture *>	m_fixtures;

	void BuildFixture(std::string & name, b2Shape * shape, float & density,
		uint16 & categoryBits, uint16 & maskBits, float & friction,
		bool & isSensor, float & restitution);

public:
	//populates bodyDef with given values, creates the body, 
	//and sets enablePhysics to true
	void CreateBody(b2BodyType type, vec2 position, float angle, bool active,
		float linearDamping, float angularDamping, float gravityScale, bool fixedRot);

	//External physics toggle
	void SetUsePhysics(bool use);

	// Box2D physics
	b2Body * GetPhysicsBody();

	void AddCircleFixture(std::string name, vec2 pos, float radius, float density,
		uint16 categoryBits, uint16 maskBits, float friction,
		bool isSensor, float restitution);

	void AddRectFixture(std::string name, vec2 pos, float width, float height, float rot,
		float density, uint16 categoryBits, uint16 maskBits, float friction,
		bool isSensor, float restitution);

	void AddPolyFixture(std::string name, vec2 pos, b2Vec2 * verts, int vertCount,
		float density, uint16 categoryBits, uint16 maskBits, float fsriction,
		bool isSensor, float restitution);

	//Only use this with static bodies. Can cause issues otherwise.
	void AddChainFixture(std::string name, vec2 pos, b2Vec2 * verts, int vertCount,
		float density, uint16 categoryBits, uint16 maskBits, float fsriction,
		bool isSensor, float restitution);

	b2Fixture * GetFixture(std::string name);

	//Applying forces
	void 	ApplyForce(const b2Vec2 &force, const b2Vec2 &point, bool wake = true);
	void 	ApplyForceToCenter(const b2Vec2 &force, bool wake = true);
	void 	ApplyTorque(float32 torque, bool wake = true);
	void 	ApplyLinearImpulse(const b2Vec2 &impulse, const b2Vec2 &point, bool wake = true);
	void 	ApplyAngularImpulse(float32 impulse, bool wake = true);

	// Bullet physics
	btRigidBody* GetBulletBody();
	btCollisionShape* GetBulletShape();
	btMotionState* GetMotionState();

	void CreateBulletRigidBodyFromConstructionInfo(const btRigidBody::btRigidBodyConstructionInfo& info, BulletCollisionFilter filter = BulletCollisionFilter());
	void CreateBulletRigidBody(btCollisionShape* shape, btMotionState* motionState, float mass, BulletCollisionFilter filter = BulletCollisionFilter());
	void CreateBulletRigidBody(btCollisionShape* shape, float mass, BulletCollisionFilter filter = BulletCollisionFilter());
	
	void CreateBoxRigidBody(vec3 halfSize, btMotionState* motionState, float mass, BulletCollisionFilter filter = BulletCollisionFilter());
	void CreateBoxRigidBody(vec3 halfSize, float mass, BulletCollisionFilter filter = BulletCollisionFilter());
	void CreateSphereRigidBody(float radius, btMotionState* motionState, float mass, BulletCollisionFilter filter = BulletCollisionFilter());
	void CreateSphereRigidBody(float radius, float mass, BulletCollisionFilter filter = BulletCollisionFilter());

	void CreateBGRigidBody(const char* filename, btMotionState* motionState, BulletCollisionFilter filter = BulletCollisionFilter());
	void CreateBGRigidBody(const char* filename, BulletCollisionFilter filter = BulletCollisionFilter());

    std::string GetName();
};
#endif //__GameObject_H__