#include "pch.h"
#include "WavePlane.h"


WavePlane::WavePlane(Scene* pScene, std::string name, vec3 pos, vec3 rot, vec3 scale, Mesh* pMesh, ShaderProgram* pShader, GLuint texture) :
    GameObject(pScene, name, pos, rot, scale, pMesh, pShader, texture),
    m_RippleCount(0)
{
}


WavePlane::~WavePlane()
{
}

void WavePlane::Update(float deltatime)
{
    GameObject::Update(deltatime);

    for (int i = m_RippleCount - 1; i >= 0; i--)
    {
        m_RippleLifetimes[i] -= deltatime;
        if (m_RippleLifetimes[i] <= 0.0f)
        {
            // if the ripple is dead, put the last one in the list in its place
            m_RipplePoints[i] = m_RipplePoints[m_RippleCount - 1];
            m_RippleLifetimes[i] = m_RippleLifetimes[m_RippleCount - 1];
            m_RippleCount--;
        }
    }
}

void WavePlane::Draw(int renderorder)
{
    if (m_pMesh == 0)
        return;

    if (m_pMesh->GetRenderOrder() == renderorder)
    {
        ShaderProgram* pShaderProgram = m_pShaderProgram;

        mat4 viewMat = static_cast<CameraObject *>(m_pScene->GetGameObject("Camera"))->GetViewMat();

        mat4 projMat = static_cast<CameraObject *>(m_pScene->GetGameObject("Camera"))->GetProjMat();

        m_pMesh->SetupAttributes(pShaderProgram);
        m_pMesh->SetupUniforms(pShaderProgram, m_Scale, m_Rotation, m_Position,
            viewMat, projMat, m_TextureHandle, Vector4(1, 1, 1, 1), m_RippleCount, m_RipplePoints, m_RippleLifetimes);
        m_pMesh->Draw(pShaderProgram);
    }
}

void WavePlane::AddRipple(vec3 position)
{
    if (m_RippleCount < s_MaxRipples)
    {
        MyMatrix worldMatrix;
        worldMatrix.CreateSRT(m_Scale, m_Rotation, m_Position);
        position = worldMatrix.GetInverse() * position;

        m_RipplePoints[m_RippleCount] = position;
        m_RippleLifetimes[m_RippleCount] = 1.0f;
        m_RippleCount++;
    }
}
