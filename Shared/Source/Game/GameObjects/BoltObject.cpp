#include "pch.h"
#include "BoltObject.h"

void BoltObject::HitBoss(GameObject* boss)
{
	DisableDeferred();
	m_pPool->DisableActiveObject(this);
	((BossObject*)boss)->Hurt(10.0f);
}

void BoltObject::HitSolid()
{
	DisableDeferred();
	m_pPool->DisableActiveObject(this);
	m_pScene->GetSoundManager()->PlaySound(m_pWeakHitSound);
}

BoltObject::BoltObject(Scene* pScene, std::string name, vec3 pos, vec3 rot,
	vec3 scale, Mesh* pMesh, ShaderProgram* pShader, GLuint texture, GLuint textureNm) :
    GameObject(pScene, name, vec3(0.0f), vec3(0.0f), vec3(1.0f), pMesh, pShader, texture, textureNm),
	m_Lifetime(10.0f),
	m_Age(0.0f),
	m_pPool(nullptr)
{
	m_pWeakHitSound = m_pScene->GetResourceManager()->GetSound("WeakHit");
	m_pHitSound = m_pScene->GetResourceManager()->GetSound("Hit");
}

BoltObject::~BoltObject()
{
    
}

void BoltObject::Update(float deltaTime)
{
	vec3 lastPosition = m_Position;

	GameObject::Update(deltaTime);

	// prevent the bolt from sweeping from where it was enabled to where it was teleported to
	// m_Age is set to 0 when a bolt is enabled, so check for that
	if (m_Age == 0.0f)
	{
		lastPosition = m_Position;
	}

	if (GetIsEnabled() == false)
	{
		return;
	}

	m_Age += deltaTime;

	if (m_Age >= m_Lifetime)
	{
		Disable();
		if (m_pPool != nullptr)
		{
			m_pPool->DisableActiveObject(this);
		}
	}

	if (m_pBulletCollisionShape != nullptr)
	{
		if ((m_Position - lastPosition).LengthSquared() > 0.0f)
		{
			btTransform lastTransform;
			btTransform thisTransform;

			lastTransform.setIdentity();
			lastTransform.setOrigin(vs::v3v3<btVector3>(lastPosition));

			thisTransform.setIdentity();
			thisTransform.setOrigin(vs::v3v3<btVector3>(m_Position));

			btDynamicsWorld::ClosestConvexResultCallback callback = btDynamicsWorld::ClosestConvexResultCallback(lastTransform.getOrigin(), thisTransform.getOrigin());
			callback.m_collisionFilterGroup = BCF_All;
			callback.m_collisionFilterMask = ~BCF_Player;
			BulletWorld::GetInstance()->GetWorld()->convexSweepTest((btSphereShape*)m_pBulletCollisionShape, lastTransform, thisTransform, callback);

			if (callback.hasHit())
			{
				const btCollisionObject* otherCollisionObject = callback.m_hitCollisionObject;
				Hit(otherCollisionObject);
			}
		}
	}
}

void BoltObject::Enable()
{
	GameObject::Enable();

	m_Age = 0;
}

void BoltObject::SetPool(GameObjectPool * pPool)
{
	m_pPool = pPool;
}

void BoltObject::Hit(const btCollisionObject* other)
{
	if (m_IsPendingDisable)
	{
		return;
	}
	GameObject* otherObject = (GameObject*)other->getUserPointer();
	std::string otherName = otherObject->GetName();
	if (otherName == "Boss")
	{
		HitBoss(otherObject);
	}
	else if (otherName == "Gem")
	{
		HitBoss(m_pScene->GetGameObject("Boss"));
	}
	else if ((other->getCollisionFlags() & btRigidBody::CF_NO_CONTACT_RESPONSE) == 0 && otherName != "Player")
	{
		HitSolid();
	}
}

