#pragma once
#include "GameObject.h"
class MeteorPool :
    public GameObject
{
protected:
    const static float s_MinAngle;
    const static float s_MaxAngle;
    const static float s_MinSpeed;
    const static float s_MaxSpeed;
    const static float s_MinXPosition;
    const static float s_MaxXPosition;
    const static float s_SpawnHeight;

    const static unsigned int s_StartingMeteors = 15;
    std::vector<GameObject*> m_Meteors;
    int m_MeteorCount;

public:
    MeteorPool(Scene* pScene, std::string name, vec3 pos, vec3 rot,
        vec3 scale, Mesh* pMesh, ShaderProgram* pShader, GLuint texture);
    ~MeteorPool();

    virtual void Update(float deltatime);
    virtual void Draw(int renderorder);

    void AddMeteor();
    void CreateMeteor(int index);
    void DestroyMeteor(int index);
    void ActivateMeteor(int index);
};

