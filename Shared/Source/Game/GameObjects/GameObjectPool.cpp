#include "pch.h"
#include "GameObjectPool.h"


GameObjectPool::GameObjectPool(Scene* scene, std::vector<GameObject*> poolContents, bool initActive) :
	ObjectPool<GameObject>(poolContents, initActive),
	m_pScene(scene)
{
	if (initActive == false)
	{
		for (unsigned int i = 0; i < GetMasterPool()->size(); i++)
		{
			GetMasterPool()->at(i)->Disable();
		}
	}
}


GameObjectPool::~GameObjectPool()
{
	std::vector<std::unique_ptr<GameObject>>* masterPool = GetMasterPool();
	for (unsigned int i = 0; i < masterPool->size(); i++)
	{
		(*masterPool)[i].release();
	}
	ObjectPool<GameObject>::~ObjectPool();
}

std::vector<GameObject*>* GameObjectPool::DisableActiveObject(GameObject* object)
{
	auto activePool = ObjectPool<GameObject>::DisableActiveObject(object);
	if (object->GetIsEnabled())
	{
		object->Disable();
	}
	return activePool;
}

GameObject* GameObjectPool::AddObjectToActive()
{
	GameObject* object = ObjectPool<GameObject>::AddObjectToActive();
	if (object == nullptr)
	{
		return nullptr;
	}

	if (object->GetIsEnabled() == false)
	{
		object->Enable();
	}
	return object;
}