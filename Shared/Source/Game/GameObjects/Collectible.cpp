#include "pch.h"
#include "Collectible.h"

Collectible::Collectible(Scene * pScene, std::string name, vec3 pos, vec3 rot, vec3 scale, Mesh * pMesh, ShaderProgram * pShader, GLuint texture) :
	GameObject(pScene, name, pos, rot, scale, pMesh, pShader, texture)
{

}

Collectible::Collectible(GameObject & obj) :
	GameObject(obj)
{

}

Collectible::~Collectible()
{
}

void Collectible::Update(float deltatime)
{
	GameObject::Update(deltatime);
	
	if (m_isPickedUp == true && m_pFadeShader != nullptr)
	{
		m_howLongBeenFading += deltatime;
	}
}

void Collectible::Draw(int renderorder)
{
	if (m_IsEnabled == false)
	{
		return;
	}

	if (m_pMesh == 0)
		return;

	if (m_pMesh->GetRenderOrder() == renderorder)
	{
		ShaderProgram* pShaderProgram = m_pShaderProgram;

		mat4 viewMat = static_cast<CameraObject *>(m_pScene->GetGameObject("Camera"))->GetViewMat();

		mat4 projMat = static_cast<CameraObject *>(m_pScene->GetGameObject("Camera"))->GetProjMat();

		m_pMesh->SetupAttributes(pShaderProgram);

		if (m_isPickedUp == true && m_pFadeShader != nullptr)
		{
			if (m_justPickedUp == true)
			{
				m_startTime = (float)MyGetSystemTime();
				m_justPickedUp = false;
			}

			m_pMesh->SetupUniforms(m_pFadeShader, m_Scale, m_Rotation, m_Position,
				viewMat, projMat, m_TextureHandle, Vector4(1, 1, 1, 1), m_startTime);
		}

		else
		{
			m_pMesh->SetupUniforms(pShaderProgram, m_Scale, m_Rotation, m_Position,
				viewMat, projMat, m_TextureHandle, Vector4(1, 1, 1, 1));
		}

		m_pMesh->Draw(pShaderProgram);
	}
}

void Collectible::ResetFade()
{
	m_isPickedUp = false; 
	m_justPickedUp = false;
	m_startTime = 0.0f;
	m_howLongBeenFading = 0.0f;
}
