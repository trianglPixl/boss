#include "pch.h"
#include "PlayerController.h"

PlayerController::PlayerController()
{
    Clear();
}

PlayerController::~PlayerController()
{
}

void PlayerController::Clear()
{
    m_ButtonFlags = 0;
}

void PlayerController::UpdateHeldButtons()
{
	m_HeldButtonFlags = m_ButtonFlags;
}

void PlayerController::SetButtonPressed(PlayerControllerButtons button)
{
    m_ButtonFlags |= button;
}

void PlayerController::SetButtonReleased(PlayerControllerButtons button)
{
    m_ButtonFlags &= ~button;
}

bool PlayerController::IsButtonPressed(PlayerControllerButtons button)
{
    if( m_ButtonFlags & button )
        return true;

    return false;
}

bool PlayerController::IsButtonJustPressed(PlayerControllerButtons button)
{
	return ((m_ButtonFlags & button) != 0 && (m_HeldButtonFlags & button) == 0);
}
