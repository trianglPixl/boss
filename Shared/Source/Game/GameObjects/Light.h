#pragma once
#ifndef __LIGHT_H
#define __LIGHT_H

const unsigned int MAXLIGHTS = 8;

struct Light
{
	vec4 ambient;
	vec4 diffuse;
	vec4 specular;
	vec4 position;
	vec3 direction = vec3(0.0f,0.0f,0.0f);
	float angle = 0.0f;
	float angleCos = 0.0f;
	float lightRadius = 1.0f;
	float attenuationCutoff = 60.0f;
	bool castsShadow = false;
	FBODefinition fbo;
};

struct Material
{
	vec4  ambient;
	vec4  specular;
	float shine;
};

#endif