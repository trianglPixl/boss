#include "pch.h"
#include "NumberDisplay.h"

void NumberDisplay::GetUvOffsetsFromNum(std::vector<unsigned int> * vec, unsigned int num)
{
	bool	done = false;
	int		remain = num;
	int		modVal = 0;

	while (!done)
	{
		if (remain < 10)
		{
			vec->push_back(remain);
			done = true;
		}

		else
		{
			modVal = remain % 10;
			remain = remain / 10;

			vec->push_back(modVal);
		}
	}
}

void NumberDisplay::BuildMesh(unsigned int offset)
{
	float realOffset = (float)offset * UVdivisions;
	int numVerts = m_vRenderVertices.size();

	//BL
	m_vRenderVertices.push_back(	VertexFormat(0.0f + (numVerts / 4), 0.0f, 0.0f,									//Position
									Color[0], Color[1], Color[2], Color[3],												//Color
									0.0f + realOffset, 0.0f,														//UV
									0, 0, 1																			//Normals	
									));
	//TL
	m_vRenderVertices.push_back(	VertexFormat(0.0f + (numVerts / 4), MeshAspectRatio, 0.0f,						//Position
		Color[0], Color[1], Color[2], Color[3],													//Color
									0.0f + realOffset, 1.0f,														//UV
									0, 0, 1																			//Normals	
								));

	//TR
	m_vRenderVertices.push_back(	VertexFormat(1.0f + (numVerts / 4), MeshAspectRatio, 0.0f,						//Position
		Color[0], Color[1], Color[2], Color[3],													//Color
									UVdivisions + realOffset, 1.0f,													//UV
									0, 0, 1																			//Normals	
								));

	//BR
	m_vRenderVertices.push_back(	VertexFormat(1.0f + (numVerts / 4), 0.0f, 0.0f,									//Position
		Color[0], Color[1], Color[2], Color[3],													//Color
									UVdivisions + realOffset, 0.0f,													//UV
									0, 0, 1																			//Normals	
								));

	m_vIndeces.push_back(numVerts + 0);
	m_vIndeces.push_back(numVerts + 1);
	m_vIndeces.push_back(numVerts + 2);
	m_vIndeces.push_back(numVerts + 0);
	m_vIndeces.push_back(numVerts + 2);
	m_vIndeces.push_back(numVerts + 3);
}

NumberDisplay::NumberDisplay(float pointSize, unsigned int displayDigits, vec2 pos, float rot, 
	unsigned int baseNum, CameraObject * cam, unsigned char r, unsigned char g, unsigned char b) :

	MenuObject(pos, rot, pointSize, cam, "Data/Textures/NumberStrip.png", "Data/Shaders/NumberDisplay.vert",
		"Data/Shaders/NumberDisplay.frag"),

	m_minDisplayDigits(displayDigits),
	m_currentNum(baseNum),
	m_pointSize(pointSize)
{

	Color[0] = r;
	Color[1] = g;
	Color[2] = b;
	SetNumber(baseNum);

	m_posOffset = m_pCamera->GetPosition().XY() - m_position.XY();
}

NumberDisplay::~NumberDisplay()
{
}

int NumberDisplay::GetCurrentNum()
{
	return m_currentNum;
}

void NumberDisplay::SetNumber(int num)
{
	m_currentNum = num;
	m_vRenderVertices.clear();
	m_vIndeces.clear();

	std::vector<unsigned int> offsets;
	GetUvOffsetsFromNum(&offsets, num);

	unsigned int currentOffset = offsets.size();
	unsigned int range = (currentOffset < m_minDisplayDigits ? m_minDisplayDigits : currentOffset);
	for (unsigned int i = 0; i < range; i++)
	{
		if ((range - i) == currentOffset)
		{
			BuildMesh(offsets[--currentOffset]);
		}

		else
		{
			BuildMesh(0);
		}
	}

	//Build VBO and IBO
	glBindBuffer(GL_ARRAY_BUFFER, m_VBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(VertexFormat) * m_vRenderVertices.size(), m_vRenderVertices.data(), GL_STATIC_DRAW);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_IBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(unsigned int) * m_vIndeces.size(), m_vIndeces.data(), GL_STATIC_DRAW);

	CheckForGLErrors();
}

void NumberDisplay::SetPosition(vec2 pos)
{
	m_position = pos;
}

void NumberDisplay::SetRotation(float rot)
{
	m_rotation = rot;
}

void NumberDisplay::Update(float delta)
{
	if (m_pCamera != nullptr)
	{
		m_position = m_pCamera->GetPosition().XY() + m_posOffset;
	}
}
