#pragma once
#ifndef __BOLTOBJECT_H
#define __BOLTOBJECT_H

class GameObjectPool;

class BoltObject : public GameObject
{
public:
    BoltObject(Scene* pScene, std::string name, vec3 pos, vec3 rot, vec3 scale, Mesh* pMesh, ShaderProgram* pShader, GLuint texture, GLuint textureNm = 0);

	virtual ~BoltObject();

	virtual void Update(float deltaTime) override;
	virtual void Enable() override;

	virtual void SetPool(GameObjectPool* pPool);
	virtual void Hit(const btCollisionObject* other);

	virtual void HitBoss(GameObject* boss);
	virtual void HitSolid();

protected:
	float m_Lifetime;
	float m_Age;

	GameObjectPool* m_pPool;

	Mix_Chunk* m_pWeakHitSound;
	Mix_Chunk* m_pHitSound;
};

#endif