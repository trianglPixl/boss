#pragma once
#ifndef __BULLETPLAYER_H
#define __BULLETPLAYER_H

class BulletPlayer : public GameObject
{
public:
	BulletPlayer(Scene* pScene, std::string name, vec3 pos, vec3 rot,
		vec3 scale, float radius, float height, Mesh* pMesh, ShaderProgram* pShader, GLuint texture, GLuint textureNm = 0);

	virtual ~BulletPlayer();

	void SetController(PlayerController* controller);
	void SetCamera(CameraObject* camera);

	void Hurt(float damage);
	void SetDamageTimer(float time);

	void SetHealth(float health);
	
	GameObjectPool* GetBolts();

	virtual void Update(float deltaTime) override;

	void MoveControllerTo(vec3 position);

    void Shoot(float deltatime);
	void ReturnBolt(GameObject* bolt);

	virtual btGhostObject* GetControllerGhostObject();
protected:
	btPairCachingGhostObject* m_pBulletGhostObject;
	btKinematicCharacterController* m_pCharacterController;

	PlayerController* m_pController;
	CameraObject* m_pCamera;

	const float m_WalkAcceleration;
	float m_MaxSpeed;
	vec3 m_WalkVelocity;
	float m_Health;

	float m_BoltSpeed;
	float m_DamageTimer;

    std::unique_ptr<GameObjectPool> m_pBolts;

	Mix_Chunk* m_pHitSound;
	Mix_Chunk* m_pDeathSound;
};

#endif