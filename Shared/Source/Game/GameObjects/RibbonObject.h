#pragma once
#ifndef __RIBBONOBJECT_H
#define __RIBBONOBJECT_H

struct RibbonVertex
{
    vec3 position;
    vec3 tangent;
    float widthOffset;
	float spawnTime;

    RibbonVertex(vec3 position, vec3 tangent, float widthOffset, float spawnTime)
    {
        this->position = position;
        this->tangent = tangent;
        this->widthOffset = widthOffset;
		this->spawnTime = spawnTime;
    }
};

class RibbonObject : public GameObject
{
public:
    RibbonObject(Scene* pScene, std::string name, float width, ShaderProgram* pShader, GLuint texture, GLuint textureNm = 0);
    RibbonObject(Scene* pScene, std::string name, vec3 startPos, float width, ShaderProgram* pShader, GLuint texture, GLuint textureNm = 0);

	virtual ~RibbonObject();

	virtual void Update(float deltaTime) override;

    virtual void Draw(int renderOrder) override;

    void AddRibbonVertex(vec3 vertex);
	void MoveLastVertex(vec3 position);

	void SetIsUsingAdditiveBlending(bool isUsingAdditiveBlending);
	void SetColor(vec4 color);
	void SetVertexLifetime(float lifetime);

	void ClearVertices();

protected:
    void RegenerateMesh();
    void DrawMesh();

    float m_RibbonWidth;
	bool m_IsMeshDirty;

    std::vector<RibbonVertex> m_Vertices;

	vec4 m_Color;
	float m_VertexLifetime;
	float m_Age;
	bool m_IsUsingAdditiveBlending;

    GLuint m_VBO;
};

#endif