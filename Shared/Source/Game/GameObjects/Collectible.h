#pragma once
#ifndef __COLLECTIBLE_H
#define __COLLECTIBLE_H

class Collectible : public GameObject
{
	bool m_isPickedUp				= false;
	bool m_justPickedUp				= false;
	float m_startTime				= 0.0f;
	float m_howLongBeenFading		= 0.0f;
	ShaderProgram * m_pFadeShader	= nullptr;

public:
	Collectible(Scene* pScene, std::string name, vec3 pos, vec3 rot,
		vec3 scale, Mesh* pMesh, ShaderProgram* pShader, GLuint texture);

	Collectible(GameObject & obj);

	~Collectible();

	void Update(float deltatime)	override;
	void Draw(int renderorder)		override;

	void SetPickedUp(bool b) { m_isPickedUp = b; m_justPickedUp = b; m_startTime = 0.0f; };

	void SetFadeShader(ShaderProgram * shader) { m_pFadeShader = shader; };

	void ResetFade();

	float HowLongBeenFading() { return m_howLongBeenFading; };
};

#endif