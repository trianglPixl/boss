#pragma once
#include "GameObject.h"
class WavePlane :
    public GameObject
{
protected:
    const static unsigned int s_MaxRipples = 30;
    vec3 m_RipplePoints[s_MaxRipples];
    float m_RippleLifetimes[s_MaxRipples];
    int m_RippleCount;

public:
    WavePlane(Scene* pScene, std::string name, vec3 pos, vec3 rot,
        vec3 scale, Mesh* pMesh, ShaderProgram* pShader, GLuint texture);
    ~WavePlane();

    virtual void Update(float deltatime);
    virtual void Draw(int renderorder);

    void AddRipple(vec3 position);
};

