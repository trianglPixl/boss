#ifndef __CameraObject_H__
#define __CameraObject_H__

enum CameraFollowMode
{
	CFM_LERP		= 1 << 0,
	CFM_DIRFOLLOW	= 1 << 1
};

class CameraObject : public GameObject
{
protected:
	void buildProjectionMatrix();
	bool m_usePerspective = true;
	bool m_debugMode;

	PlayerController * m_pController = nullptr;
	float m_movementSpeed = 5.0f;
	float m_rotationSpeed = 20.0f;

	//Projection values
	float m_aspectRatio = 16.0f / 9.0f;
	float m_fov = 75.0f;
	float m_orthoScale = 5.0f;

	mat4 m_viewMat;
	mat4 m_projectionMat;

	GameObject * m_pFollowObject2D = nullptr;
	GameObject* m_pFollowObject3D = nullptr;
	CameraFollowMode m_followMode = CFM_LERP;

	float m_followOffset3D;
	vec3 m_eyeOffset3D;

	float m_lerpFactor = 0.5f;

	void HandleInput(float delta);

public:
    CameraObject(Scene* pScene, std::string name, vec3 pos, vec3 rot, float scale, Mesh* pMesh, ShaderProgram* pShader, GLuint texture, bool debug = false);
    virtual ~CameraObject();

	virtual void AspectRatioChanged(float ar);

    virtual void Update(float deltatime)		override;

	mat4 GetViewMat() { m_viewMat.CreateSRT(m_Scale, m_Rotation, m_Position); m_viewMat.Inverse(); return m_viewMat; };
	mat4 GetProjMat() { return m_projectionMat; };

	void SetUsePerspective(bool use) { m_usePerspective = use; };
	void SetFOV(float fov);
	float GetFOV();

	void SetController(PlayerController * con);

	vec3 MoveInDirection(float rotaboutY, float speed);

	bool GetDebugMode();
	void SetDebugMode(bool mode);

	void SetFollowObject2D(GameObject* obj);
	void SetFollowObject3D(GameObject* object);
	void ClearFollowObject(GameObject* obj);
	void SetFollowMode(const CameraFollowMode & mode);
	void SetLerpFactor(float speed) { m_lerpFactor = speed; };

	void SetFollowOffset3D(float offset);
	void SetEyeOffset3D(vec3 offset);
};

#endif //__CameraObject_H__
