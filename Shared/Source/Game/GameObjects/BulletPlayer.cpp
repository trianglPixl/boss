#include "pch.h"
#include "BulletPlayer.h"

BulletPlayer::BulletPlayer(Scene * pScene, std::string name, vec3 pos, vec3 rot, vec3 scale, float radius, float height, Mesh * pMesh, ShaderProgram * pShader, GLuint texture, GLuint textureNm) :
	GameObject(pScene, name, pos, rot, scale, pMesh, pShader, texture, textureNm),
	m_pController(nullptr),
	m_WalkAcceleration(80.0f),
	m_MaxSpeed(5.0f),
	m_BoltSpeed(20.0f)
{
	// create a base transform
	MyMatrix posRotMatrix;
	posRotMatrix.CreateSRT(1.0f, rot, pos);
	btTransform objectTransform;
	objectTransform.setFromOpenGLMatrix(&posRotMatrix.m11);

	m_pBulletGhostObject = new btPairCachingGhostObject();
	m_pBulletGhostObject->setWorldTransform(objectTransform);
	m_pBulletGhostObject->setCollisionShape(m_pBulletCollisionShape);
	m_pBulletGhostObject->setCollisionFlags(btCollisionObject::CF_CHARACTER_OBJECT);

	// the kinematic character controller rotates the capsule from z-facing to y-facing when we supply an up vector of (0, 1, 0)
	m_pBulletCollisionShape = new btCapsuleShapeZ(radius, height);
	m_pBulletGhostObject->setCollisionShape(m_pBulletCollisionShape);
	
	m_pCharacterController = new btKinematicCharacterController(m_pBulletGhostObject, (btConvexShape*)m_pBulletCollisionShape, 0.35f, btVector3(0.0f, 1.0f, 0.0f));
	m_pCharacterController->setMaxPenetrationDepth(0.05f);

	// climb 60-degree slopes
	m_pCharacterController->setMaxSlope(60.0f / 180.0f * PI);

	m_pCharacterController->setGravity(BulletWorld::GetInstance()->GetWorld()->getGravity() * 2.0f);

	BulletWorld::GetInstance()->GetWorld()->getBroadphase()->getOverlappingPairCache()->setInternalGhostPairCallback(new btGhostPairCallback());

	BulletWorld::GetInstance()->GetWorld()->addCollisionObject(m_pBulletGhostObject, BCF_Player);
	BulletWorld::GetInstance()->GetWorld()->addAction(m_pCharacterController);

	m_Health = 100.0f;
	m_DamageTimer = 0.0f;


    //GameObject bullet = GameObject(m_pScene, "PlayerBolt", vec3(0, 0, 0), vec3(0, 0, 0), vec3(1, 1, 1), m_pScene->GetResourceManager()->GetMesh("Bolt"), m_pScene->GetResourceManager()->GetShader("Texture"), m_pScene->GetResourceManager()->GetTexture("WallDI"), m_pScene->GetResourceManager()->GetTexture("BlankNM"));
	const int boltCount = 10;
	std::vector<GameObject*> bolts;
	for (int i = 0; i < 10; i++)
	{
		char nameBuffer[20];
		snprintf(nameBuffer, 20, "PlayerBolt%d", i);
		BoltObject* bolt = new BoltObject(m_pScene, nameBuffer, vec3(0.0f), vec3(0.0f), vec3(1.0f), m_pScene->GetResourceManager()->GetMesh("Bolt"), m_pScene->GetResourceManager()->GetShader("Texture"), m_pScene->GetResourceManager()->GetTexture("WallDI"), m_pScene->GetResourceManager()->GetTexture("BlankNM"));
		bolts.push_back(bolt);
		m_pScene->AddObject(bolt, nameBuffer);
	}

	m_pBolts.reset(new GameObjectPool(m_pScene, bolts, false));
	for (unsigned int i = 0; i < m_pBolts->GetMasterPool()->size(); i++)
	{
		BoltObject* bolt = (BoltObject*)bolts[i];
		bolt->SetPool(m_pBolts.get());
		bolt->CreateSphereRigidBody(0.085f, 0.1f);
        btRigidBody* boltBody = bolt->GetBulletBody();
		
        boltBody->setGravity(btVector3(0.0f, 0.0f, 0.0f));
        boltBody->setCollisionFlags(boltBody->getCollisionFlags() | btRigidBody::CF_NO_CONTACT_RESPONSE);
		bolt->Disable();
        //bulletBody->setLinearVelocity(btVector3(RandomRange(-2.0f, 2.0f), RandomRange(0.0f, 2.0f), RandomRange(-2.0f, 2.0f)));
	}


	// Get sounds
	m_pHitSound = m_pScene->GetResourceManager()->GetSound("PlayerHit");
	m_pDeathSound = m_pScene->GetResourceManager()->GetSound("PlayerDeath");
}

BulletPlayer::~BulletPlayer()
{
	if (m_pBulletGhostObject != nullptr)
	{
		BulletWorld::GetInstance()->GetWorld()->removeCollisionObject(m_pBulletGhostObject);
		delete m_pBulletGhostObject;
	}
	if (m_pCharacterController != nullptr)
	{
		delete m_pCharacterController;
	}
}

void BulletPlayer::SetController(PlayerController* controller)
{
	m_pController = controller;
}

void BulletPlayer::SetCamera(CameraObject* camera)
{
	m_pCamera = camera;
}

void BulletPlayer::Update(float deltatime)
{
	if (m_DamageTimer > 0.0f)
	{
		m_DamageTimer -= deltatime;
	}

	MyMatrix characterTransform;
	m_pBulletGhostObject->getWorldTransform().getOpenGLMatrix(&characterTransform.m11);
	m_Position = characterTransform.GetTranslation();
	m_Rotation = characterTransform.GetEulerAngles();

	vec3 moveDirection = vec3(0.0f);

	if (m_pController->IsButtonPressed(PCB_Up) == true)
	{
		moveDirection.z += 1.0f;
	}
	if (m_pController->IsButtonPressed(PCB_Down) == true)
	{
		moveDirection.z -= 1.0f;
	}
	if (m_pController->IsButtonPressed(PCB_Left) == true)
	{
		moveDirection.x -= 1.0f;
	}
	if (m_pController->IsButtonPressed(PCB_Right) == true)
	{
		moveDirection.x += 1.0f;
	}
    if (m_pController->IsButtonJustPressed(PCB_LMOUSE) == true)
    {
        Shoot(deltatime);
    }
	if (m_pController->IsButtonJustPressed(PCB_SPACE) == true)
	{
		OutputMessage("Active: %d\n", m_pBolts->GetActiveObjects()->size());
		if (m_pBolts->GetActiveObjects()->size() > 0)
		{
			OutputMessage("Things are active.\n");
		}
	}

	moveDirection.Normalize();

	if (m_pCamera != nullptr)
	{
		vec3 cameraRotation = m_pCamera->GetRotation();
		float sinY = sin((cameraRotation.y + 90.0f) / 180.0f * PI);
		float cosY = cos((cameraRotation.y + 90.0f) / 180.0f * PI);
		vec3 cameraForwardDirection = vec3(cosY, 0.0f, sinY);
		vec3 cameraRightDirection = vec3(sinY, 0.0f, -cosY);

		moveDirection = cameraForwardDirection * moveDirection.z + cameraRightDirection * moveDirection.x;
	}

	vec3 targetVelocity = moveDirection * m_MaxSpeed;
	vec3 targetVelocityDifference = targetVelocity - m_WalkVelocity;
	vec3 accelerationDirection = targetVelocityDifference.GetNormalized();

	float accelerationMagnitude = m_WalkAcceleration * deltatime;

	// if our acceleration this frame would "overshoot" the acceleration to our target velocity, just set our
	//     velocity to the target velocity
	if (targetVelocityDifference.LengthSquared() < accelerationMagnitude * accelerationMagnitude)
	{
		m_WalkVelocity = targetVelocity;
	}
	else
	{
		m_WalkVelocity += accelerationDirection * accelerationMagnitude;
	}

	// 1/60.0f because the velocity is in physics step time, not game time
	m_pCharacterController->setWalkDirection(vs::v3v3<btVector3>(m_WalkVelocity * (1.0f/60.0f)));

	if (m_pCharacterController->canJump() && m_pController->IsButtonJustPressed(PCB_SPACE) == true)
	{
		m_pCharacterController->jump(btVector3(0.0f, 6.0f, 0.0f));
	}

	if (m_Health <= 0)
	{
		m_pScene->GetSoundManager()->PlaySound(m_pDeathSound);
		SceneManager::GetInstance()->PushSceneToStack("GameOverScene", true);
	}

	GameObject::Update(deltatime);
}

void BulletPlayer::MoveControllerTo(vec3 position)
{
	// moving this hunk o' junk only works in z-up because otherwise it rotates the ghost object again, which you can't fix without rotating gravity along with the thing
	// solution: un-rotate it, move it and rotate it
	m_pCharacterController->setUp(btVector3(0.0f, 0.0f, 1.0f));
	m_pCharacterController->warp(vs::v3v3<btVector3>(position));
	m_pCharacterController->setUp(btVector3(0.0f, 1.0f, 0.0f));
}

btGhostObject* BulletPlayer::GetControllerGhostObject()
{
	return m_pBulletGhostObject;
}

void BulletPlayer::Hurt(float damage)
{
	if (m_DamageTimer <= 0.0f)
	{
    	m_Health -= damage;
		m_DamageTimer = 1.0f;
		m_pScene->GetSoundManager()->PlaySound(m_pHitSound);
	}
}

void BulletPlayer::SetDamageTimer(float time)
{
	m_DamageTimer = time;
}

void BulletPlayer::SetHealth(float health)
{
	m_Health = health;
}

GameObjectPool* BulletPlayer::GetBolts()
{
	return m_pBolts.get();
}

void BulletPlayer::Shoot(float deltatime)
{
    const float laserLength = 8.0f;
	MyMatrix cameraMatrix;
	// this is probably faster than a matrix transpose
	cameraMatrix.CreateSRT(1.0f, m_pCamera->GetRotation(), m_pCamera->GetPosition());

    vec4 zForward = vec4(0.0f, 0.0f, 1.0f, 0.0f);

    vec4 tempForward = cameraMatrix * zForward;

    vec3 boltForward = vec3(tempForward.x, tempForward.y, tempForward.z);

	GameObject* bolt = m_pBolts->AddObjectToActive();
	if (bolt != nullptr)
	{
		btRigidBody* boltBody = bolt->GetBulletBody();
		boltBody->setLinearVelocity(vs::v3v3<btVector3>(boltForward * m_BoltSpeed));
		btTransform boltTransform;
		boltTransform.setFromOpenGLMatrix(&cameraMatrix.m11);
		boltBody->setWorldTransform(boltTransform);
		// Yes, gravity has to be set again
		boltBody->setGravity(btVector3(0.0f, 0.0f, 0.0f));
		boltBody->activate(true);
	}

    RibbonObject* laser = (RibbonObject*)m_pScene->GetGameObject("PlayerLaser");
    laser->ClearVertices();
}

void BulletPlayer::ReturnBolt(GameObject* bolt)
{
	bolt->DisableDeferred();
	m_pBolts->DisableActiveObject(bolt);
}
