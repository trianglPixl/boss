#ifndef __PlayerController_H__
#define __PlayerController_H__

enum PlayerControllerButtons
{
    PCB_Up			= 1<<1,
    PCB_Down		= 1<<2,
    PCB_Left		= 1<<3,
    PCB_Right		= 1<<4,
    PCB_Bomb		= 1<<5,

	PCB_CUp			= 1<<6,
	PCB_CDown		= 1<<7,
	PCB_CLeft		= 1<<8,
	PCB_CRight		= 1<<9,
	PCB_CROTL		= 1<<10,
	PCB_CROTR		= 1<<11,
	PCB_CFORWARD	= 1<<12,
	PCB_CBACKWARD	= 1<<13,
	PCB_CROTUP		= 1<<14,
	PCB_CROTDOWN	= 1<<15,

	PCB_LMOUSE		= 1<<16,
	PCB_RMOUSE		= 1<<17,

	PCB_SPACE       = 1<<18,
	PCB_ENTER       = 1<<19,
	PCB_NULL		= 1<<20,
	PCB_ESCAPE		= 1<<21
};

class PlayerController
{
protected:
    unsigned int	m_ButtonFlags;
	unsigned int	m_HeldButtonFlags;
	vec2			m_mousePos;

public:
    PlayerController();
    virtual ~PlayerController();

    void Clear();
	void UpdateHeldButtons();

    void SetButtonPressed(PlayerControllerButtons button);
    void SetButtonReleased(PlayerControllerButtons button);

    bool IsButtonPressed(PlayerControllerButtons button);
	bool IsButtonJustPressed(PlayerControllerButtons button);

	vec2 GetMousePos() { return m_mousePos; };
	void SetMousePos(vec2 pos) { m_mousePos = pos; };
};

#endif //__PlayerController_H__