#include "pch.h"
#include "MeteorPool.h"

const float MeteorPool::s_MinAngle = PI * 1.25f;
const float MeteorPool::s_MaxAngle = PI * 1.75f;
const float MeteorPool::s_MinSpeed = 5.0f;
const float MeteorPool::s_MaxSpeed = 15.0f;
const float MeteorPool::s_MinXPosition = -15.0f;
const float MeteorPool::s_MaxXPosition = 15.0f;
const float MeteorPool::s_SpawnHeight = 25.0f;

MeteorPool::MeteorPool(Scene* pScene, std::string name, vec3 pos, vec3 rot, vec3 scale, Mesh* pMesh, ShaderProgram* pShader, GLuint texture):
    GameObject(pScene, name, pos, rot, scale, pMesh, pShader, texture), m_MeteorCount(0)
{
    for (int i = 0; i < s_StartingMeteors; i++)
    {
        CreateMeteor(i);
    }
}


MeteorPool::~MeteorPool()
{
    for (auto meteor : m_Meteors)
    {
        delete meteor;
    }
}

void MeteorPool::Update(float deltatime)
{
    for (int i = 0; i < m_MeteorCount; i++)
    {
        m_Meteors[i]->Update(deltatime);
        if (m_Meteors[i]->GetPosition().y < -20.0f)
        {
            DestroyMeteor(i);
        }
    }
}

void MeteorPool::Draw(int renderorder)
{
    for (int i = 0; i < m_MeteorCount; i++)
    {
        m_Meteors[i]->Draw(renderorder);
    }
}

void MeteorPool::AddMeteor()
{
    if (m_MeteorCount == m_Meteors.size())
    {
        CreateMeteor(m_MeteorCount);
    }

    ActivateMeteor(m_MeteorCount);
    m_MeteorCount++;
}

void MeteorPool::CreateMeteor(int index)
{
    GameObject* meteor = new GameObject(m_pScene, "Meteor", vec3(0, 0, 0), vec3(0, 0, 0), m_Scale,
        m_pMesh, m_pShaderProgram, m_TextureHandle);
    meteor->CreateBody(b2_dynamicBody, vec2(0.0f), 0.0f, false, 0.2f, 0.4f, 1.0f, false);
    meteor->AddCircleFixture("Collider", vec2(0.0f), meteor->GetScale().x / 2.0f, 1.0f, MB_METEOR, MB_PLAYER | MB_WAVE, 0.0f, false, 0.0f);

    m_Meteors.push_back(meteor);
}

void MeteorPool::DestroyMeteor(int index)
{
    GameObject* temp = m_Meteors[index];
    temp->GetPhysicsBody()->SetActive(false);

    m_Meteors[index] = m_Meteors[m_MeteorCount - 1];
    m_Meteors[m_MeteorCount - 1] = temp;
    m_MeteorCount--;
}

void MeteorPool::ActivateMeteor(int index)
{
    b2Body* meteorBody = m_Meteors[index]->GetPhysicsBody();

    meteorBody->SetActive(true);
    meteorBody->SetTransform(b2Vec2(RandomRange(s_MinXPosition, s_MaxXPosition), s_SpawnHeight), meteorBody->GetAngle());
    // get a random angle from 225 degrees to 315
    float newAngle = RandomRange(s_MinAngle, s_MaxAngle);
    float newVelocity = RandomRange(s_MinSpeed, s_MaxSpeed);
    //OutputMessage("%f, %f\n", cosf(newAngle), sinf(newAngle));
    meteorBody->SetLinearVelocity(b2Vec2(cosf(newAngle) * newVelocity, sinf(newAngle) * newVelocity));

	//m_Meteors[index]->SetPosition(vec3(rand() % 200 + 0, 0, rand() % 200 + 0));
}