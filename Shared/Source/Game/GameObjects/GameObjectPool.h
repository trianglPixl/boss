#ifndef __GAMEOBJECTPOOL_H
#define __GAMEOBJECTPOOL_H


class GameObjectPool :
	public ObjectPool<GameObject>
{
public:
	GameObjectPool(Scene* scene, std::vector<GameObject*> poolContents, bool initActive);
	~GameObjectPool();

	virtual std::vector<GameObject*>* DisableActiveObject(GameObject* object);
	virtual GameObject* AddObjectToActive();

private:
	Scene* m_pScene;
};

#endif