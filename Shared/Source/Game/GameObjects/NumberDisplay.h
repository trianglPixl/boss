#pragma once
#ifndef __NUMBERDISPLAY_H
#define __NUMBERDISPLAY_H

class NumberDisplay : public MenuObject
{
	const float MeshAspectRatio		= 412.0f / 275.0f;
	const float UVdivisions			= 1.0f / 10.0f;
	unsigned char  Color[4]	= {57,181,74,255};

	unsigned int		m_currentNum;
	unsigned int		m_minDisplayDigits;
	
	float m_pointSize = 1.0f;

	vec2 m_posOffset = vec2(0,0);

	void GetUvOffsetsFromNum(std::vector<unsigned int> * vec, unsigned int num);
	void BuildMesh(unsigned int offset);

public:

	NumberDisplay(	float pointSize, unsigned int displayDigits,
					vec2 pos, float rot, unsigned int baseNum,
					CameraObject * cam , unsigned char r, unsigned char g, unsigned char b);

	~NumberDisplay();

	int GetCurrentNum();

	void SetNumber(int num);

	void SetPosition(vec2 pos);
	void SetRotation(float rot);

	void Update(float delta);
};

#endif // __NUMBERDISPLAY_H