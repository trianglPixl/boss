#include "pch.h"
#include "RibbonObject.h"

RibbonObject::RibbonObject(Scene* pScene, std::string name, float width, ShaderProgram* pShader, GLuint texture, GLuint textureNm) :
    GameObject(pScene, name, vec3(0.0f), vec3(0.0f), vec3(1.0f), nullptr, pShader, texture, textureNm),
    m_RibbonWidth(width),
	m_Color(1.0f, 1.0f, 1.0f, 1.0f),
	m_VertexLifetime(0.0f),
	m_Age(0.0f),
	m_IsUsingAdditiveBlending(false),
	m_IsMeshDirty(false),
    m_VBO(-1)
{
}

RibbonObject::RibbonObject(Scene* pScene, std::string name, vec3 startPos, float width, ShaderProgram* pShader, GLuint texture, GLuint textureNm) :
    RibbonObject(pScene, name, width, pShader, texture, textureNm)
{
    AddRibbonVertex(startPos);
}

RibbonObject::~RibbonObject()
{
    glDeleteBuffers(1, &m_VBO);
}

void RibbonObject::Update(float deltaTime)
{
	m_Age += deltaTime;

	// if vertices are set to expire, erase them when they're not useful
	// for example, if we have a strip like this (numbers are vertex lifetimes, == are just the lines in between vertices):
	// -1==0==1
	// Delete the first one since interpolating from -1 to 0 will not yield any "lifetime" but keep vertex 2 because
	//  interpolating from 0 to 1 yields a useful value
	// This code assumes that you don't have vertices that aren't in order by spawn time, but that's impossible right now
	if (m_VertexLifetime > 0.0f)
	{
		for (unsigned int i = 2; i < m_Vertices.size();)
		{
			if (m_Age - m_Vertices[i].spawnTime > m_VertexLifetime)
			{
				m_Vertices.erase(m_Vertices.begin(), m_Vertices.begin() + i);
				m_IsMeshDirty = true;
				break;
			}

			i += 2;
		}
	}
}

void RibbonObject::Draw(int renderOrder)
{
    if (m_IsEnabled == false || renderOrder != 4)
    {
        return;
    }

    if (m_IsMeshDirty == true)
    {
        RegenerateMesh();
    }

    if (m_VBO == -1)
    {
        return;
    }

    // use additive blending
	if (m_IsUsingAdditiveBlending)
	{
		int lastSrc, lastDst;
		glGetIntegerv(GL_BLEND_SRC_ALPHA, &lastSrc);
		glGetIntegerv(GL_BLEND_DST_ALPHA, &lastDst);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE);

		DrawMesh();

		glBlendFunc(lastSrc, lastDst);
	}
	else
	{
		DrawMesh();
	}
}

void RibbonObject::AddRibbonVertex(vec3 vertex)
{
	vec3 tangent;

    if (m_Vertices.size() == 0)
    {
        // the tangent is a dummy
        tangent = vec3(0.0f, 1.0f, 0.0f);
    }
    else if (m_Vertices.size() == 2)
    {
        // we are adding the second vertex - its tangent and first tangent are just the direction
        //     from the first vertex to this one
        vec3 lastTangent = (vertex - m_Vertices[0].position).GetNormalized();
        m_Vertices[0].tangent = lastTangent;
		m_Vertices[1].tangent = lastTangent;

		tangent = lastTangent;
    }
    else
    {
        vec3 lastPoint = m_Vertices.back().position;
        vec3 secondLastPoint = m_Vertices[m_Vertices.size() - 3].position;
        vec3 lastPointDirection = (lastPoint - secondLastPoint).GetNormalized();
        vec3 thisPointDirection = (vertex - lastPoint).GetNormalized();
        vec3 lastTangent = (lastPointDirection + thisPointDirection).GetNormalized();
        m_Vertices.back().tangent = lastTangent;
		m_Vertices[m_Vertices.size() - 2].tangent = lastTangent;

		tangent = thisPointDirection;
    }

	m_Vertices.push_back(RibbonVertex(vertex, tangent, 1.0f, m_Age));
	m_Vertices.push_back(RibbonVertex(vertex, tangent, -1.0f, m_Age));

	m_IsMeshDirty = true;
}

void RibbonObject::MoveLastVertex(vec3 position)
{
	// prevent pointless calculations
	if (position == m_Vertices.back().position)
	{
		return;
	}

	m_Vertices.pop_back();
	m_Vertices.pop_back();

	AddRibbonVertex(position);
}

void RibbonObject::SetIsUsingAdditiveBlending(bool isUsingAdditiveBlending)
{
	m_IsUsingAdditiveBlending = isUsingAdditiveBlending;
}

void RibbonObject::SetColor(vec4 color)
{
	m_Color = color;
}

void RibbonObject::SetVertexLifetime(float lifetime)
{
	m_VertexLifetime = lifetime;
}

void RibbonObject::ClearVertices()
{
	m_Vertices.clear();

	m_IsMeshDirty = true;
}

void RibbonObject::RegenerateMesh()
{
	if (m_VBO == -1)
	{
		glGenBuffers(1, &m_VBO);
	}
    glBindBuffer(GL_ARRAY_BUFFER, m_VBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(RibbonVertex) * m_Vertices.size(), m_Vertices.data(), GL_DYNAMIC_DRAW);

    CheckForGLErrors();

	m_IsMeshDirty = false;
}

void RibbonObject::DrawMesh()
{
    // set attributes up
    glBindBuffer(GL_ARRAY_BUFFER, m_VBO);

    GLuint programHandle = m_pShaderProgram->GetProgram();
    glUseProgram(programHandle);
    
    GLint aPosition = glGetAttribLocation(programHandle, "a_Position");
    GLint aTangent = glGetAttribLocation(programHandle, "a_Tangent");
    GLint aWidthOffset = glGetAttribLocation(programHandle, "a_WidthOffset");
	GLint aSpawnTime = glGetAttribLocation(programHandle, "a_SpawnTime");

    assert(aPosition != -1);
    glVertexAttribPointer(aPosition, 3, GL_FLOAT, GL_FALSE, sizeof(RibbonVertex), (void*)offsetof(RibbonVertex, position));
    glEnableVertexAttribArray(aPosition);

    if (aTangent != -1)
    {
        glVertexAttribPointer(aTangent, 3, GL_FLOAT, GL_FALSE, sizeof(RibbonVertex), (void*)offsetof(RibbonVertex, tangent));
        glEnableVertexAttribArray(aTangent);
    }

    if (aWidthOffset != -1)
    {
        glVertexAttribPointer(aWidthOffset, 1, GL_FLOAT, GL_FALSE, sizeof(RibbonVertex), (void*)offsetof(RibbonVertex, widthOffset));
        glEnableVertexAttribArray(aWidthOffset);
    }

	if (aSpawnTime != -1)
	{
		glVertexAttribPointer(aSpawnTime, 1, GL_FLOAT, GL_FALSE, sizeof(RibbonVertex), (void*)offsetof(RibbonVertex, spawnTime));
		glEnableVertexAttribArray(aSpawnTime);
	}

    CheckForGLErrors();

    CameraObject* camera = (CameraObject*)m_pScene->GetGameObject("Camera");
    assert(camera != nullptr);

    // this thing can't be transformed and vertices are already in world space so there's no model matrix
    MyMatrix MVP = camera->GetProjMat() * camera->GetViewMat();
    
    vec3 cameraPosition = camera->GetPosition();

    GLint uMVP = glGetUniformLocation(programHandle, "u_MVP");
    GLint uCameraPosition = glGetUniformLocation(programHandle, "u_CameraPosition");
    GLint uWidth = glGetUniformLocation(programHandle, "u_Width");
	GLint uColor = glGetUniformLocation(programHandle, "u_Color");
	GLint uLifetime = glGetUniformLocation(programHandle, "u_Lifetime");
	GLint uTime = glGetUniformLocation(programHandle, "u_Time");

    if (uMVP != -1)
    {
        glUniformMatrix4fv(uMVP, 1, GL_FALSE, &MVP.m11);
    }

    if (uCameraPosition != -1)
    {
        glUniform3f(uCameraPosition, cameraPosition.x, cameraPosition.y, cameraPosition.z);
    }

    if (uWidth != -1)
    {
        glUniform1f(uWidth, m_RibbonWidth);
    }

	if (uColor != -1)
	{
		glUniform4f(uColor, m_Color.x, m_Color.y, m_Color.z, m_Color.w);
	}

	if (uLifetime != -1)
	{
		glUniform1f(uLifetime, m_VertexLifetime);
	}

	if (uTime != -1)
	{
		glUniform1f(uTime, m_Age);
	}

    CheckForGLErrors();

    // draw and disable attribute arrays
    glDrawArrays(GL_TRIANGLE_STRIP, 0, m_Vertices.size());

    CheckForGLErrors();
    
    if (aPosition != -1)
    {
        glDisableVertexAttribArray(aPosition);
    }
    if (aTangent != -1)
    {
        glDisableVertexAttribArray(aTangent);
    }
    if (aWidthOffset != -1)
    {
        glDisableVertexAttribArray(aWidthOffset);
    }
	if (aSpawnTime != -1)
	{
		glDisableVertexAttribArray(aSpawnTime);
	}
}