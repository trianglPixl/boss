#include "pch.h"
#include "GameObject.h"
#include <fstream>

using namespace std;

GameObject::GameObject(Scene* pScene, std::string name, vec3 pos, vec3 rot,
	vec3 scale, Mesh* pMesh, ShaderProgram* pShader, GLuint texture, GLuint textureNm) :
	m_pBody(nullptr),
	m_pBulletRigidBody(nullptr),
	m_pBulletCollisionShape(nullptr),
	m_pBulletMotionState(nullptr)
{
	Init(pScene, name, pos, rot, scale, pMesh, pShader, texture, textureNm);
}

GameObject::GameObject(GameObject & obj)
{
	m_pScene = obj.m_pScene;

	m_Name = obj.m_Name;

	m_Position = obj.m_Position;
	m_Rotation = obj.m_Rotation;
	m_Scale = obj.m_Scale;

	m_pMesh = obj.m_pMesh;

	m_pShaderProgram = obj.m_pShaderProgram;
	m_TextureHandle = obj.m_TextureHandle;
	m_IsEnabled = obj.m_IsEnabled;

	m_material = obj.m_material;
	m_renderMode = obj.m_renderMode;

	m_pBody = nullptr;
	m_pBulletRigidBody = nullptr;
	m_pBulletCollisionShape = nullptr;
	m_pBulletMotionState = nullptr;
}

GameObject::~GameObject()
{
	if (m_pBulletRigidBody != nullptr)
	{
		delete m_pBulletRigidBody;
	}

	if (m_pBulletCollisionShape != nullptr)
	{
		delete m_pBulletCollisionShape;
	}

	if (m_pBulletMotionState != nullptr)
	{
		delete m_pBulletMotionState;
	}
}

void GameObject::Init(Scene* pScene, std::string name, vec3 pos, vec3 rot,
	vec3 scale, Mesh* pMesh, ShaderProgram* pShader, GLuint texture, GLuint textureNm)
{
	m_pScene = pScene;

	m_Name = name;

	m_Position = pos;
	m_Rotation = rot;
	m_Scale = scale;

	m_pMesh = pMesh;

	m_pShaderProgram = pShader;
	m_TextureHandle = texture;
	m_TextureNormalHandle = textureNm;

	//Defualt Material values;
	m_material.ambient = vec4(0.01f, 0.01f, 0.01f, 1.0f);
	m_material.specular = vec4(0.01f, 0.01f, 0.01f, 1.0f);
	m_material.shine = 1.0f;

	m_IsEnabled = true;
	m_IsPendingDisable = false;
}

void GameObject::Update(float deltatime)
{
	if (m_IsPendingDisable == true && m_IsEnabled == true)
	{
		Disable();
	}

	if (GetIsEnabled() == false)
	{
		return;
	}

	//Slave to b2Body if physics enabled
	if (m_enablePhysics == true)
	{
		if (m_pBody != nullptr)
		{
			b2Transform transform = m_pBody->GetTransform();
			m_Position.x = transform.p.x;
			m_Position.y = transform.p.y;
			m_Rotation.z = transform.q.GetAngle() * 180.0f / PI;
		}
		else if (m_pBulletRigidBody != nullptr)
		{
			MyMatrix worldMatrix;
			m_pBulletRigidBody->getWorldTransform().getOpenGLMatrix(&worldMatrix.m11);
			m_Position = worldMatrix.GetTranslation();
			m_Rotation = worldMatrix.GetEulerAngles() * 180.0f / PI;
		}
	}
}

//This call is volatile as it relies on there being an object called "Camera" ~Fix soon
void GameObject::Draw(int renderorder)
{
	if (GetIsEnabled() == false)
	{
		return;
	}

	if (m_pMesh == 0)
		return;

	if (m_pMesh->GetRenderOrder() == renderorder)
	{
		ShaderProgram* pShaderProgram = m_pShaderProgram;

		CameraObject* camera = static_cast<CameraObject *>(m_pScene->GetGameObject("Camera"));

		mat4 viewMat = camera->GetViewMat();

		mat4 projMat = camera->GetProjMat();

		m_pMesh->SetupAttributes(pShaderProgram);

		switch (m_renderMode)
		{
		case GameObject::RM_TEXTURE:
			m_pMesh->SetupUniforms(pShaderProgram, m_Scale, m_Rotation, m_Position,
				viewMat, projMat, m_TextureHandle, Vector4(1, 1, 1, 1));
			break;
		case GameObject::RM_LIGHTING:
			m_pMesh->SetupUniforms(pShaderProgram, m_Scale, m_Rotation, m_Position, camera->GetPosition(),
				viewMat, projMat, Vector4(1, 1, 1, 1), m_TextureHandle, m_TextureNormalHandle, m_pScene->GetLightArr(), m_pScene->ACTIVELIGHTS, m_material.ambient, m_material.specular, m_material.shine );
			break;
		default:
			break;
		}
		
		m_pMesh->Draw(pShaderProgram);
	}
}

void GameObject::Draw(int renderorder, vec3 pos, mat4 proj, mat4 view)
{
	if (GetIsEnabled() == false)
	{
		return;
	}

	if (m_pMesh == 0)
		return;

	if (m_pMesh->GetRenderOrder() == renderorder)
	{
		ShaderProgram* pShaderProgram = m_pShaderProgram;

		m_pMesh->SetupAttributes(pShaderProgram);

		switch (m_renderMode)
		{
		case GameObject::RM_TEXTURE:
			m_pMesh->SetupUniforms(pShaderProgram, m_Scale, m_Rotation, m_Position,
				view, proj, m_TextureHandle, Vector4(1, 1, 1, 1));
			break;
		case GameObject::RM_LIGHTING:
			m_pMesh->SetupUniforms(pShaderProgram, m_Scale, m_Rotation, m_Position, pos,
				view, proj, Vector4(1, 1, 1, 1), m_TextureHandle, m_TextureNormalHandle, m_pScene->GetLightArr(), m_pScene->ACTIVELIGHTS, m_material.ambient, m_material.specular, m_material.shine);
			break;
		default:
			break;
		}

		m_pMesh->Draw(pShaderProgram);
	}
}

btMotionState* GameObject::CreateMotionStateFromTransform()
{
	btTransform initialTransform;
	MyMatrix engineTransform;
	engineTransform.CreateSRT(vec3(1.0f), m_Rotation, m_Position);
	initialTransform.setFromOpenGLMatrix(&engineTransform.m11);

	return new btDefaultMotionState(initialTransform);
}

btBvhTriangleMeshShape* GameObject::CreateTriangleMeshShapeFromBG(const char* filename)
{
	std::ifstream file;
	file.open(filename, std::ios::binary);

	if (!file.is_open())
	{
		OutputMessage("Error: failed to open %s", filename);
		assert(false);
	}
	unsigned int vertexCount;
	unsigned int indexCount;

	file.read((char*)&vertexCount, sizeof(unsigned int));
	file.read((char*)&indexCount, sizeof(unsigned int));

	assert(indexCount % 3 == 0);

	// read the vertices and indices into buffers
	vec3* vertices = new vec3[vertexCount];
	unsigned int* indices = new unsigned int[indexCount];
	file.read((char*)vertices, sizeof(vec3) * vertexCount);
	file.read((char*)indices, sizeof(unsigned int) * indexCount);
	file.close();

	// according to examples, you don't have to worry about this new - the btBvhTriangleMeshShape owns the thing.
	btTriangleIndexVertexArray* meshInterface = new btTriangleIndexVertexArray(indexCount / 3, (int*)indices, sizeof(unsigned int) * 3, vertexCount, (btScalar*)vertices, sizeof(vec3));

	return new btBvhTriangleMeshShape(meshInterface, true);
}

//Build the physics body based on given data and set enablePhysics true
void GameObject::CreateBody(b2BodyType type, vec2 position, float angle, bool active,
	float linearDamping, float angularDamping, float gravityScale,
	bool fixedRot)
{
	if (m_pBody == nullptr)
	{
		b2BodyDef bdef;
		bdef.type = type;
		bdef.position = b2Vec2(position.x, position.y);
		bdef.angle = angle;
		bdef.active = active;
		bdef.linearDamping = linearDamping;
		bdef.angularDamping = angularDamping;
		bdef.gravityScale = gravityScale;
		bdef.fixedRotation = fixedRot;
		bdef.userData = this;

		m_pBody = PhysicsWorld::GetInstance()->GetB2WorldPointer()->CreateBody(&bdef);
		m_enablePhysics = true;
	}
}

void GameObject::SetUsePhysics(bool use)
{
	if (m_pBody != nullptr)
		m_enablePhysics = use;
}

b2Body* GameObject::GetPhysicsBody()
{
	return m_pBody;
}

void GameObject::Enable()
{
	if (m_IsPendingDisable)
	{
		m_IsPendingDisable = false;
	}

	if (m_IsEnabled == true)
	{
		return;
	}

	m_IsEnabled = true;
	if (m_enablePhysics)
	{
		if (m_pBody != nullptr)
		{
			m_pBody->SetActive(false);
		}

        if (m_pBulletRigidBody != nullptr)
        {
            BulletWorld::GetInstance()->GetWorld()->addRigidBody(m_pBulletRigidBody, m_BulletFilter.group, m_BulletFilter.mask);
        }
	}
}

void GameObject::Disable()
{
	m_IsEnabled = false;
	m_IsPendingDisable = false;
	if (m_enablePhysics)
	{
		if (m_pBody != nullptr)
		{
			m_pBody->SetActive(false);
		}

        if (m_pBulletRigidBody != nullptr)
        {
            BulletWorld::GetInstance()->GetWorld()->removeCollisionObject(m_pBulletRigidBody);
        }
	}
}

void GameObject::DisableDeferred()
{
	m_IsPendingDisable = true;
}

bool GameObject::GetIsEnabled()
{
	return m_IsEnabled && !m_IsPendingDisable;
}

void GameObject::BuildFixture(std::string & name, b2Shape * shape, float & density,
	uint16 & categoryBits, uint16 & maskBits, float & friction,
	bool & isSensor, float & restitution)
{
	b2Filter filter;
	filter.categoryBits = categoryBits;
	filter.maskBits = maskBits;

	b2FixtureDef def;
	def.shape = shape;
	def.userData = this;
	def.density = density;
	def.filter = filter;
	def.friction = friction;
	def.isSensor = isSensor;
	def.restitution = restitution;

	m_fixtures[name] = m_pBody->CreateFixture(&def);
}

void GameObject::AddCircleFixture(std::string name, vec2 pos, float radius, float density,
	uint16 categoryBits, uint16 maskBits, float friction,
	bool isSensor, float restitution)
{
	b2CircleShape shape;
	shape.m_p = b2Vec2(pos.x, pos.y);
	shape.m_radius = radius;

	BuildFixture(name, &shape, density, categoryBits, maskBits,
		friction, isSensor, restitution);
}

void GameObject::AddRectFixture(std::string name, vec2 pos, float width, float height, float rot,
	float density, uint16 categoryBits, uint16 maskBits, float friction,
	bool isSensor, float restitution)
{
	b2PolygonShape shape;
	shape.SetAsBox(width / 2.0f, height / 2.0f, b2Vec2(pos.x, pos.y), rot);

	BuildFixture(name, &shape, density, categoryBits, maskBits,
		friction, isSensor, restitution);
}

void GameObject::AddPolyFixture(std::string name, vec2 pos, b2Vec2 * verts, int vertCount,
	float density, uint16 categoryBits, uint16 maskBits, float friction,
	bool isSensor, float restitution)
{
	b2PolygonShape shape;
	shape.Set(verts, vertCount);

	BuildFixture(name, &shape, density, categoryBits, maskBits,
		friction, isSensor, restitution);
}

void GameObject::AddChainFixture(std::string name, vec2 pos, b2Vec2 * verts, int vertCount,
	float density, uint16 categoryBits, uint16 maskBits, float friction,
	bool isSensor, float restitution)
{
	b2ChainShape shape;
	shape.CreateChain(verts, vertCount);

	BuildFixture(name, &shape, density, categoryBits, maskBits,
		friction, isSensor, restitution);
}

b2Fixture * GameObject::GetFixture(std::string name)
{
	return m_fixtures[name];
}

void GameObject::ApplyForce(const b2Vec2 & force, const b2Vec2 & point, bool wake)
{
	m_pBody->ApplyForce(force, point, wake);
}

void GameObject::ApplyForceToCenter(const b2Vec2 & force, bool wake)
{
	m_pBody->ApplyForceToCenter(force, wake);
}

void GameObject::ApplyTorque(float32 torque, bool wake)
{
	m_pBody->ApplyTorque(torque, wake);
}

void GameObject::ApplyLinearImpulse(const b2Vec2 & impulse, const b2Vec2 & point, bool wake)
{
	m_pBody->ApplyLinearImpulse(impulse, point, wake);
}

void GameObject::ApplyAngularImpulse(float32 impulse, bool wake)
{
	m_pBody->ApplyAngularImpulse(impulse, wake);
}

btRigidBody* GameObject::GetBulletBody()
{
	return m_pBulletRigidBody;
}

btCollisionShape* GameObject::GetBulletShape()
{
	return m_pBulletCollisionShape;
}

btMotionState* GameObject::GetMotionState()
{
	return m_pBulletMotionState;
}

void GameObject::CreateBulletRigidBodyFromConstructionInfo(const btRigidBody::btRigidBodyConstructionInfo& info, BulletCollisionFilter filter)
{
	m_pBulletCollisionShape = info.m_collisionShape;
	m_pBulletMotionState = info.m_motionState;
	m_pBulletRigidBody = new btRigidBody(info);
	m_pBulletRigidBody->setUserPointer(this);
	BulletWorld::GetInstance()->GetWorld()->addRigidBody(m_pBulletRigidBody, filter.group, filter.mask);
	m_enablePhysics = true;
}

void GameObject::CreateBulletRigidBody(btCollisionShape* shape, btMotionState* motionState, float mass, BulletCollisionFilter filter)
{
	m_pBulletCollisionShape = shape;
	m_pBulletMotionState = motionState;
	btVector3 localInertia(0.0f, 0.0f, 0.0f);
	if (mass != 0)
	{
		shape->calculateLocalInertia(mass, localInertia);
	}
	btRigidBody::btRigidBodyConstructionInfo bodyCI = btRigidBody::btRigidBodyConstructionInfo(mass, motionState, shape, localInertia);
	CreateBulletRigidBodyFromConstructionInfo(bodyCI, filter);
}

void GameObject::CreateBulletRigidBody(btCollisionShape* shape, float mass, BulletCollisionFilter filter)
{
	CreateBulletRigidBody(shape, CreateMotionStateFromTransform(), mass, filter);
}

void GameObject::CreateBoxRigidBody(vec3 halfSize, btMotionState * motionState, float mass, BulletCollisionFilter filter)
{
	btCollisionShape* boxShape = new btBoxShape(vs::v3v3<btVector3>(halfSize));
	CreateBulletRigidBody(boxShape, motionState, mass, filter);
}

void GameObject::CreateBoxRigidBody(vec3 halfSize, float mass, BulletCollisionFilter filter)
{
	btCollisionShape* boxShape = new btBoxShape(vs::v3v3<btVector3>(halfSize));
	CreateBulletRigidBody(boxShape, mass, filter);
}

void GameObject::CreateSphereRigidBody(float radius, btMotionState* motionState, float mass, BulletCollisionFilter filter)
{
	btCollisionShape* sphereShape = new btSphereShape(radius);
	CreateBulletRigidBody(sphereShape, motionState, mass, filter);
}

void GameObject::CreateSphereRigidBody(float radius, float mass, BulletCollisionFilter filter)
{
	btCollisionShape* sphereShape = new btSphereShape(radius);
	CreateBulletRigidBody(sphereShape, mass, filter);
}

void GameObject::CreateBGRigidBody(const char* filename, btMotionState* motionState, BulletCollisionFilter filter)
{
	CreateBulletRigidBody(CreateTriangleMeshShapeFromBG(filename), motionState, 0, filter);
}

void GameObject::CreateBGRigidBody(const char* filename, BulletCollisionFilter filter)
{
	CreateBulletRigidBody(CreateTriangleMeshShapeFromBG(filename), 0, filter);
}

std::string GameObject::GetName()
{
    return m_Name;
}
