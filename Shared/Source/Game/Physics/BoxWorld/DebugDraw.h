#pragma once
#ifndef __DEBUGDRAW_H
#define __DEBUGDRAW_H
#include <memory>
#include <vector>
#include "..\..\..\Core\Utility\ShaderProgram.h"
#include "Box2D\Box2D.h"



struct DebugVerts
{
	b2Vec2	vertex;
	vec4	color;
};

class PhysicsDebugDraw : public b2Draw
{
	GLuint m_VBO;
	std::shared_ptr<ShaderProgram> m_pShader = nullptr;

	CameraObject * m_pCam = nullptr;

	float angleStep;

public:
	PhysicsDebugDraw();
	~PhysicsDebugDraw();

	void DrawPolygon		(const b2Vec2 *vertices, int32 vertexCount, const b2Color &color)					override;
	void DrawSolidPolygon	(const b2Vec2 *vertices, int32 vertexCount, const b2Color &color)					override;
	void DrawCircle			(const b2Vec2 &center, float32 radius, const b2Color &color)						override;
	void DrawSolidCircle	(const b2Vec2 &center, float32 radius, const b2Vec2 &axis, const b2Color &color)	override;
	void DrawSegment		(const b2Vec2 &p1, const b2Vec2 &p2, const b2Color &color)							override;
	void DrawTransform		(const b2Transform &xf)																override;

	void DrawDebug(std::vector<DebugVerts> &data, GLenum drawMode);

	void SetCamera(CameraObject * cam);
};


#endif // !__DEBUGDRAW_H
