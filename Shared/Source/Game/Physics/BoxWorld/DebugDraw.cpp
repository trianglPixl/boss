#include "pch.h"
#include "DebugDraw.h"

using namespace std;

PhysicsDebugDraw::PhysicsDebugDraw()
{
	glGenBuffers(1, &m_VBO);

	m_pShader.reset(new ShaderProgram("Data/Shaders/Debug.vert", "Data/Shaders/Debug.frag"));

	angleStep = (float)((PI * (360.0f / 20) / 180.0f));

	SetFlags(e_shapeBit | e_aabbBit);
}

PhysicsDebugDraw::~PhysicsDebugDraw()
{
	if(m_VBO != -1)
		glDeleteBuffers(1, &m_VBO);
}

void PhysicsDebugDraw::DrawPolygon(const b2Vec2 * vertices, int32 vertexCount, const b2Color & color)
{
	vector<DebugVerts> verts;
	
	verts.push_back(DebugVerts());
	verts.back().vertex = vertices[0];
	verts.back().color = vec4(color.r, color.g, color.b, 0.4f);

	verts.push_back(DebugVerts());
	verts.back().vertex = vertices[3];
	verts.back().color = vec4(color.r, color.g, color.b, 0.4f);
	
	verts.push_back(DebugVerts());
	verts.back().vertex = vertices[2];
	verts.back().color = vec4(color.r, color.g, color.b, 0.4f);

	verts.push_back(DebugVerts());
	verts.back().vertex = vertices[1];
	verts.back().color = vec4(color.r, color.g, color.b, 0.4f);

	DrawDebug(verts, GL_LINE_LOOP);
}

void PhysicsDebugDraw::DrawSolidPolygon(const b2Vec2 * vertices, int32 vertexCount, const b2Color & color)
{
	vector<DebugVerts> verts;	

	verts.push_back(DebugVerts());
	verts.back().vertex = vertices[3];
	verts.back().color = vec4(color.r, color.g, color.b, 0.4f);

	verts.push_back(DebugVerts());
	verts.back().vertex = vertices[2];
	verts.back().color = vec4(color.r, color.g, color.b, 0.4f);

	verts.push_back(DebugVerts());
	verts.back().vertex = vertices[0];
	verts.back().color = vec4(color.r, color.g, color.b, 0.4f);

	verts.push_back(DebugVerts());
	verts.back().vertex = vertices[1];
	verts.back().color = vec4(color.r, color.g, color.b, 0.4f);

	DrawDebug(verts, GL_TRIANGLE_STRIP);
}

void PhysicsDebugDraw::DrawCircle(const b2Vec2 & center, float32 radius, const b2Color & color)
{
	vector<DebugVerts> verts;

	for (int i = 0; i < 20; i++)
	{
		verts.push_back(DebugVerts());
		verts.back().vertex = b2Vec2(cosf(i * angleStep) * radius + center.x, sinf(i * angleStep) * radius + center.y);
		verts.back().color = (vec4(color.r, color.g, color.b, 0.4f));
	}
	verts.push_back(DebugVerts());
	//verts.back().vertex = b2Vec2(cosf(0 * angleStep) * radius + center.x, sinf(0 * angleStep) * radius + center.y);
	//verts.back().color = (vec4(1.0f, 0.0f, 0.0f));

	DrawDebug(verts, GL_LINE_LOOP);
}

void PhysicsDebugDraw::DrawSolidCircle(const b2Vec2 & center, float32 radius, const b2Vec2 & axis, const b2Color & color)
{
	vector<DebugVerts> verts;

	verts.push_back(DebugVerts());
	verts.back().vertex = center;
	verts.back().color = (vec4(color.r, color.g, color.b , 0.4f));

	for (int i = 0; i < 20; i++)
	{
		verts.push_back(DebugVerts());
		verts.back().vertex = b2Vec2(cosf(i * angleStep) * radius + center.x, sinf(i * angleStep) * radius + center.y);
		verts.back().color = (vec4(color.r, color.g, color.b, 0.4f));
	}

	verts.push_back(DebugVerts());
	verts.back().vertex = b2Vec2(cosf(0 * angleStep) * radius + center.x, sinf(0 * angleStep) * radius + center.y);
	verts.back().color = (vec4(color.r, color.g, color.b, 0.4f));

	DrawDebug(verts, GL_TRIANGLE_FAN);
}

void PhysicsDebugDraw::DrawSegment(const b2Vec2 & p1, const b2Vec2 & p2, const b2Color & color)
{
	vector<DebugVerts> verts;

	verts.push_back(DebugVerts());
	verts.back().vertex = p1;
	verts.back().color = (vec4(color.r, color.g, color.b, 0.4f));

	verts.push_back(DebugVerts());
	verts.back().vertex = p2;
	verts.back().color = (vec4(color.r, color.g, color.b, 0.4f));

	DrawDebug(verts, GL_LINE);
}

void PhysicsDebugDraw::DrawTransform(const b2Transform & xf)
{
	vector<DebugVerts> verts;

	verts.push_back(DebugVerts());
	verts.back().vertex = b2Vec2(xf.p.x, xf.p.y);
	verts.back().color = (vec4(0.0f, 1.0f, 0.0f, 1.0f));

	verts.push_back(DebugVerts());
	verts.back().vertex = xf.q.GetXAxis();;
	verts.back().color = (vec4(0.0f, 1.0f, 0.0f, 1.0f));

	verts.push_back(DebugVerts());
	verts.back().vertex = b2Vec2(xf.p.x, xf.p.y);
	verts.back().color = (vec4(0.0f, 1.0f, 0.0f, 1.0f));

	verts.push_back(DebugVerts());
	verts.back().vertex = xf.q.GetYAxis();;
	verts.back().color = (vec4(0.0f, 1.0f, 0.0f, 1.0f));

	DrawDebug(verts, GL_LINES);
}

void PhysicsDebugDraw::DrawDebug(vector<DebugVerts> &data, GLenum drawMode)
{
	glUseProgram(m_pShader->GetProgram());
	glBindBuffer(GL_ARRAY_BUFFER, m_VBO);

	glBufferData(GL_ARRAY_BUFFER, sizeof(DebugVerts) * data.size(), data.data(), GL_STATIC_DRAW);

	GLuint pos = glGetAttribLocation(m_pShader->GetProgram(), "a_Position");
	if (pos != -1)
	{
		glVertexAttribPointer(pos, 2, GL_FLOAT, GL_FALSE, sizeof(DebugVerts), (void *)0 );
		glEnableVertexAttribArray(pos);
	}

	GLuint col = glGetAttribLocation(m_pShader->GetProgram(), "a_Color");
	if(col != -1)
	{
		glVertexAttribPointer(col, 4, GL_FLOAT, GL_FALSE, sizeof(DebugVerts), (void *)offsetof(DebugVerts, color));
		glEnableVertexAttribArray(col);
	}

	if (m_pCam != nullptr)
	{
		GLuint vp = glGetUniformLocation(m_pShader->GetProgram(), "u_VP");
		if (vp != -1)
		{
			mat4 matrix = m_pCam->GetProjMat() * m_pCam->GetViewMat();
			glUniformMatrix4fv(vp, 1, GL_FALSE, &matrix.m11);
		}
	}

	glDrawArrays(drawMode, 0, data.size());
	glBindBuffer(GL_ARRAY_BUFFER, 0);
}

void PhysicsDebugDraw::SetCamera(CameraObject * cam)
{
	m_pCam = cam;
}
