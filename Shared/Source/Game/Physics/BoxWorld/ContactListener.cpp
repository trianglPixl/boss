#include "pch.h"
#include "ContactListener.h"

void ContactListener::BeginContact(b2Contact * contact)
{
	if(bContact != nullptr)
		(*bContact)(contact);
}

void ContactListener::EndContact(b2Contact * contact)
{
	if(eContact != nullptr)
		(*eContact)(contact);
}
