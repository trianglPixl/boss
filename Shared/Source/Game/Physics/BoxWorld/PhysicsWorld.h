#pragma once
#ifndef __PHYSICSWORLD_H
#define __PHYSICSWORLD_H
#include <memory>
#include <mutex>
#include "Box2D\Box2D.h"
#include "DebugDraw.h"
#include "ContactListener.h"

//Class is singleton
class PhysicsWorld
{
	static std::shared_ptr<std::mutex>			__MutexLock;
	static std::shared_ptr<PhysicsWorld>	m_physInstance;

	std::shared_ptr<b2World>				m_pB2World			= nullptr;
	std::unique_ptr<PhysicsDebugDraw>		m_pDebugDraw		= nullptr;
	std::unique_ptr<ContactListener>		m_pContactListener	= nullptr;
	
	//Const vars
	const b2Vec2	m_gravity				= b2Vec2(0.0f, -9.8f);
	const float		m_timeStep				= 1.0f / 60.0f;
	const int		m_velocityIterations	= 8;
	const int		m_positionIterations	= 8;

public:
	//Return single instance of PhysicsWorld

	static PhysicsWorld * GetInstance();

	//Constuctors & destructors

	PhysicsWorld();
	~PhysicsWorld();

	//Functions.

	void		Update				(float delta);
	void		SetDebugCamera		(CameraObject * cam);
	void		DrawDebug			();

	b2World *	GetB2WorldPointer	() { return m_pB2World.get(); };

    ContactListener* GetContactListener() { return m_pContactListener.get(); };

	void ResetWorld();
};


#endif // !__PHYSICSWORLD_H
