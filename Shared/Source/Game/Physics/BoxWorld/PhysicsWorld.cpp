#include "pch.h"
#include "PhysicsWorld.h"

using namespace std;

shared_ptr<mutex>			PhysicsWorld::__MutexLock	= shared_ptr<mutex>(new mutex());
shared_ptr<PhysicsWorld>	PhysicsWorld::m_physInstance		= nullptr;

PhysicsWorld * PhysicsWorld::GetInstance()
{
	if (m_physInstance == nullptr)
	{
		lock_guard<mutex> lock(*__MutexLock);
		if(m_physInstance == nullptr)
		{
			m_physInstance.reset(new PhysicsWorld());
		}
	}

	return m_physInstance.get();
}

PhysicsWorld::PhysicsWorld()
{
	m_pB2World.reset(new b2World(m_gravity));
	m_pDebugDraw.reset(new PhysicsDebugDraw());
	m_pContactListener.reset(new ContactListener());

	//Set Draw Function
	m_pB2World->SetDebugDraw(m_pDebugDraw.get());

	//Set Contact Listener
	m_pB2World->SetContactListener(m_pContactListener.get());
}

PhysicsWorld::~PhysicsWorld()
{
}

void PhysicsWorld::Update(float delta)
{
	static float accumTime = 0;
	accumTime += delta;

	if (accumTime >= m_timeStep)
	{
		m_pB2World->Step(m_timeStep, m_velocityIterations, m_positionIterations);
		accumTime -= m_timeStep;
	}

	else
	{
		m_pB2World->ClearForces();
	}
}

void PhysicsWorld::SetDebugCamera(CameraObject * cam)
{
	if(m_pDebugDraw != nullptr)
		m_pDebugDraw->SetCamera(cam);
}

void PhysicsWorld::DrawDebug()
{
    glDepthFunc(GL_ALWAYS);
	m_pB2World->DrawDebugData();
    glDepthFunc(GL_LEQUAL);
}

void PhysicsWorld::ResetWorld()
{
	m_pB2World.reset(new b2World(m_gravity));

	//Set Draw Function
	m_pB2World->SetDebugDraw(m_pDebugDraw.get());

	//Set Contact Listener
	m_pContactListener->SetBeginContact(nullptr);
	m_pContactListener->SetEndContact(nullptr);
	m_pB2World->SetContactListener(m_pContactListener.get());
}
