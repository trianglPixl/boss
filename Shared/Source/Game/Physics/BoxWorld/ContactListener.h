#pragma once
#ifndef __CONTACTLISTENER_H
#define __CONTACTLISTENER_H
#include "..\..\..\GameHeaders.h"

//Seeing as contact stuff may change based on the game you can just set the contact pointers from wherever.
class ContactListener : public b2ContactListener
{
public:
	void BeginContact		(b2Contact* contact) override;
	void EndContact			(b2Contact* contact) override;

	void SetBeginContact	(void(*func)(b2Contact* contact	)) { bContact = func; };
	void SetEndContact		(void(*func)(b2Contact * contact)) { eContact = func; };
private:
	void(*bContact)(b2Contact* contact) = nullptr;
	void(*eContact)(b2Contact* contact) = nullptr;
};

#endif // !__CONTACTLISTENER_H
