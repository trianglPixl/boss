#include "pch.h"
#include "BulletWorld.h"

BulletWorld* BulletWorld::s_pInstance = nullptr;

BulletWorld::BulletWorld()
{
	m_pBroadphase = new btDbvtBroadphase();
	m_pCollisionConfiguration = new btDefaultCollisionConfiguration();
	m_pCollisionDispatcher = new btCollisionDispatcher(m_pCollisionConfiguration);
	m_pConstraintSolver = new btSequentialImpulseConstraintSolver();

	m_pDynamicsWorld = new btDiscreteDynamicsWorld(m_pCollisionDispatcher, m_pBroadphase, m_pConstraintSolver, m_pCollisionConfiguration);
	m_pDynamicsWorld->setGravity(btVector3(0.0f, -9.8f, 0.0f));
}

BulletWorld::~BulletWorld()
{
	if (m_pDynamicsWorld != nullptr)
	{
		delete m_pDynamicsWorld;
	}

	if (m_pBroadphase != nullptr)
	{
		delete m_pBroadphase;
	}

	if (m_pCollisionConfiguration != nullptr)
	{
		delete m_pCollisionConfiguration;
	}

	if (m_pCollisionDispatcher != nullptr)
	{
		delete m_pCollisionDispatcher;
	}

	if (m_pConstraintSolver != nullptr)
	{
		delete m_pConstraintSolver;
	}
}

BulletWorld* BulletWorld::GetInstance()
{
	if (s_pInstance == nullptr)
	{
		s_pInstance = new BulletWorld();
	}

	return s_pInstance;
}

btDynamicsWorld* BulletWorld::GetWorld()
{
	return m_pDynamicsWorld;
}