#pragma once
#ifndef __BULLETWORLD_H
#define __BULLETWORLD_H

enum BulletFilter
{
	BCF_Player = 1 << 1,
	BCF_Enemy = 1 << 2,
	BCF_World = 1 << 3,
	BCF_All = -1,
};

struct BulletCollisionFilter
{
	int group = BCF_All;
	int mask = BCF_All;

	BulletCollisionFilter() {}

	BulletCollisionFilter(int group, int mask)
	{
		this->group = group;
		this->mask = mask;
	}
};

class BulletWorld
{
public:
	~BulletWorld();

	static BulletWorld* GetInstance();

	btDynamicsWorld* GetWorld();
private:
	BulletWorld();

	btBroadphaseInterface* m_pBroadphase;
	btCollisionConfiguration* m_pCollisionConfiguration;
	btCollisionDispatcher* m_pCollisionDispatcher;
	btSequentialImpulseConstraintSolver* m_pConstraintSolver;

	btDynamicsWorld* m_pDynamicsWorld;

	static BulletWorld* s_pInstance;
};

#endif