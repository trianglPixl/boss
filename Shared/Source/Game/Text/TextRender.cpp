#include "pch.h"
#include "TextRender.h"

TextRender::TextRender(std::string faceName, unsigned int pointSize, unsigned int renderWidth, unsigned int renderHeight, float boxWidth, float boxHeight, TextRender::AnchorPos anchor, CameraObject * cam,
	const char * vert, const char * frag) :
	m_pCamera(cam),
	m_position(vec3(0)),
	m_rotation(vec3(0)),
	m_pointSize(pointSize),
	m_attachedToCamera(true)
{
	m_pShader.reset(new ShaderProgram(vert, frag));

	//Create bitmap array
	m_width = renderWidth;
	m_height = renderHeight;
	m_vAspectRatio = (float)m_height / (float)m_width;
	m_bitmap.reset( new unsigned char[ renderWidth * renderHeight ] { 0 } );

	//init freetype
	m_errorCode = FT_Init_FreeType(&m_library);
	if (m_errorCode)
		assert(false);

	m_errorCode = FT_New_Face(m_library, faceName.c_str(), 0, &m_face);
	if (m_errorCode)
		assert(false);

	m_errorCode = FT_Set_Char_Size(m_face, 0, pointSize * 64, renderWidth, renderHeight);
	if (m_errorCode)
		assert(false);

	m_boxWidth = boxWidth;
	m_boxHeight = boxHeight;

	m_baseScaleX = m_face->size->metrics.x_scale;
	m_baseScaleY = m_face->size->metrics.y_scale;

	glGenTextures(1, &m_texHandle);

	//TL
	m_vIndeces.push_back(0);
	
	//TR
	m_vIndeces.push_back(1);

	//BL
	m_vIndeces.push_back(2);

	//TR
	m_vIndeces.push_back(1);

	//BR
	m_vIndeces.push_back(3);

	//BL
	m_vIndeces.push_back(2);

	glGenBuffers(1, &m_VBO);
	glGenBuffers(1, &m_IBO);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_IBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(int) * m_vIndeces.size(), m_vIndeces.data(), GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	CheckForGLErrors();

	ChangeBoxDimensions(boxWidth, boxHeight, anchor);
}

TextRender::~TextRender()
{
	FT_Done_Face(m_face);
	FT_Done_FreeType(m_library);
}

void TextRender::ChangeBoxDimensions(float width, float height, TextRender::AnchorPos anchor)
{
	float modHeight;
	if (height == 0)
	{
		modHeight = width * m_vAspectRatio;
	}
	else
	{
		modHeight = height;
	}

	RegenerateBox(width, modHeight, anchor);
}

void TextRender::Draw()
{
	glUseProgram(m_pShader->GetProgram());
	
	glBindBuffer(GL_ARRAY_BUFFER, m_VBO);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_IBO);

	//Load uniforms

	glDepthMask(GL_FALSE);

	GLint aPos = glGetAttribLocation(m_pShader->GetProgram(), "a_Position");
	if (aPos != -1)
	{
		glVertexAttribPointer(aPos, 3, GL_FLOAT, GL_FALSE, sizeof(VertexFormat), (void *)offsetof(VertexFormat, pos));
		glEnableVertexAttribArray(aPos);
	}

	GLint aUV = glGetAttribLocation(m_pShader->GetProgram(), "a_UV");
	if (aUV != -1)
	{
		glVertexAttribPointer(aUV, 2, GL_FLOAT, GL_FALSE, sizeof(VertexFormat), (void*)offsetof(VertexFormat, uv));
		glEnableVertexAttribArray(aUV);
	}

	GLint aCol = glGetAttribLocation(m_pShader->GetProgram(), "a_Color");
	if (aCol != -1)
	{
		glVertexAttribPointer(aCol, 4, GL_UNSIGNED_BYTE, GL_TRUE, sizeof(VertexFormat), (void*)offsetof(VertexFormat, color));
		glEnableVertexAttribArray(aCol);
	}

	GLint uTexCol = glGetUniformLocation(m_pShader->GetProgram(), "u_TextColor");
	if (uTexCol != -1)
	{
		glUniform3f(uTexCol, m_textColor.x, m_textColor.y, m_textColor.z);
	}

	GLint uMvp = glGetUniformLocation(m_pShader->GetProgram(), "u_MVP");
	if (uMvp != -1)
	{
		mat4 transform;
		transform.CreateSRT(vec3(1), m_rotation, m_position);

		if(m_attachedToCamera)
			transform = m_pCamera->GetProjMat() * m_pCamera->GetViewMat() * transform;
		else
			transform = m_pCamera->GetProjMat() * transform;

		glUniformMatrix4fv(uMvp, 1, GL_FALSE, &transform.m11);
	}

	GLint uTexture = glGetUniformLocation(m_pShader->GetProgram(), "u_Texture");
	if (uTexture != -1)
	{
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, m_texHandle);

		glUniform1i(uTexture, 0);
	}

	//Draw
	glDrawElements(GL_TRIANGLES, m_vIndeces.size(), GL_UNSIGNED_INT, 0);

	glDepthMask(GL_TRUE);
	glBindTexture(GL_TEXTURE_2D, 0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	CheckForGLErrors();
}

void TextRender::SetTranslation(vec3 pos, vec3 rot)
{
	m_position = pos;
	m_rotation = rot;
}

void TextRender::ChangeString(std::string str)
{
	ZeroBitmap();
	FT_Vector DrawPoint;

	//Text anchored to the Top left of the box
	DrawPoint.x = (1) * 64.0f;
	
	int maxCharHeight = (m_face->bbox.yMax - m_face->bbox.yMin) * (m_pointSize * m_height / 72) / m_face->units_per_EM;
	maxCharHeight *= m_face->size->metrics.y_scale / m_baseScaleY * 0.8;
	DrawPoint.y = (m_height - maxCharHeight) * 64;

	FT_GlyphSlot slot = m_face->glyph;

	for (unsigned int i = 0; i < str.length(); i++)
	{
		FT_Set_Transform(m_face, nullptr, &DrawPoint);

		FT_UInt index;

		index = FT_Get_Char_Index(m_face, str[i]);

		m_errorCode = FT_Load_Glyph(m_face, index, FT_LOAD_DEFAULT);
		if (m_errorCode)
			continue;

		m_errorCode = FT_Render_Glyph(m_face->glyph, FT_RENDER_MODE_NORMAL);
		if (m_errorCode)
			continue;

		slot = m_face->glyph;

		//Draw call
		DrawToBitmap(slot->bitmap, slot->bitmap_left, m_height - slot->bitmap_top);

		DrawPoint.x += slot->advance.x;
		DrawPoint.y += slot->advance.y;
	}

	glUseProgram(m_pShader->GetProgram());
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, m_texHandle);

	glTexImage2D(GL_TEXTURE_2D, 0, GL_ALPHA, m_width, m_height, 0, GL_ALPHA, GL_UNSIGNED_BYTE, m_bitmap.get());

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

	glBindTexture(GL_TEXTURE_2D, 0);
}

void TextRender::ChangeInt(int num)
{
	ChangeString(std::to_string(num));
}

void TextRender::SetAttachedToCamera(bool isAttached)
{
	m_attachedToCamera = isAttached;
}

void TextRender::SetTextColor(vec3 color)
{
	m_textColor = color;
}

void TextRender::RegenerateBox(float width, float height, TextRender::AnchorPos anchor)
{
	//Clear verts
	m_vRenderVertices.clear();

	m_face->size->metrics.x_scale = m_baseScaleX * (1 / width);
	m_face->size->metrics.y_scale = m_baseScaleY * (1 / height);

	float halfWidth = width / 2.0f;
	float halfHeight = height / 2.0f;

	//Vertex Ids
	//TL 0
	vec3 TL(0);
	//TR 1
	vec3 TR(0);
	//BL 2
	vec3 BL(0);
	//BR 3
	vec3 BR(0);

	switch (anchor)
	{
	case AP_CENTER:
		TL = vec3(-halfWidth, halfHeight, 0);

		TR = vec3(halfWidth, halfHeight, 0);

		BL = vec3(-halfWidth, -halfHeight, 0);

		BR = vec3(halfWidth, -halfHeight, 0);
		break;
	case AP_CENTERLEFT:
		TL = vec3(0, halfHeight, 0);

		TR = vec3(width, halfHeight, 0);

		BL = vec3(0, -halfHeight, 0);

		BR = vec3(width, -halfHeight, 0);
		break;
	case AP_TOPLEFT:
		TL = vec3(0, 0, 0);

		TR = vec3(width, 0, 0);

		BL = vec3(0, -height, 0);

		BR = vec3(width, -height, 0);
		break;

	case AP_BOTTOMLEFT:
		TL = vec3(0, height, 0);

		TR = vec3(width, height, 0);

		BL = vec3(0, 0, 0);

		BR = vec3(width, 0, 0);
		break;
	}

	m_vRenderVertices.push_back(VertexFormat(
		TL.x, TL.y, TL.z,									//Position
		1, 1, 1, 1,											//Color
		0, 0,												//UV
		0, 0, -1											//Normal
	));

	m_vRenderVertices.push_back(VertexFormat(
		TR.x, TR.y, TR.z,									//Position
		1, 1, 1, 1,											//Color
		1, 0,												//UV
		0, 0, -1											//Normal
	));

	m_vRenderVertices.push_back(VertexFormat(
		BL.x, BL.y, BL.z,									//Position
		1, 1, 1, 1,											//Color
		0, 1,												//UV
		0, 0, -1											//Normal
	));

	m_vRenderVertices.push_back(VertexFormat(
		BR.x, BR.y, BR.z,									//Position
		1, 1, 1, 1,											//Color
		1, 1,												//UV
		0, 0, -1											//Normal
	));

	//Buffer attributes
	glBindBuffer(GL_ARRAY_BUFFER, m_VBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(VertexFormat) * m_vRenderVertices.size(), m_vRenderVertices.data(), GL_STATIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
}

void TextRender::ZeroBitmap()
{
	for (int i = 0; i < m_width * m_height; i++)
	{
		m_bitmap[i] = 0;
	}
}

void TextRender::DrawToBitmap(FT_Bitmap & bitmap, float left, float top)
{
	//Iterate width
	for (int X = 0; X < bitmap.width; X++)
	{
		//Iterate height
		for (int Y = 0; Y < bitmap.rows; Y++)
		{
			//check if valid
			if (X + left <= m_width && X + left >= 0 && Y + top <= m_height && Y + top >= 0)
			{
				//Get indeces
				int localIndex = ((Y + top) * m_width) + X + left;
				int givenIndex = (Y ) * bitmap.width + X;

				//Store color index
				m_bitmap[localIndex] |= bitmap.buffer[givenIndex];
			}
		}
	}
}