#pragma once
#ifndef __TEXTRENDER_H
#define __TEXTRENDER_H

#include "ft2build.h"
#include FT_FREETYPE_H

#include <string>

class TextRender
{
public:
	enum AnchorPos
	{
		AP_CENTER,
		AP_TOPLEFT,
		AP_BOTTOMLEFT,
		AP_CENTERLEFT
	};
private:

	std::unique_ptr<ShaderProgram>  m_pShader;
	GLuint							m_texHandle = 0;

	//Bitmap
	std::unique_ptr<unsigned char[]> m_bitmap;

	//Render data
	int m_width, m_height;
	float m_vAspectRatio;
	unsigned int m_pointSize;
	AnchorPos m_anchorPos;

	bool m_attachedToCamera;

	float m_boxWidth, m_boxHeight;
	float m_baseScaleX, m_baseScaleY;

	vec3 m_textColor = vec3(0);

	//World data
	vec3 m_position;
	vec3 m_rotation;

	//Freetype vars
	FT_Error	m_errorCode;
	FT_Library	m_library;
	FT_Face		m_face;

	//Pointer to camera for draw
	CameraObject * m_pCamera;
	
	//Inds and verts
	std::vector<VertexFormat>			m_vRenderVertices;
	std::vector<unsigned int>			m_vIndeces;

	//Buffer objects
	GLuint m_VBO;
	GLuint m_IBO;

	//Create new verts for box
	void RegenerateBox(float width, float height, TextRender::AnchorPos anchor);

	//Clear Bitmap
	void ZeroBitmap();

	void DrawToBitmap(FT_Bitmap & bitmap, float left, float top);

public:
	//If box height is zero it will match aspect ratio of the render resolution.
	TextRender(std::string faceName, unsigned int pointSize, unsigned int renderWidth, unsigned int renderHeight,
		float boxWidth, float boxHeight, TextRender::AnchorPos anchor, CameraObject * cam,
		const char * vert, const char * frag);
	~TextRender();

	//If height is zero it will match aspect ratio of the render resolution.
	void ChangeBoxDimensions(float width, float height, TextRender::AnchorPos anchor = TextRender::AnchorPos::AP_TOPLEFT);

	void Draw();
	void SetTranslation(vec3 pos, vec3 rot);

	void ChangeString	(std::string str);
	void ChangeInt		(int num);

	//This works exactly the opposite of how you think it should
	void SetAttachedToCamera(bool isAttached);
	void SetTextColor(vec3 color);
};

#endif