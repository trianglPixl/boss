#ifndef __PlayerObject_H__
#define __PlayerObject_H__

class PlayerObject : public GameObject
{
protected:
    PlayerController* m_pController;

    const float m_Force = 20.0f;
	const float m_AngleMult = 100.0f;

	//Used when not touching a wall
	bool	m_isDisabled = false;
	bool	m_isLeft = true;
	bool	m_chargingJump = false;
	bool	m_reachedThreshold = false;
	float	m_jumpTimer = 0.0f;
	float	m_jumpRot = 0.0f;

	GameObject * m_pArrow = nullptr;

public:
    PlayerObject(Scene* pScene, std::string name, vec3 pos, vec3 rot, vec3 scale, Mesh* pMesh, ShaderProgram* pShader, GLuint texture);
    virtual ~PlayerObject();

    void SetController(PlayerController* pController);
    virtual void Update(float deltatime);
	void HandleWallCollision();

	void SetArrow(GameObject * ar) { m_pArrow = ar; }

	bool Dead = false;
};

#endif //__PlayerObject_H__
