#include "pch.h"
#include "MenuScene.h"

MenuScene::MenuScene(std::string name, GameCore* pGame, ResourceManager* pResources)
: BaseMenuScene(name, pGame, pResources )
{
}

MenuScene::~MenuScene()
{
	m_pGame->GameScores.SaveScores("TestSave.txt");
}

void MenuScene::OnSurfaceChanged(unsigned int width, unsigned int height)
{
	BaseMenuScene::OnSurfaceChanged( width, height );
}

void MenuScene::LoadContent()
{
	BaseMenuScene::LoadContent();
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LESS);

    // create our shaders.
    {
		if (m_pResources->GetShader("Texture") == nullptr)
			m_pResources->AddShader( "Texture", new ShaderProgram( "Data/Shaders/Texture.vert", "Data/Shaders/Texture.frag" ) );

			m_pResources->AddShader("Lit", new ShaderProgram("Data/Shaders/Complex.vert", "Data/Shaders/Complex.frag"));

			m_pResources->AddShader("Depth", new ShaderProgram("Data/Shaders/Misc/Position.vert", "Data/Shaders/Misc/DepthColor.frag"));

			SetRenderPointShader(m_pResources->GetShader("Depth"));
		
    }

    // Create meshes.
    {
		m_pResources->AddMesh("MENUBG", Mesh::CreatePlane(vec2((int)m_pGame->GetWindowWidth(), (int)m_pGame->GetWindowWidth()), ivec2(4, 4), vec2(0.0f,0.0f), vec2(4,4)));
    }
    
    // load our textures.
    {
		m_pResources->AddTexture( "BGTex", LoadTexture("Data/Textures/SourceTextures/tileable-metal-textures-8ad.png"));
		m_pResources->AddTexture( "BGNorm", LoadTexture("Data/Textures/SourceTextures/tileable-metal-textures-8NM.png"));
    }

    // create some game objects.
    {
		m_pGameObjects["Background"] = new GameObject(this, "Background", vec3( -(int)m_pGame->GetWindowWidth() / 2.0f, -(int)m_pGame->GetWindowWidth() / 2.0f, 400.0f), vec3(90,0,0), vec3(1,1,1), m_pResources->GetMesh("MENUBG"),
			m_pResources->GetShader("Lit"), m_pResources->GetTexture("BGTex"), m_pResources->GetTexture("BGNorm"));


        // camera
		{
			m_pGameObjects["Camera"] = new CameraObject(this, "Camera", vec3(0, 0, -40), vec3(0, 0, 0), 5.0f, 0, 0, 0);
			((CameraObject *)m_pGameObjects["Camera"])->SetController(m_pGame->GetController(1));

		}
		float rot = -45.0f;


		m_lights[0].ambient = vec4(1.0f, 1.0f, 1.0f, 0.01f);
		m_lights[0].diffuse = vec4(0.8f, 0.8f, 0.8f, 1.0f);
		m_lights[0].specular = vec4(0.8f, 0.8f, 0.8f, 1.0f);
		m_lights[0].position = vec4(0.0f, 800.0f, 0.0f, 1.0f);
		m_lights[0].direction = vec3(0.0f, sinf(rot), cosf(rot));
		m_lights[0].angle = 60.0f; 
		m_lights[0].angleCos = 0.5f;
		m_lights[0].lightRadius = 0.0f;
		m_lights[0].attenuationCutoff = 1000.0f;

		ACTIVELIGHTS = 1;

		m_numButtons = 2;
		m_currentButton = 1;

		//Cursor
		cursor.reset(new MenuSlate(vec3(-10, -7.2f, 10.0f), 0, 1, (CameraObject *)m_pGameObjects["Camera"], 4, 2,
			"Data/Textures/CursorArrow.png", "Data/Shaders/Texture.vert", "Data/Shaders/Texture.frag"));

		//Play Button
		playButton.reset(new PlayButton(vec3(-10, -7.2f, 10.0f), 0.0f, 2.0f, ((CameraObject *)m_pGameObjects["Camera"]), "LightingScene", 
			"Data/Textures/Button.png", "Data/Shaders/Texture.vert", "Data/Shaders/Texture.frag", PlayerControllerButtons::PCB_NULL));
		playButton->SetController(m_pGame->GetController(1));

		playButton->UseText(vec3(-8.6f, -7.6f, 10.0f), 72, 1024, "PLAY", vec3(1,1,1));

		//Quit Button
		quitButton.reset(new QuitButton(vec3(4.65f, -7.2f, 10.0f), 0.0f, 2.0f, ((CameraObject *)m_pGameObjects["Camera"]), "LightingScene",
			"Data/Textures/Button.png", "Data/Shaders/Texture.vert", "Data/Shaders/Texture.frag", PlayerControllerButtons::PCB_NULL));
		quitButton->SetController(m_pGame->GetController(1));

		quitButton->UseText(vec3(6.1f, -7.6f, 10.0f), 72, 1024, "QUIT", vec3(1, 1, 1));

		//Create score pane
		scorePane.reset(new MenuSlate(vec3(-4, -2, 4.0f), 0, 1, (CameraObject *)m_pGameObjects["Camera"], 
			8 , 4 ,"Data/Textures/MenuPane.png", "Data/Shaders/Texture.vert", "Data/Shaders/Texture.frag"));

		scoreText.reset(new TextRender("Data/TypeFaces/Amble-Regular.ttf", 24, 1024, 1024, 3, 1, TextRender::AP_BOTTOMLEFT, ((CameraObject *)m_pGameObjects["Camera"]),
			"Data/Shaders/Text.vert",
			"Data/Shaders/Text.frag"));
		scoreText->SetAttachedToCamera(false);
		scoreText->SetTextColor(vec3(1, 1, 1));
		scoreText->ChangeString("HIGHSCORES");
		scoreText->SetTranslation(vec3(-0.9f, 0.5f,3), vec3(0));

		//Load Scores cJSON
		m_pGame->GameScores.LoadScores("TestSave.txt");


		//Setup Player data

		player1Name.reset(new TextRender("Data/TypeFaces/Amble-Regular.ttf", 20, 1024, 1024, 4, 1, TextRender::AP_BOTTOMLEFT, ((CameraObject *)m_pGameObjects["Camera"]),
			"Data/Shaders/Text.vert",
			"Data/Shaders/Text.frag"));
		player1Name->SetAttachedToCamera(false);
		player1Name->SetTextColor(vec3(1, 1, 1));
		player1Name->ChangeString("1: " + m_pGame->GameScores.Players[0].name);
		player1Name->SetTranslation(vec3(-2.5f, 0.0f, 3), vec3(0));

		player1Score.reset(new TextRender("Data/TypeFaces/Amble-Regular.ttf", 20, 1024, 1024, 4, 1, TextRender::AP_BOTTOMLEFT, ((CameraObject *)m_pGameObjects["Camera"]),
			"Data/Shaders/Text.vert",
			"Data/Shaders/Text.frag"));
		player1Score->SetAttachedToCamera(false);
		player1Score->SetTextColor(vec3(1, 1, 1));
		player1Score->ChangeString(std::to_string(m_pGame->GameScores.Players[0].score));
		player1Score->SetTranslation(vec3(1.0f, 0.0f, 3), vec3(0));

		player2Name.reset(new TextRender("Data/TypeFaces/Amble-Regular.ttf", 20, 1024, 1024, 4, 1, TextRender::AP_BOTTOMLEFT, ((CameraObject *)m_pGameObjects["Camera"]),
			"Data/Shaders/Text.vert",
			"Data/Shaders/Text.frag"));
		player2Name->SetAttachedToCamera(false);
		player2Name->SetTextColor(vec3(1, 1, 1));
		player2Name->ChangeString("2: " + m_pGame->GameScores.Players[1].name);
		player2Name->SetTranslation(vec3(-2.5f, -0.4f, 3), vec3(0));

		player2Score.reset(new TextRender("Data/TypeFaces/Amble-Regular.ttf", 20, 1024, 1024, 4, 1, TextRender::AP_BOTTOMLEFT, ((CameraObject *)m_pGameObjects["Camera"]),
			"Data/Shaders/Text.vert",
			"Data/Shaders/Text.frag"));
		player2Score->SetAttachedToCamera(false);
		player2Score->SetTextColor(vec3(1, 1, 1));
		player2Score->ChangeString(std::to_string(m_pGame->GameScores.Players[1].score));
		player2Score->SetTranslation(vec3(1.0f, -0.4f, 3), vec3(0));

		player3Name.reset(new TextRender("Data/TypeFaces/Amble-Regular.ttf", 20, 1024, 1024, 4, 1, TextRender::AP_BOTTOMLEFT, ((CameraObject *)m_pGameObjects["Camera"]),
			"Data/Shaders/Text.vert",
			"Data/Shaders/Text.frag"));
		player3Name->SetAttachedToCamera(false);
		player3Name->SetTextColor(vec3(1, 1, 1));
		player3Name->ChangeString("3: " + m_pGame->GameScores.Players[2].name);
		player3Name->SetTranslation(vec3(-2.5f, -0.8f, 3), vec3(0));

		player3Score.reset(new TextRender("Data/TypeFaces/Amble-Regular.ttf", 20, 1024, 1024, 4, 1, TextRender::AP_BOTTOMLEFT, ((CameraObject *)m_pGameObjects["Camera"]),
			"Data/Shaders/Text.vert",
			"Data/Shaders/Text.frag"));
		player3Score->SetAttachedToCamera(false);
		player3Score->SetTextColor(vec3(1, 1, 1));
		player3Score->ChangeString(std::to_string(m_pGame->GameScores.Players[2].score));
		player3Score->SetTranslation(vec3(1.0f, -0.8f, 3), vec3(0));

		player4Name.reset(new TextRender("Data/TypeFaces/Amble-Regular.ttf", 20, 1024, 1024, 4, 1, TextRender::AP_BOTTOMLEFT, ((CameraObject *)m_pGameObjects["Camera"]),
			"Data/Shaders/Text.vert",
			"Data/Shaders/Text.frag"));
		player4Name->SetAttachedToCamera(false);
		player4Name->SetTextColor(vec3(1, 1, 1));
		player4Name->ChangeString("4: " + m_pGame->GameScores.Players[3].name);
		player4Name->SetTranslation(vec3(-2.5f, -1.2f, 3), vec3(0));

		player4Score.reset(new TextRender("Data/TypeFaces/Amble-Regular.ttf", 20, 1024, 1024, 4, 1, TextRender::AP_BOTTOMLEFT, ((CameraObject *)m_pGameObjects["Camera"]),
			"Data/Shaders/Text.vert",
			"Data/Shaders/Text.frag"));
		player4Score->SetAttachedToCamera(false);
		player4Score->SetTextColor(vec3(1, 1, 1));
		player4Score->ChangeString(std::to_string(m_pGame->GameScores.Players[3].score));
		player4Score->SetTranslation(vec3(1.0f, -1.2f, 3), vec3(0));

		player5Name.reset(new TextRender("Data/TypeFaces/Amble-Regular.ttf", 20, 1024, 1024, 4, 1, TextRender::AP_BOTTOMLEFT, ((CameraObject *)m_pGameObjects["Camera"]),
			"Data/Shaders/Text.vert",
			"Data/Shaders/Text.frag"));
		player5Name->SetAttachedToCamera(false);
		player5Name->SetTextColor(vec3(1, 1, 1));
		player5Name->ChangeString("5: " + m_pGame->GameScores.Players[4].name);
		player5Name->SetTranslation(vec3(-2.5f, -1.6f, 3), vec3(0));

		player5Score.reset(new TextRender("Data/TypeFaces/Amble-Regular.ttf", 20, 1024, 1024, 4, 1, TextRender::AP_BOTTOMLEFT, ((CameraObject *)m_pGameObjects["Camera"]),
			"Data/Shaders/Text.vert",
			"Data/Shaders/Text.frag"));
		player5Score->SetAttachedToCamera(false);
		player5Score->SetTextColor(vec3(1, 1, 1));
		player5Score->ChangeString(std::to_string(m_pGame->GameScores.Players[4].score));
		player5Score->SetTranslation(vec3(1.0f, -1.6f, 3), vec3(0));


		
    }
}

void MenuScene::Restart()
{
	Scene::Restart();

	player1Name->ChangeString("1: " + m_pGame->GameScores.Players[0].name);
	player1Score->ChangeString(std::to_string(m_pGame->GameScores.Players[0].score));

	player2Name->ChangeString("2: " + m_pGame->GameScores.Players[1].name);
	player2Score->ChangeString(std::to_string(m_pGame->GameScores.Players[1].score));

	player3Name->ChangeString("3: " + m_pGame->GameScores.Players[2].name);
	player3Score->ChangeString(std::to_string(m_pGame->GameScores.Players[2].score));

	player4Name->ChangeString("4: " + m_pGame->GameScores.Players[3].name);
	player4Score->ChangeString(std::to_string(m_pGame->GameScores.Players[3].score));

	player5Name->ChangeString("5: " + m_pGame->GameScores.Players[4].name);
	player5Score->ChangeString(std::to_string(m_pGame->GameScores.Players[4].score));
}

void MenuScene::Update(float deltatime)
{
	BaseMenuScene::Update( deltatime );

	RenderShadowmaps(1024);
    
	playButton->Update(deltatime);
	quitButton->Update(deltatime);
	scorePane->Update(deltatime);
	cursor->Update(deltatime);

	
	switch (m_currentButton)
	{
	case 1:
	{
		if (m_pGame->GetController(1)->IsButtonJustPressed(PCB_ENTER))
			playButton->Press();

		vec3 newPos = playButton->GetPosition();

		newPos.x -= (playButton->GetButtonWidth() * playButton->GetScale()) / 1.5f;

		cursor->SetPosition(newPos);
	}
		break;
			
	case 2:
		if (m_pGame->GetController(1)->IsButtonJustPressed(PCB_ENTER))
			quitButton->Press();

		vec3 newPos = quitButton->GetPosition();

		newPos.x -= (quitButton->GetButtonWidth() * quitButton->GetScale()) / 1.5f;

		cursor->SetPosition(newPos);
		break;
	}
	
}

void MenuScene::Draw()
{
	BaseMenuScene::Draw();
	scorePane->Draw();
	scoreText->Draw();
	playButton->Draw();
	quitButton->Draw();
	cursor->Draw();

	player1Name->Draw();
	player1Score->Draw();
	player2Name->Draw();
	player2Score->Draw();
	player3Name->Draw();
	player3Score->Draw();
	player4Name->Draw();
	player4Score->Draw();
	player5Name->Draw();
	player5Score->Draw();
}