#pragma once
#include "pch.h"

enum SoundGroups
{
	SoundGroup_Music = 0,
	SoundGroup_Effects =1,
};

class SoundManager
{
public:
	SoundManager();
	~SoundManager();

	void StartUp();
	int PlaySound(Mix_Chunk* sound, int loops = 0);
	void StopSound(int channel);
private:
	int audio_rate;
	Uint16 audio_format;
	int audio_channels;
	int audio_buffers;
};
