#include "pch.h"
#include "SoundManager.h"

SoundManager::SoundManager()
{
}

SoundManager::~SoundManager()
{
}

void SoundManager::StartUp()
{
	audio_rate = 44100;
	audio_format = MIX_DEFAULT_FORMAT;
	audio_channels = 2;
	audio_buffers = 512;

	SDL_Init(SDL_INIT_AUDIO);

	assert(Mix_OpenAudio(audio_rate, audio_format, audio_channels, audio_buffers) == 0);


	int numchannels = Mix_GroupCount(-1);

	Mix_GroupChannel(0, SoundGroup_Music);
	Mix_GroupChannels(1, numchannels - 1, SoundGroup_Effects);
}

int SoundManager::PlaySound(Mix_Chunk* sound, int loops)
{
	int channel = Mix_GroupAvailable(SoundGroup_Effects);

	if (channel == -1)
	{
		channel = Mix_GroupOldest(SoundGroup_Effects);
	}

	return Mix_PlayChannel(channel, sound, loops);
}

void SoundManager::StopSound(int channel)
{
	if (channel != NULL)
	{
		Mix_HaltChannel(channel);
	}
}