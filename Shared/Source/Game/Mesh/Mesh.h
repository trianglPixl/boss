#ifndef __Mesh_H__
#define __Mesh_H__
#include "../GameObjects/Light.h"

class Mesh
{
protected:
    GLuint m_VBO;
    GLuint m_IBO;

    unsigned int m_NumVerts;
    unsigned int m_NumIndices;

    GLenum m_PrimitiveType;

    unsigned int m_RenderOrder;

	//Shader output arrays
	vec4	ambient[MAXLIGHTS];
	vec4	diffuse[MAXLIGHTS];
	vec4	specular[MAXLIGHTS];
	vec4	position[MAXLIGHTS];
	vec3	direction[MAXLIGHTS];
	float	angle[MAXLIGHTS];
	float	angleCos[MAXLIGHTS];
	float	lightRadius[MAXLIGHTS];
	float	attenuationCutoff[MAXLIGHTS];
	GLuint  shadowMap[MAXLIGHTS];
	int     shadowIndeces[MAXLIGHTS];;

public:
    Mesh();
    virtual ~Mesh();

    unsigned int GetRenderOrder() { return m_RenderOrder; }

    virtual void Init(VertexFormat* verts, int numverts, unsigned int* indices, int numindices, GLenum usage);
    virtual void Init(const void* verts, int numverts, int vertexbytesize, GLenum usage);

    virtual void SetupAttributes(ShaderProgram* pShaderProgram);
    virtual void SetupUniforms(ShaderProgram* pShaderProgram, vec3 scale, vec3 rot, vec3 pos, mat4 viewMat, mat4 projMat, GLuint texturehandle, vec4 color, int rippleCount = 0, vec3* ripplePoints = nullptr, float* rippleLifetimes = nullptr);
	virtual void SetupUniforms(ShaderProgram* pShaderProgram, vec3 scale, vec3 rot, vec3 pos, mat4 viewMat, mat4 projMat, GLuint texturehandle, vec4 color, float startTime);

	//Lighting
	virtual void SetupUniforms(ShaderProgram* pShaderProgram, vec3 scale, vec3 rot, vec3 pos, mat4 viewMat, mat4 projMat, 
		vec4 color, GLuint texturehandle, GLuint textureNormalHandle, Light * lights, unsigned int numlights, vec4 matAmb, vec4 matSpec, float matShine);
	virtual void SetupUniforms(ShaderProgram* pShaderProgram, vec3 scale, vec3 rot, vec3 pos, vec3 lightPos, mat4 viewMat, mat4 projMat,
		vec4 color, GLuint texturehandle, GLuint textureNormalHandle, Light * lights, unsigned int numlights, vec4 matAmb, vec4 matSpec, float matShine);

    virtual void Draw(ShaderProgram* pShaderProgram);

    void SetRenderOrder(int renderorder) { m_RenderOrder = renderorder; }

    // defined in MeshShapes.cpp 
    static Mesh* CreateBox(vec2 size);
	static Mesh* CreateBoxWithOffset(vec2 size, vec2 offset);
	static Mesh* CreateCube(vec3 size, bool invertFaces = false);
	static Mesh* CreatePlane(vec2 size, ivec2 vertCount, vec2 initUVOffset, vec2 UVMax);
	static Mesh* LoadOBJ(const char* filename, bool optimize = false, bool saveOptimized = false);
	static Mesh* LoadSDM(const char* filename);
};

#endif //__Game_H__
