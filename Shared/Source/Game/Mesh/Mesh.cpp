#include "pch.h"
#include "Mesh.h"

Mesh::Mesh()
{
    m_VBO = 0;
    m_IBO = 0;
    m_RenderOrder = 0;
    m_PrimitiveType = GL_TRIANGLES;
}

Mesh::~Mesh()
{
    glDeleteBuffers( 1, &m_VBO );
    glDeleteBuffers( 1, &m_IBO );
}

//Calculate TBN for each triangle
void Mesh::Init(VertexFormat* verts, int numverts, unsigned int* indices, int numindices, GLenum usage)
{
    m_NumVerts = numverts;
    m_NumIndices = numindices;

	for (int i = 0; i < numindices;)
	{
		vec3 edge1 = verts[indices[i + 1]].pos - verts[indices[i]].pos;
		vec3 edge2 = verts[indices[i + 2]].pos - verts[indices[i]].pos;
		vec2 uvdiff1 = verts[indices[i + 1]].uv - verts[indices[i]].uv;
		vec2 uvdiff2 = verts[indices[i + 2]].uv - verts[indices[i]].uv;

		float inverse = 1.0f / (uvdiff1.x * uvdiff2.y - uvdiff2.x * uvdiff1.y);
		vec3 tangent;
		tangent.x = inverse * (uvdiff2.y * edge1.x - uvdiff1.y * edge2.x);
		tangent.y = inverse * (uvdiff2.y * edge1.y - uvdiff1.y * edge2.y);
		tangent.z = inverse * (uvdiff2.y * edge1.z - uvdiff1.y * edge2.z);

		tangent.Normalize();

		verts[indices[i]].tangent = tangent;
		verts[indices[i + 1]].tangent = tangent;
		verts[indices[i + 2]].tangent = tangent;

		i += 3;
	}

    // gen and fill buffer with our vertex attributes.
    if( m_VBO == 0 )
        glGenBuffers( 1, &m_VBO );
    glBindBuffer( GL_ARRAY_BUFFER, m_VBO );
    glBufferData( GL_ARRAY_BUFFER, sizeof(VertexFormat)*numverts, verts, usage );

    // gen and fill buffer with our indices.
    if( m_IBO == 0 )
        glGenBuffers( 1, &m_IBO );
    glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, m_IBO );
    glBufferData( GL_ELEMENT_ARRAY_BUFFER, sizeof(GLuint)*numindices, indices, usage );

    CheckForGLErrors();
}

void Mesh::Init(const void* verts, int numverts, int vertexbytesize, GLenum usage)
{
    m_NumVerts = numverts;
    m_NumIndices = 0;

    // gen and fill buffer with our vertex attributes.
    if( m_VBO == 0 )
        glGenBuffers( 1, &m_VBO );
    glBindBuffer( GL_ARRAY_BUFFER, m_VBO );
    glBufferData( GL_ARRAY_BUFFER, vertexbytesize, verts, usage );

    CheckForGLErrors();
}

void Mesh::SetupAttributes(ShaderProgram* pShaderProgram)
{
    assert( m_VBO != 0 );

    // bind our vertex and index buffers.
    glBindBuffer( GL_ARRAY_BUFFER, m_VBO );
    if( m_IBO != 0 )
        glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, m_IBO );

    GLuint programhandle = pShaderProgram->GetProgram();

    // get the attribute locations.
    GLint aPos = glGetAttribLocation( programhandle, "a_Position" );
    GLint aColor = glGetAttribLocation( programhandle, "a_Color" );
    GLint aUV = glGetAttribLocation( programhandle, "a_UV" );
    GLint aNormal = glGetAttribLocation( programhandle, "a_Normal" );
	GLint aTangent = glGetAttribLocation( programhandle, "a_Tangent");

    // setup our vbo and attributes.
    {
        // setup the position attribute.
        assert( aPos != -1 );
        glVertexAttribPointer( aPos, 3, GL_FLOAT, GL_FALSE, sizeof(VertexFormat), (void*)offsetof(VertexFormat, pos) );
        glEnableVertexAttribArray( aPos );

        // setup the color attribute.
        if( aColor != -1 )
        {
            glVertexAttribPointer( aColor, 4, GL_UNSIGNED_BYTE, GL_TRUE, sizeof(VertexFormat), (void*)offsetof(VertexFormat, color) );
            glEnableVertexAttribArray( aColor );
        }

        // setup the uv attribute.
        if( aUV != -1 )
        {
            glVertexAttribPointer( aUV, 2, GL_FLOAT, GL_FALSE, sizeof(VertexFormat), (void*)offsetof(VertexFormat, uv) );
            glEnableVertexAttribArray( aUV );
        }

        // setup the normal attribute.
        if( aNormal != -1 )
        {
            glVertexAttribPointer( aNormal, 3, GL_FLOAT, GL_FALSE, sizeof(VertexFormat), (void*)offsetof(VertexFormat, normal) );
            glEnableVertexAttribArray( aNormal );
        }

		if(aTangent != -1)
		{
			glVertexAttribPointer(aTangent, 3, GL_FLOAT, GL_FALSE, sizeof(VertexFormat), (void*)offsetof(VertexFormat, tangent));
			glEnableVertexAttribArray(aTangent);
		}
    }
}

//Will need to pass in ripple points then modify the unifrom if statements
void Mesh::SetupUniforms(ShaderProgram* pShaderProgram, vec3 scale, vec3 rot, vec3 pos, mat4 viewMat, mat4 projMat, GLuint texturehandle, vec4 color, int rippleCount, vec3* ripplePoints, float* rippleLifetimes)
{
    GLuint programhandle = pShaderProgram->GetProgram();

    // enable our shader program.
    glUseProgram( programhandle );

    // get the uniform locations.
	GLint uMVP = glGetUniformLocation(programhandle, "u_MVP");

	//Matrices
	GLint uModel = glGetUniformLocation(programhandle, "u_Model");
	GLint uView = glGetUniformLocation(programhandle, "u_View");
	GLint uProj = glGetUniformLocation(programhandle, "u_Projection");
	GLint uModelView = glGetUniformLocation(programhandle, "u_ModelView");

	//CubeMap
	GLint uCubeMap = glGetUniformLocation(programhandle, "u_TextureCube");


	//For converting to and from model space if needed.
	GLint uWM = glGetUniformLocation(programhandle, "u_WorldMatrix");

	GLint uRippleArr = glGetUniformLocation(programhandle, "u_RipplePoints");
	GLint uRippleCount = glGetUniformLocation(programhandle, "u_NumFilled");
    GLint uRippleLifetimes = glGetUniformLocation(programhandle, "u_RippleLifetimes");

	GLint uTexture = glGetUniformLocation( programhandle, "u_Texture" );
    GLint uColor = glGetUniformLocation( programhandle, "u_Color" );
    GLint uTime = glGetUniformLocation( programhandle, "u_Time" );
	CheckForGLErrors();
    // setup the texture.
    if( texturehandle != 0 && uTexture != -1 )
    {
        glActiveTexture( GL_TEXTURE0 + 0 );
        glBindTexture( GL_TEXTURE_2D, texturehandle );
        glUniform1i( uTexture, 0 );
    }
	CheckForGLErrors();
	if (texturehandle != 0 && uCubeMap != -1)
	{
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_CUBE_MAP, texturehandle);
		glUniform1i(uCubeMap, 0);
	}

	if (uMVP != -1)
	{
		mat4 ModelMat;
		ModelMat.CreateSRT(scale, rot, pos);
		mat4 MVP = projMat * viewMat * ModelMat;
		glUniformMatrix4fv(uMVP, 1, GL_FALSE, &MVP.m11);
	}
	if (uModelView != -1)
	{
		mat4 MV;
		MV.CreateSRT(scale, rot, pos);
		MV = MV * viewMat;
		glUniformMatrix4fv(uModelView, 1, GL_FALSE, &MV.m11);
	}
	if (uModel != -1)
	{
		mat4 MV;
		MV.CreateSRT(scale, rot, pos);
		glUniformMatrix4fv(uModel, 1, GL_FALSE, &MV.m11);
	}

	if (uProj != -1)
	{
		glUniformMatrix4fv(uProj, 1, GL_FALSE, &projMat.m11);
	}
	CheckForGLErrors();
	if (uView != -1)
	{
		glUniformMatrix4fv(uView, 1, GL_FALSE, &viewMat.m11);
	}

	if (uWM != -1)
	{
		mat4 mMat;
		mMat.CreateSRT(scale, rot, pos);
		glUniformMatrix4fv(uWM, 1, GL_FALSE, &mMat.m11);
	}

	if (uRippleArr != -1)
	{
		//vec3 points[2] = { vec3(50,0,50), vec3(100,0,100)};
		glUniform3fv(uRippleArr, rippleCount, &ripplePoints[0].x);
	}

	if (uRippleCount != -1)
	{
		glUniform1i(uRippleCount, rippleCount);
	}

    if (uRippleLifetimes != -1)
    {
        glUniform1fv(uRippleLifetimes, rippleCount, rippleLifetimes);
    }

	if( uColor != -1 )
    {
        glUniform4f( uColor, color.x, color.y, color.z, color.w );
    }

    // setup time.
    if( uTime != -1 )
    {
        glUniform1f( uTime, (float)MyGetSystemTime() );
    }

    CheckForGLErrors();
}

void Mesh::SetupUniforms(ShaderProgram * pShaderProgram, vec3 scale, vec3 rot, vec3 pos, mat4 viewMat, mat4 projMat, GLuint texturehandle, vec4 color, float startTime)
{
	GLuint programhandle = pShaderProgram->GetProgram();
	glUseProgram(programhandle);

	GLint bt = glGetUniformLocation(programhandle, "u_StartTime");

	if (bt != -1)
	{
		glUniform1f(bt, startTime);
	}

	CheckForGLErrors();

	SetupUniforms(pShaderProgram, scale, rot, pos, viewMat, projMat, texturehandle, color);
}

void Mesh::SetupUniforms(ShaderProgram * pShaderProgram, vec3 scale, vec3 rot, vec3 pos, mat4 viewMat, 
	mat4 projMat, vec4 color, GLuint texturehandle, GLuint textureNormalHandle, Light * lights, 
	unsigned int numlights, vec4 matAmb, vec4 matSpec, float matShine)
{
	GLuint programhandle = pShaderProgram->GetProgram();
	glUseProgram(programhandle);

	//NormalMap
	GLint uNTexture = glGetUniformLocation(programhandle, "u_NormalMap");


	//NumLights
	GLint uLnumLights = glGetUniformLocation(programhandle, "u_ActiveLights");

	//Lights
	GLint uLambient = glGetUniformLocation(programhandle, "u_lAmbient");
	GLint uLdiffuse = glGetUniformLocation(programhandle, "u_lDiffuse");
	GLint uLspecular = glGetUniformLocation(programhandle, "u_lSpecular");
	GLint uLposition = glGetUniformLocation(programhandle, "u_lPosition");
	GLint uLdirection = glGetUniformLocation(programhandle, "u_lDirection");
	GLint uLangle = glGetUniformLocation(programhandle, "u_lAngle");
	GLint uLangleCos = glGetUniformLocation(programhandle, "u_lAngleCos");
	GLint uLlightrad = glGetUniformLocation(programhandle, "u_lLightRadius");
	GLint uLattenCu = glGetUniformLocation(programhandle, "u_lAttenuationCutoff");
	//ShadowMap
	GLint uShadowMap = glGetUniformLocation(programhandle, "u_lShadowMap");


	//Material values
	GLint uLmatAmb = glGetUniformLocation(programhandle, "u_matAmbient");
	GLint uLmatSpec = glGetUniformLocation(programhandle, "u_matSpecular");
	GLint uLmatShine = glGetUniformLocation(programhandle, "u_matShine");

	if (uNTexture != -1)
	{
		glActiveTexture(GL_TEXTURE0 + 1);
		glBindTexture(GL_TEXTURE_2D, textureNormalHandle);
		glUniform1i(uNTexture, 1);
	}

	for (unsigned int i = 0; i < numlights; i++)
	{
		ambient[i]			 = lights[i].ambient;
		diffuse[i]			 = lights[i].diffuse;
		specular[i]			 = lights[i].specular;
		position[i]			 = lights[i].position;
		direction[i]		 = lights[i].direction;
		angle[i]			 = lights[i].angle;
		angleCos[i]			 = lights[i].angleCos;
		lightRadius[i]		 = lights[i].lightRadius;
		attenuationCutoff[i] = lights[i].attenuationCutoff;
		shadowMap[i]         = lights[i].fbo.GetColorTextureHandle();
	}

	if (uLnumLights != -1)
	{
		glUniform1i(uLnumLights, numlights);
	}
	CheckForGLErrors();
	if (uShadowMap != -1)
	{
		char uniformNameBuffer[64];
		for (unsigned int i = 0; i < 8; i++)
		{
			glActiveTexture(GL_TEXTURE0 + i + 2);
			glBindTexture(GL_TEXTURE_CUBE_MAP, shadowMap[i]);
			CheckForGLErrors();
			snprintf(uniformNameBuffer, 64, "u_lShadowMap[%d]", i);
			GLuint uniformLocation = glGetUniformLocation(programhandle, uniformNameBuffer);
			assert(uniformLocation != -1);
			glUniform1i(uniformLocation, i + 2);
		}
		/*glActiveTexture(GL_TEXTURE0 + 2 );
		glBindTexture(GL_TEXTURE_CUBE_MAP, shadowMap[0]);

		glUniform1i(uShadowMap, 2);*/
	}
	CheckForGLErrors();
	if (uLambient != -1)
	{
		glUniform4fv(uLambient, numlights, &ambient[0].x);
	}

	if (uLdiffuse != -1)
	{
		glUniform4fv(uLdiffuse, numlights, &diffuse[0].x);
	}

	if (uLspecular != -1)
	{
		glUniform4fv(uLspecular, numlights, &specular[0].x);
	}

	if (uLposition != -1)
	{
		glUniform4fv(uLposition, numlights, &position[0].x);
	}

	if (uLdirection != -1)
	{
		glUniform3fv(uLdirection, numlights, &direction[0].x);
	}

	if (uLangle != -1)
	{
		glUniform1fv(uLangle, numlights, &angle[0]);
	}

	if (uLangleCos != -1)
	{
		glUniform1fv(uLangleCos, numlights, &angleCos[0]);
	}

	if (uLlightrad != -1)
	{
		glUniform1fv(uLlightrad, numlights, &lightRadius[0]);
	}

	if (uLattenCu != -1)
	{
		glUniform1fv(uLattenCu, numlights, &attenuationCutoff[0]);
	}

	if (uLmatAmb != -1)
	{
		glUniform4f(uLmatAmb, matAmb.x, matAmb.y, matAmb.z, matAmb.w);
	}

	if (uLmatSpec != -1)
	{
		glUniform4f(uLmatSpec, matSpec.x, matSpec.y, matSpec.z, matSpec.w);
	}

	if (uLmatShine != -1)
	{
		glUniform1f(uLmatShine, matShine);
	}

	CheckForGLErrors();

	SetupUniforms(pShaderProgram, scale, rot, pos, viewMat, projMat, texturehandle, color);
}

void Mesh::SetupUniforms(ShaderProgram * pShaderProgram, vec3 scale, vec3 rot, vec3 pos, vec3 cameraPos,
	mat4 viewMat, mat4 projMat, vec4 color, GLuint texturehandle, GLuint textureNormalHandle, Light * lights,
	unsigned int numlights, vec4 matAmb, vec4 matSpec, float matShine)
{
	GLuint programhandle = pShaderProgram->GetProgram();
	glUseProgram(programhandle);

	//NormalMap
	GLint uCameraPosLocation = glGetUniformLocation(programhandle, "u_CameraPosition");

	if (uCameraPosLocation != -1)
	{
		glUniform3f(uCameraPosLocation, cameraPos.x, cameraPos.y, cameraPos.z);
	}

	SetupUniforms(pShaderProgram, scale, rot, pos, viewMat, projMat, color, texturehandle, textureNormalHandle, lights, numlights, matAmb, matSpec, matShine);
}

void Mesh::Draw(ShaderProgram* pShaderProgram)
{
    GLuint programhandle = pShaderProgram->GetProgram();
	// enable our shader program.
	glUseProgram(programhandle);
	CheckForGLErrors();
    // draw the shape.
    if( m_NumIndices > 0 )
    {
        glDrawElements( m_PrimitiveType, m_NumIndices, GL_UNSIGNED_INT, 0 );
    }
    else
    {
        glDrawArrays( m_PrimitiveType, 0, m_NumVerts );
    }
	CheckForGLErrors();
    GLint aPos = glGetAttribLocation( programhandle, "a_Position" );
    GLint aColor = glGetAttribLocation( programhandle, "a_Color" );
    GLint aUV = glGetAttribLocation( programhandle, "a_UV" );
    GLint aNormal = glGetAttribLocation( programhandle, "a_Normal" );
	 CheckForGLErrors();
    // disable the attribute arrays used
    glDisableVertexAttribArray( aPos );

    if( aColor != -1 )
        glDisableVertexAttribArray( aColor );

    if( aUV != -1 )
        glDisableVertexAttribArray( aUV );

    if( aNormal != -1 )
        glDisableVertexAttribArray( aNormal );

    CheckForGLErrors();
}
