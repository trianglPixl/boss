#include "pch.h"
#include <vector>
#include <list>
#include <fstream>
#include <sstream>

#define OBJ_INVERT_YZ 1
//MUST BE CW!

Mesh* Mesh::CreateBox(vec2 size)
{
    VertexFormat verts[4];

    verts[0] = VertexFormat( -size.x/2, -size.y/2, 0, 255,255,255,255, 0,0, 0,0,0 );
    verts[1] = VertexFormat( -size.x/2,  size.y/2, 0, 255,255,255,255, 0,1, 0,0,0 );
    verts[2] = VertexFormat(  size.x/2,  size.y/2, 0, 255,255,255,255, 1,1, 0,0,0 );
    verts[3] = VertexFormat(  size.x/2, -size.y/2, 0, 255,255,255,255, 1,0, 0,0,0 );

    unsigned int indices[6] = { 0,1,2, 0,2,3 };

    Mesh* pMesh = new Mesh();
    pMesh->Init( verts, 4, indices, 6, GL_STATIC_DRAW );

    return pMesh;
};

Mesh* Mesh::CreateBoxWithOffset(vec2 size, vec2 offset)
{
	VertexFormat verts[4];

	verts[0] = VertexFormat(-size.x / 2 + offset.x, -size.y / 2 + offset.y, 0, 255, 255, 255, 255, 0, 0, 0, 0, 0);
	verts[1] = VertexFormat(-size.x / 2 + offset.x, size.y / 2 + offset.y, 0, 255, 255, 255, 255, 0, 1, 0, 0, 0);
	verts[2] = VertexFormat(size.x / 2 + offset.x, size.y / 2 + offset.y, 0, 255, 255, 255, 255, 1, 1, 0, 0, 0);
	verts[3] = VertexFormat(size.x / 2 + offset.x, -size.y / 2 + offset.y, 0, 255, 255, 255, 255, 1, 0, 0, 0, 0);

	unsigned int indices[6] = { 0,1,2, 0,2,3 };

	Mesh* pMesh = new Mesh();
	pMesh->Init(verts, 4, indices, 6, GL_STATIC_DRAW);

	return pMesh;
};

Mesh* Mesh::CreateCube(vec3 size, bool invertFaces)
{
	VertexFormat verts[24];

	//Face Front
	verts[0] = VertexFormat( -size.x / 2, size.y / 2, -size.z / 2, 255, 255, 255, 255, 1.0f / 4.0f, 1.0f / 2.0f, 0.0f, 0.0f, -1.0f);
	verts[1] = VertexFormat(  size.x / 2, size.y / 2, -size.z / 2, 255, 255, 255, 255, 1.0f / 2.0f, 1.0f / 2.0f, 0.0f, 0.0f, -1.0f);
	verts[2] = VertexFormat( -size.x / 2,-size.y / 2, -size.z / 2, 255, 255, 255, 255, 1.0f / 4.0f, 1.0f / 4.0f, 0.0f, 0.0f, -1.0f);
	verts[3] = VertexFormat(  size.x / 2,-size.y / 2, -size.z / 2, 255, 255, 255, 255, 1.0f / 2.0f, 1.0f / 4.0f, 0.0f, 0.0f, -1.0f);

	//Face Left
	verts[4] = VertexFormat( -size.x / 2, size.y / 2, size.z / 2, 255, 255, 255, 255, 0.0f, 1.0f / 2.0f, -1.0f, 0.0f, 0.0f);
	verts[5] = VertexFormat( -size.x / 2, size.y / 2, -size.z / 2, 255, 255, 255, 255, 1.0f / 4.0f, 1.0f / 2.0f, -1.0f, 0.0f, 0.0f);
	verts[6] = VertexFormat(-size.x / 2, -size.y / 2, size.z / 2, 255, 255, 255, 255, 0.0f, 1.0f / 4.0f, -1.0f, 0.0f, 0.0f);
	verts[7] = VertexFormat(-size.x / 2, -size.y / 2, -size.z / 2, 255, 255, 255, 255, 1.0f / 4.0f, 1.0f / 4.0f, -1.0f, 0.0f, 0.0f);

	//Face Right
	verts[8] = VertexFormat(size.x / 2, size.y / 2, -size.z / 2, 255, 255, 255, 255, 1.0f / 2.0f, 1.0f / 2.0f, 1.0f, 0.0f, 0.0f);
	verts[9] = VertexFormat(size.x / 2, size.y / 2, size.z / 2, 255, 255, 255, 255, 3.0f / 4.0f, 1.0f / 2.0f, 1.0f, 0.0f, 0.0f);
	verts[10] = VertexFormat(size.x / 2, -size.y / 2, -size.z / 2, 255, 255, 255, 255, 1.0f / 2.0f, 1.0f / 4.0f, 1.0f, 0.0f, 0.0f);
	verts[11] = VertexFormat(size.x / 2, -size.y / 2, size.z / 2, 255, 255, 255, 255, 3.0f / 4.0f, 1.0f / 4.0f, 1.0f, 0.0f, 0.0f);

	//Face Back
	verts[12] = VertexFormat(size.x / 2, size.y / 2, size.z / 2, 255, 255, 255, 255, 3.0f / 4.0f, 1.0f / 2.0f, 0.0f, 0.0f, 1.0f);
	verts[13] = VertexFormat(-size.x / 2, size.y / 2, size.z / 2, 255, 255, 255, 255, 1.0f, 1.0f / 2.0f, 0.0f, 0.0f, 1.0f);
	verts[14] = VertexFormat(size.x / 2, -size.y / 2, size.z / 2, 255, 255, 255, 255, 3.0f / 4.0f, 1.0f / 4.0f, 0.0f, 0.0f, 1.0f);
	verts[15] = VertexFormat(-size.x / 2, -size.y / 2, size.z / 2, 255, 255, 255, 255, 1.0f, 1.0f / 4.0f, 0.0f, 0.0f, 1.0f);

	//Face Top
	verts[16] = VertexFormat(-size.x / 2, size.y / 2, size.z / 2, 255, 255, 255, 255, 1.0f / 4.0f, 3.0f / 4.0f, 0.0f, 1.0f, 0.0f);
	verts[17] = VertexFormat(size.x / 2, size.y / 2, size.z / 2, 255, 255, 255, 255, 1.0f / 2.0f, 3.0f / 4.0f, 0.0f, 1.0f, 0.0f);
	verts[18] = VertexFormat(-size.x / 2, size.y / 2, -size.z / 2, 255, 255, 255, 255, 1.0f / 4.0f, 1.0f / 2.0f, 0.0f, 1.0f, 0.0f);
	verts[19] = VertexFormat(size.x / 2, size.y / 2, -size.z / 2, 255, 255, 255, 255, 1.0f / 2.0f, 1.0f / 2.0f, 0.0f, 1.0f, 0.0f);

	//Face Bottom
	verts[20] = VertexFormat(-size.x / 2, -size.y / 2, -size.z / 2, 255, 255, 255, 255, 1.0f / 4.0f, 1.0f / 4.0f, 0.0f, -1.0f, 0.0f);
	verts[21] = VertexFormat(size.x / 2, -size.y / 2, -size.z / 2, 255, 255, 255, 255, 1.0f / 2.0f, 1.0f / 4.0f, 0.0f, -1.0f, 0.0f);
	verts[22] = VertexFormat(-size.x / 2, -size.y / 2, size.z / 2, 255, 255, 255, 255, 1.0f / 4.0f, 0.0f, 0.0f, -1.0f, 0.0f);
	verts[23] = VertexFormat(size.x / 2, -size.y / 2, size.z / 2, 255, 255, 255, 255, 1.0f / 2.0f, 0.0f, 0.0f, -1.0f, 0.0f);

	Mesh* pMesh = new Mesh();

	if (!invertFaces)
	{
		unsigned int indices[36] = { 0, 1, 2,  1, 3, 2,
									4, 5, 6,  5, 7, 6,
									8, 9, 10,  9, 11, 10,
									12, 13, 14,  13, 15, 14,
									16, 17, 18,  17, 19, 18,
									20, 21, 22,  21, 23, 22 };
		pMesh->Init(verts, 24, indices, 36, GL_STATIC_DRAW);
	}
	else
	{
		unsigned int indices[36] = {	0, 2, 1,  1, 2, 3,
										4, 6, 5,  5, 6, 7,
										8, 10, 9,  9, 10, 11,
										12, 14, 13,  13, 14, 15,
										16, 18, 17,  17, 18, 19,
										20, 22, 21,  21, 22, 23 };

		pMesh->Init(verts, 24, indices, 36, GL_STATIC_DRAW);
	}


	return pMesh;
}


Mesh* Mesh::CreatePlane(vec2 size, ivec2 vertCount, vec2 initUVOffset, vec2 UVMax)
{
	std::vector<VertexFormat> verts;
	std::vector<unsigned int> inds;

	ivec2 numBoxes = ivec2(vertCount.x - 1, vertCount.y - 1);

	vec2 increment = vec2(size.x / (vertCount.x - 1), size.y / (vertCount.y - 1));
	vec2 UVincrement = vec2(UVMax.x / (vertCount.x - 1) + initUVOffset.x, UVMax.y / (vertCount.y - 1) + initUVOffset.y);

	for (int x = 0; x < numBoxes.x; x++)
	{
		for (int y = 0; y < numBoxes.y; y++)
		{
			int numverts = verts.size();
			verts.push_back(VertexFormat(x * increment.x, 0, (y + 1) * increment.y, 0, rand() % 255 + 1, 255, 255, UVincrement.x * x, UVincrement.y * (y + 1), 0.0f, 1.0f, 0.0f));
			verts.push_back(VertexFormat((x + 1) * increment.x, 0, (y + 1) * increment.y, 0, rand() % 255 + 1, 255, 255, UVincrement.x * (x + 1), UVincrement.y * (y + 1), 0.0f, 1.0f, 0.0f));
			verts.push_back(VertexFormat(x * increment.x, 0, y * increment.y, 0, rand() % 255 + 1, 255, 255, UVincrement.x * x, UVincrement.y * y, 0.0f, 1.0f, 0.0f));
			verts.push_back(VertexFormat((x + 1) * increment.x, 0, y * increment.y, 0, rand() % 255 + 1, 255, 255, UVincrement.x * (x + 1), UVincrement.y * y, 0.0f, 1.0f, 0.0f));

			inds.push_back(numverts + 0);
			inds.push_back(numverts + 1);
			inds.push_back(numverts + 2);
			inds.push_back(numverts + 1);
			inds.push_back(numverts + 3);
			inds.push_back(numverts + 2);
		}
	}
			

	Mesh* pMesh = new Mesh();
	pMesh->Init(verts.data(), verts.size(), inds.data(), inds.size(), GL_STATIC_DRAW);
	pMesh->m_PrimitiveType = GL_TRIANGLES;
	glPointSize(10.0f);

	return pMesh;
}

Mesh* Mesh::LoadOBJ(const char* filename, bool optimize, bool saveOptimized)
{
	std::ifstream objStream(filename);
	std::string filenameString = filename;
	std::string name;
	std::string extension;
	unsigned int dotPosition = filenameString.find_last_of('.');
	if (dotPosition != std::string::npos) // npos is just size_t's -1
	{
		name = filenameString.substr(0, dotPosition);
		extension = filenameString.substr(dotPosition + 1);
	}
	else
	{
		name = filenameString;
		extension = "";
	}
	if (extension != "obj")
	{
		OutputMessage("Warning: file %s's extension is not .obj!\n", filename);
	}
	
	std::string linebuf;

	if (!objStream.is_open())
	{
		return false;
	}

	// create the buffers of information to load into the VBO
	std::vector<Vector3> points;
	std::vector<Vector2> uvs;
	std::vector<Vector3> normals;

	std::vector<ivec3> vertices;
	std::vector<ivec3> IBO;
	std::vector<VertexFormat> VBO;

	while (std::getline(objStream, linebuf))
	{

		std::istringstream lineStream(linebuf);
		std::string header;
		lineStream >> header;
		if (header == "v") // vertex
		{
			// format: f f f
			Vector3 vertex;
#if OBJ_INVERT_YZ
			lineStream >> vertex.x >> vertex.z >> vertex.y;
#else
			lineStream >> vertex.x >> vertex.y >> vertex.z;
#endif
			points.push_back(vertex);
		}
		else if (header == "vt") // texture coordinates
		{
			// format: f f
			Vector2 uv;
			lineStream >> uv.x >> uv.y;
			uvs.push_back(uv);
		}
		else if (header == "vn") // _per-face_ vertex normal
		{
			// format: f f f
			Vector3 normal;
#if OBJ_INVERT_YZ
			lineStream >> normal.x >> normal.z >> normal.y;
#else
			lineStream >> normal.x >> normal.y >> normal.z;
#endif
			normals.push_back(normal);
		}
		else if (header == "f") // face
		{
			// format: i/i/i i/i/i i/i/i
			enum TriangleVertex {
				VERT_A,
				VERT_B,
				VERT_C
			};

			ivec3 face;

			for (int i = 0; i < 3; i++)
			{
				unsigned int vertIndex, uvIndex, normIndex;
				// the dummy char eats both slashes in the stream, avoiding duplicate code
				char dummy;
				lineStream >> vertIndex >> dummy >> uvIndex >> dummy >> normIndex;

				// ignore the space to prepare for the next face vertex
				lineStream.ignore();

				// .obj is 1-indexed, so subtract 1 for vector/array indices
				vertIndex--;
				uvIndex--;
				normIndex--;

				vertices.push_back(ivec3(vertIndex, uvIndex, normIndex));

#if OBJ_INVERT_YZ
				// flipping the yz axes also inverts winding order so we need to correct the winding order by swapping some of the faces
				switch (i)
				{
				case VERT_A: face.x = vertices.size()-1; break;
				case VERT_B: face.z = vertices.size()-1; break;
				case VERT_C: face.y = vertices.size()-1; break;
				}
#else
				switch (i)
				{
				case VERT_A: face.x = vertices.size()-1; break;
				case VERT_B: face.y = vertices.size()-1; break;
				case VERT_C: face.z = vertices.size()-1; break;
				}
#endif
			}

			// add face to IBO
			IBO.push_back(face);
		}
	}

	objStream.close();

	// find and replace duplicate vertices and their corresponding indices
	/* algorithm:
		loop over the array to fill the list of "ungrouped" vertices
		until the ungrouped vertices list is empty:
			get the first ungrouped vertex
			add it to uniqueVertices
			add its index to indexMap (value is its position in uniqueVertices, size()-1)
			
			for each ungrouped vertex past the first:
				if that ungrouped vertex is identical to the first:
					add its index to indexMap (value is the first ungrouped vertex's value, uniqueVertices.size()-1)
					remove its ungrouped index from ungroupedIndices
				else:
					increment counter

			remove the first ungrouped index from ungroupedIndices

		// now the vertices are grouped; modify the IBO
		for each index in the IBO:
			replace the index with the unique index it's mapped to
	*/
	if (optimize)
	{
		std::vector<ivec3> uniqueVertices;
		int* indexMap = new int[vertices.size()];
		std::vector<int> ungroupedIndices;
		for (unsigned int i = 0; i < vertices.size(); i++)
		{
			ungroupedIndices.push_back(i);
		}
		while (ungroupedIndices.size() > 0)
		{
			int currentIndex = ungroupedIndices[0];
			ivec3 currentVertex = vertices[currentIndex];
			uniqueVertices.push_back(currentVertex);
			indexMap[currentIndex] = uniqueVertices.size() - 1;

			for (unsigned int subUngroupedIndex = 1; subUngroupedIndex < ungroupedIndices.size();)
			{
				int nextIndex = ungroupedIndices[subUngroupedIndex];
				ivec3 nextVertex = vertices[nextIndex];
				if (nextVertex == currentVertex)
				{
					indexMap[nextIndex] = uniqueVertices.size() - 1;
					ungroupedIndices.erase(ungroupedIndices.begin() + subUngroupedIndex);
				}
				else
				{
					subUngroupedIndex++;
				}
			}
			ungroupedIndices.erase(ungroupedIndices.begin());
		}

		OutputMessage("Consolidated %d vertices into %d vertices\n", vertices.size(), uniqueVertices.size());

		// now that we've consolidated all of the vertices, use the consolidated verts
		for (unsigned int triangle = 0; triangle < IBO.size(); triangle++)
		{
			IBO[triangle].x = indexMap[IBO[triangle].x];
			IBO[triangle].y = indexMap[IBO[triangle].y];
			IBO[triangle].z = indexMap[IBO[triangle].z];
		}

		// done parsing file and fixing IBO - build VBO using uniqueVertices
		for (unsigned int i = 0; i < uniqueVertices.size(); i++)
		{
			VertexFormat vertex;
			vertex.color = MyColor(255, 255, 255, 255);

			// add the vertex position to the VBO
			vertex.pos = points[uniqueVertices[i].x];

			// add the vertex UV coordinate to the VBO
			vertex.uv = uvs[uniqueVertices[i].y];

			// add the vertex normals, divide by total, and add to VBO
			vertex.normal = normals[uniqueVertices[i].z];
			VBO.push_back(vertex);
		}

		if (saveOptimized == true)
		{
			std::ofstream optimizedFile;
			optimizedFile.open(name + ".sdm", std::ios::binary);
			if (!optimizedFile.is_open())
			{
				OutputMessage("Error: failed to open %s.sdm", name.c_str());
			}
			else
			{
				unsigned int vertexCount = VBO.size();
				unsigned int indexCount = IBO.size() * 3;

				// write the dumb header
				optimizedFile.write((char*)&vertexCount, sizeof(unsigned int));
				optimizedFile.write((char*)&indexCount, sizeof(unsigned int));

				// write the VBO and IBO to the file
				optimizedFile.write((char*)VBO.data(), VBO.size() * sizeof(VertexFormat));
				optimizedFile.write((char*)IBO.data(), IBO.size() * sizeof(ivec3));

				optimizedFile.close();
			}
		}
	}
	else
	{
		// done parsing file and fixing IBO - build VBO using uniqueVertices
		for (unsigned int i = 0; i < vertices.size(); i++)
		{
			VertexFormat vertex;
			vertex.color = MyColor(255, 255, 255, 255);

			// add the vertex position to the VBO
			vertex.pos = points[vertices[i].x];

			// add the vertex UV coordinate to the VBO
			vertex.uv = uvs[vertices[i].y];

			// add the vertex normals, divide by total, and add to VBO
			vertex.normal = normals[vertices[i].z];
			VBO.push_back(vertex);
		}
	}

	Mesh* pMesh = new Mesh();
	pMesh->Init(VBO.data(), VBO.size(), (unsigned int*)(IBO.data()), IBO.size() * 3, GL_STATIC_DRAW);
	pMesh->m_PrimitiveType = GL_TRIANGLES;
	return pMesh;
}

Mesh* Mesh::LoadSDM(const char* filename)
{
	std::ifstream file;
	file.open(filename, std::ios::binary);

	if (!file.is_open())
	{
		OutputMessage("Error: failed to open %s", filename);
		assert(false);
	}
	unsigned int vertexCount;
	unsigned int indexCount;

	// read the dumb header
	file.read((char*)&vertexCount, 4);
	file.read((char*)&indexCount, 4);

	VertexFormat* VBO = new VertexFormat[vertexCount];
	unsigned int* IBO = new unsigned int[indexCount];

	// read the VBO and IBO
	for (unsigned int i = 0; i < vertexCount; i++)
	{
		// sizeof(VertexFormat) - sizeof(vec3) because the tangent, stored at the end, is to be assigned later
		file.read((char*)&VBO[i], sizeof(VertexFormat) - sizeof(vec3));
	}

	file.read((char*)IBO, indexCount * sizeof(unsigned int));

	file.close();

	Mesh* pMesh = new Mesh();
	pMesh->Init(VBO, vertexCount, IBO, indexCount, GL_STATIC_DRAW);

	delete[] VBO;
	delete[] IBO;

	pMesh->m_PrimitiveType = GL_TRIANGLES;
	return pMesh;
}