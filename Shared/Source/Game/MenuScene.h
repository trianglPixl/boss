#ifndef __MENUSCENE_H
#define __MENUSCENE_H
#include "Base\ObjectPooling.h"

class GameCore;

class MenuScene : public BaseMenuScene
{
protected:
	std::unique_ptr<MenuSlate> cursor;

	std::unique_ptr<PlayButton> playButton;
	std::unique_ptr<QuitButton> quitButton;

	std::unique_ptr<TextRender> scoreText;

	std::unique_ptr<TextRender> player1Name;
	std::unique_ptr<TextRender> player2Name;
	std::unique_ptr<TextRender> player3Name;
	std::unique_ptr<TextRender> player4Name;
	std::unique_ptr<TextRender> player5Name;

	std::unique_ptr<TextRender> player1Score;
	std::unique_ptr<TextRender> player2Score;
	std::unique_ptr<TextRender> player3Score;
	std::unique_ptr<TextRender> player4Score;
	std::unique_ptr<TextRender> player5Score;

	std::unique_ptr<MenuSlate> scorePane;

public:
	MenuScene(std::string name, GameCore* pGame, ResourceManager* pResources);
    virtual ~MenuScene();

    virtual void OnSurfaceChanged(unsigned int width, unsigned int height) override;
    virtual void LoadContent()				override;

	virtual void Restart()					override;

    virtual void Update(float deltatime)	override;
    virtual void Draw()						override;
};

#endif //__Scene_H__
