#ifndef __GameCore_H__
#define __GameCore_H__

#define __CLEAR_R 1.0f
#define __CLEAR_G 1.0f
#define __CLEAR_B 1.0f

class GameCore;

class ResourceManager;

class GameCore
{
protected:
	const float m_deltaClamp = 1.0f / 10.0f;

    ResourceManager* m_pResources;

    unsigned int m_WindowWidth;
    unsigned int m_WindowHeight;

	std::string m_mainGameScene = "";

    std::queue<Event*> m_EventQueue;

    PlayerController m_Controllers[4];
public:

	//Hacked I know
	HighScore GameScores;

    GameCore();
    virtual ~GameCore();

    virtual void OnSurfaceChanged(unsigned int width, unsigned int height);
    virtual void LoadContent();

    virtual void QueueEvent(Event* pEvent);
    virtual void ProcessEvents();
    virtual void OnEvent(Event* pEvent);

    virtual void Update(float deltatime);
    virtual void Draw();

    PlayerController* GetController(int index) { return &m_Controllers[index]; }

	unsigned int GetWindowWidth() { return m_WindowWidth; };
	unsigned int GetWindowHeight() { return m_WindowHeight; }

	void LoadScenes();
};

#endif //__GameCore_H__
