#include "pch.h"

GameOverScene::GameOverScene(std::string name, GameCore* pGame, ResourceManager* pResources)
: BaseMenuScene(name, pGame, pResources )
{
}

GameOverScene::~GameOverScene()
{
}

void GameOverScene::OnSurfaceChanged(unsigned int width, unsigned int height)
{
	BaseMenuScene::OnSurfaceChanged( width, height );
}

void GameOverScene::LoadContent()
{
	BaseMenuScene::LoadContent();
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LESS);

    // create our shaders.
    {
		if (m_pResources->GetShader("Texture") == nullptr)
			m_pResources->AddShader( "Texture", new ShaderProgram( "Data/Shaders/Texture.vert", "Data/Shaders/Texture.frag" ) );

    }

    // create some game objects.
    {

        // camera
		{
			m_pGameObjects["Camera"] = new CameraObject(this, "Camera", vec3(0, 0, -40), vec3(0, 0, 0), 5.0f, 0, 0, 0);
			((CameraObject *)m_pGameObjects["Camera"])->SetController(m_pGame->GetController(1));

		}

		m_numButtons = 3;
		m_currentButton = 1;

		//Cursor
		cursor.reset(new MenuSlate(vec3(-10, -7.2f, 10.0f), 0, 1, (CameraObject *)m_pGameObjects["Camera"], 4, 2,
			"Data/Textures/CursorArrow.png", "Data/Shaders/Texture.vert", "Data/Shaders/Texture.frag"));

		//Quit Button
		quitButton.reset(new QuitButton(vec3(-2.7f, -7.2f, 10.0f), 0.0f, 2.0f, ((CameraObject *)m_pGameObjects["Camera"]), "LightingScene",
			"Data/Textures/Button.png", "Data/Shaders/Texture.vert", "Data/Shaders/Texture.frag", PlayerControllerButtons::PCB_NULL));
		quitButton->SetController(m_pGame->GetController(1));

		quitButton->UseText(vec3(-1.2f, -7.6f, 10.0f), 72, 1024, "QUIT", vec3(1, 1, 1));

		//Return Button
		returnToMenu.reset(new ReturnMenuButton(vec3(-2.7f, -1.5f, 10.0f), 0.0f, 2.0f, ((CameraObject *)m_pGameObjects["Camera"]), "LightingScene",
			"Data/Textures/Button.png", "Data/Shaders/Texture.vert", "Data/Shaders/Texture.frag", PlayerControllerButtons::PCB_NULL));
		returnToMenu->SetController(m_pGame->GetController(1));

		returnToMenu->UseText(vec3(-2.2f, -1.8f, 10.0f), 72, 1024, "TO MENU", vec3(1, 1, 1));

		//Restart Button
		restartButton.reset(new RestartButton(vec3(-2.7f, -4.3f, 10.0f), 0.0f, 2.0f, ((CameraObject *)m_pGameObjects["Camera"]), "LightingScene",
			"Data/Textures/Button.png", "Data/Shaders/Texture.vert", "Data/Shaders/Texture.frag", PlayerControllerButtons::PCB_NULL));
		restartButton->SetController(m_pGame->GetController(1));

		restartButton->UseText(vec3(-1.8f, -4.6f, 10.0f), 72, 1024, "Restart", vec3(1, 1, 1));

		//Create score pane
		scorePane.reset(new MenuSlate(vec3(-6, -8, 4.0f), 0, 1, (CameraObject *)m_pGameObjects["Camera"], 
			12 , 16 ,"Data/Textures/MenuPane.png", "Data/Shaders/Texture.vert", "Data/Shaders/Texture.frag"));

		scoreText.reset(new TextRender("Data/TypeFaces/Amble-Regular.ttf", 24, 1024, 1024, 3, 1, TextRender::AP_BOTTOMLEFT, ((CameraObject *)m_pGameObjects["Camera"]),
			"Data/Shaders/Text.vert",
			"Data/Shaders/Text.frag"));
		scoreText->SetAttachedToCamera(false);
		scoreText->SetTextColor(vec3(1, 1, 1));
		scoreText->ChangeString("EWE SUCK");
		scoreText->SetTranslation(vec3(-0.6f, 0.5f,3), vec3(0));
		
		
    }
}

void GameOverScene::Update(float deltatime)
{
	BaseMenuScene::Update(deltatime);


	quitButton->Update(deltatime);
	scorePane->Update(deltatime);
	cursor->Update(deltatime);


	switch (m_currentButton)
	{
	case 1:
	{
		if (m_pGame->GetController(1)->IsButtonJustPressed(PCB_ENTER))
			quitButton->Press();

		vec3 newPos = quitButton->GetPosition();

		newPos.x -= (quitButton->GetButtonWidth() * quitButton->GetScale()) / 1.5f;

		cursor->SetPosition(newPos);
	}
	break;

	case 2:
	{
		if (m_pGame->GetController(1)->IsButtonJustPressed(PCB_ENTER))
			restartButton->Press();

		vec3 newPos = restartButton->GetPosition();

		newPos.x -= (restartButton->GetButtonWidth() * restartButton->GetScale()) / 1.5f;

		cursor->SetPosition(newPos);
	}
	break;
	case 3:
	{
		if (m_pGame->GetController(1)->IsButtonJustPressed(PCB_ENTER))
			returnToMenu->Press();

		vec3 newPos = returnToMenu->GetPosition();

		newPos.x -= (returnToMenu->GetButtonWidth() * returnToMenu->GetScale()) / 1.5f;

		cursor->SetPosition(newPos);
	}
	break;
	}
}

void GameOverScene::Draw()
{
	BaseMenuScene::Draw();
	scorePane->Draw();
	scoreText->Draw();
	quitButton->Draw();
	returnToMenu->Draw();
	restartButton->Draw();
	cursor->Draw();
}