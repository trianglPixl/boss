#include "pch.h"
#include "GameCore.h"
#include "MenuScene.h"

#define __CULLFACE

GameCore::GameCore()
{
    m_pResources = nullptr;
}

GameCore::~GameCore()
{
    while( m_EventQueue.size() != 0 )
    {
        Event* pEvent = m_EventQueue.front();
        m_EventQueue.pop();
        delete pEvent;
    }

    delete m_pResources;
	SceneManager::GetInstance()->ClearStack();
	SceneManager::GetInstance()->PurgeAllScenesFromPool();
}

void GameCore::OnSurfaceChanged(unsigned int width, unsigned int height)
{
    glViewport( 0, 0, width, height );

    m_WindowWidth = width;
    m_WindowHeight = height;

	//Aspect ratio for scene
	if (SceneManager::GetInstance()->IsStackValid())
	{
		SceneManager::GetInstance()->UpdateSceneCamerasAR(static_cast<float>(width) / static_cast<float>(height));
		SceneManager::GetInstance()->OnSurfaceChanged(width, height);
	}
}

void GameCore::LoadContent()
{
	//CW occlusion culling
#ifdef __CULLFACE
	
		glEnable(GL_CULL_FACE);
		glCullFace(GL_BACK);
		glFrontFace(GL_CW);
#endif	

    // turn on alpha blending.
    glEnable( GL_BLEND );
    glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    m_pResources = new ResourceManager();

	LoadScenes();

    CheckForGLErrors();

    srand((unsigned int)time(nullptr));
}

void GameCore::QueueEvent(Event* pEvent)
{
    m_EventQueue.push( pEvent );
}

void GameCore::ProcessEvents()
{
    while( m_EventQueue.size() != 0 )
    {
        Event* pEvent = m_EventQueue.front();
        m_EventQueue.pop();

        OnEvent( pEvent );
        delete pEvent;
    }
}

void GameCore::OnEvent(Event* pEvent)
{
	SceneManager::GetInstance()->OnEvent( pEvent );

    if( pEvent->GetEventType() == EventType_Input )
    {
        InputEvent* pInput = (InputEvent*)pEvent;

#if _WIN32
        if( pInput->GetInputDeviceType() == InputDeviceType_Keyboard )
        {
            int id = pInput->GetID();

            if( pInput->GetInputState() == InputState_Pressed )
            {
				if (id == 'W')
					m_Controllers[0].SetButtonPressed( PCB_Up );
				if (id == 'S')
					m_Controllers[0].SetButtonPressed(PCB_Down);
				if (id == 'A')
					m_Controllers[0].SetButtonPressed(PCB_Left);
				if (id == 'D')
					m_Controllers[0].SetButtonPressed(PCB_Right);
				if (id == VK_SPACE)
				{
					m_Controllers[0].SetButtonPressed(PCB_SPACE);
					//m_Controllers[1].SetButtonPressed(PCB_SPACE);
				}

				//Menu
				if (id == VK_RETURN)
					m_Controllers[1].SetButtonPressed(PCB_ENTER);
				if (id == VK_LEFT || id == VK_UP)
					m_Controllers[1].SetButtonPressed(PCB_Up);
				if (id == VK_RIGHT || id == VK_DOWN)
					m_Controllers[1].SetButtonPressed(PCB_Down);
				if (id == VK_ESCAPE)
					m_Controllers[1].SetButtonPressed(PCB_ESCAPE);
            }

            if( pInput->GetInputState() == InputState_Released )
            {
				if (id == 'W')
					m_Controllers[0].SetButtonReleased(PCB_Up);
				if (id == 'S')
					m_Controllers[0].SetButtonReleased(PCB_Down);
				if (id == 'A')
					m_Controllers[0].SetButtonReleased(PCB_Left);
				if (id == 'D')
					m_Controllers[0].SetButtonReleased(PCB_Right);
				if (id == VK_SPACE)
				{
					m_Controllers[0].SetButtonReleased(PCB_SPACE);
					//m_Controllers[1].SetButtonReleased(PCB_SPACE);
				}

				//Menu
				if (id == VK_RETURN)
					m_Controllers[1].SetButtonReleased(PCB_ENTER);
				if (id == VK_LEFT || id == VK_UP)
					m_Controllers[1].SetButtonReleased(PCB_Up);
				if (id == VK_RIGHT || id == VK_DOWN)
					m_Controllers[1].SetButtonReleased(PCB_Down);
				if (id == VK_ESCAPE)
					m_Controllers[1].SetButtonReleased(PCB_ESCAPE);
            }
        }

		if (pInput->GetInputDeviceType() == InputDeviceType_Mouse)
		{
			int id = pInput->GetID();
			vec2 pos = pInput->GetPosition();

			m_Controllers[0].SetMousePos(pos);

			switch (id)
			{
			case MK_LBUTTON:
				if(pInput->GetInputState() == InputState_Pressed)
					m_Controllers[0].SetButtonPressed(PCB_LMOUSE);
				if (pInput->GetInputState() == InputState_Released)
					m_Controllers[0].SetButtonReleased(PCB_LMOUSE);
				break;
			case MK_RBUTTON:
				if (pInput->GetInputState() == InputState_Pressed)
					m_Controllers[0].SetButtonPressed(PCB_RMOUSE);
				else if (pInput->GetInputState() == InputState_Released)
					m_Controllers[0].SetButtonReleased(PCB_RMOUSE);
				break;
			}
			
		}
#endif // _WIN32

        if( pInput->GetInputDeviceType() == InputDeviceType_Gamepad )
        {
            if( pInput->GetInputState() == InputState_Held )
            {
                if( pInput->GetGamepadID() == GamepadID_LeftStick )
                {
                    if( pInput->GetPosition().y > 0 )
                        m_Controllers[0].SetButtonPressed( PCB_Up );
                    if( pInput->GetPosition().y < 0 )
                        m_Controllers[0].SetButtonPressed( PCB_Down );
                    if( pInput->GetPosition().x < 0 )
                        m_Controllers[0].SetButtonPressed( PCB_Left );
                    if( pInput->GetPosition().x > 0 )
                        m_Controllers[0].SetButtonPressed( PCB_Right );

                    if( pInput->GetPosition().y == 0 )
                    {
                        m_Controllers[0].SetButtonReleased( PCB_Up );
                        m_Controllers[0].SetButtonReleased( PCB_Down );
                    }
                    if( pInput->GetPosition().x == 0 )
                    {
                        m_Controllers[0].SetButtonReleased( PCB_Left );
                        m_Controllers[0].SetButtonReleased( PCB_Right );
                    }
                }
            }
        }
    }
}

void GameCore::Update(float deltatime)
{
    CheckForGLErrors();

	if (deltatime > m_deltaClamp)
		deltatime = m_deltaClamp;

    ProcessEvents();
	SceneManager::GetInstance()->Update( deltatime );

	// update the held buttons
	for (int i = 0; i < 4; i++)
	{
		m_Controllers[i].UpdateHeldButtons();
	}

	if (SceneManager::GetInstance()->IsDone())
	{
		//ResetSceneHere

		SceneManager::GetInstance()->PopStack();
	}

    CheckForGLErrors();
}

void GameCore::Draw()
{
    CheckForGLErrors();

    glClearColor( __CLEAR_R, __CLEAR_G, __CLEAR_B, 0.0f );
    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

	SceneManager::GetInstance()->Draw();

    CheckForGLErrors();
}

void GameCore::LoadScenes()
{

	SceneManager::GetInstance()->MakeNewSceneInPool("LightingScene", new LightingScene("LightingScene", this, m_pResources));
	SceneManager::GetInstance()->GetScene("LightingScene")->LoadContent();
	m_mainGameScene = "LightingScene";

	//SceneManager::GetInstance()->PushSceneToStack("LightingScene");

	SceneManager::GetInstance()->MakeNewSceneInPool("PauseScene", new PauseScene("PauseScene", this, m_pResources));
	SceneManager::GetInstance()->GetScene("PauseScene")->LoadContent();

	SceneManager::GetInstance()->MakeNewSceneInPool("GameOverScene", new GameOverScene("GameOverScene", this, m_pResources));
	SceneManager::GetInstance()->GetScene("GameOverScene")->LoadContent();

	SceneManager::GetInstance()->MakeNewSceneInPool("MenuScene", new MenuScene("MenuScene", this, m_pResources));
	SceneManager::GetInstance()->GetScene("MenuScene")->LoadContent();

	SceneManager::GetInstance()->PushSceneToStack("MenuScene");
}
