#pragma once
#include "Base\ObjectPooling.h"

class GameCore;

class GameOverScene : public BaseMenuScene
{
protected:
	std::unique_ptr<MenuSlate> cursor;

	std::unique_ptr<QuitButton> quitButton;
	std::unique_ptr<ReturnMenuButton> returnToMenu;
	std::unique_ptr<RestartButton> restartButton;

	std::unique_ptr<TextRender> titleText;
	std::unique_ptr<TextRender> scoreText;

	std::unique_ptr<MenuSlate> scorePane;

public:
	GameOverScene(std::string name, GameCore* pGame, ResourceManager* pResources);
    virtual ~GameOverScene();

    virtual void OnSurfaceChanged(unsigned int width, unsigned int height) override;
    virtual void LoadContent()				override;

    virtual void Update(float deltatime)	override;
    virtual void Draw()						override;
};
