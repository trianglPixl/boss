#ifndef __LIGHTINGSCENE_H
#define __LIGHTINGSCENE_H

class GameCore;

class LightingScene : public Scene
{
protected:
	std::unique_ptr<TextRender> text;

	FBODefinition fbo;

public:
	LightingScene(std::string name, GameCore* pGame, ResourceManager* pResources);
    virtual ~LightingScene();

    virtual void OnSurfaceChanged(unsigned int width, unsigned int height) override;
    virtual void LoadContent()				override;

    virtual void Update(float deltatime)	override;
    virtual void Draw()						override;
	virtual void Restart()					override;

	virtual void OnEvent(Event* pEvent) override;

	void SetupCustomRenderPointUniforms(Mesh * pMesh, GameObject * object, vec3 pos, mat4 viewMat, mat4 projMat, Light * light) override;
};

#endif //__Scene_H__
