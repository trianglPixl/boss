#ifndef __Scene_H__
#define __Scene_H__

class GameCore;
class GameObject;

class Scene
{
protected:

	std::string m_name;

    GameCore* m_pGame;
    ResourceManager* m_pResources;

    std::map<std::string, GameObject*> m_pGameObjects;

	std::unique_ptr<Light[]> m_lights;

	SoundManager* m_pSoundManager;

	ShaderProgram * m_pRenderPointShader;

	bool m_isSceneActive;
	bool m_isSceneFrozen;
	bool m_isSceneOpaque;

	Scene * m_parentScene;

	float m_RestartTime;

	std::unique_ptr<FBODefinition> m_pRenderTarget;
	std::unique_ptr<FBODefinition> m_pSecondRenderTarget;

	std::vector<GLuint> m_PostprocessStack;

	void DrawFramebuffer(GLuint shader, GLuint source);

public:
	unsigned int ACTIVELIGHTS = 0;

	Light * GetLightArr() { return m_lights.get(); };

	bool m_isDone = false;

    Scene(std::string name, GameCore* pGame, ResourceManager* pResources);
    virtual ~Scene();

    virtual void OnSurfaceChanged(unsigned int width, unsigned int height);
    virtual void LoadContent();

    virtual void OnEvent(Event* pEvent);
    virtual void Update(float deltatime);
    virtual void Draw();
	virtual void Restart();

	float GetSceneStartTime();

	virtual void AddObject(GameObject* object, std::string name = "");

	virtual void UpdateSceneCamerasAR(float ar);

	//Point is in world space
	void RenderSceneToCubeFromPoint(FBODefinition & fbo, vec3 pos, int resolution, float renderDistance, std::string objectName = "");

	//m_pRenderPointShader must be set otherwise nothing will happen
	void RenderShadowmaps( int resolution );

	void SetRenderPointShader(ShaderProgram * given) { m_pRenderPointShader = given; };

	virtual void SetupCustomRenderPointUniforms(Mesh * pMesh, GameObject * object, vec3 pos, mat4 viewMat, mat4 projMat, Light * light) { };

    GameObject* GetGameObject(const char* name) { return m_pGameObjects[name]; }
	std::string GetName() { return m_name; };

	GameCore* GetGameCore() { return m_pGame; }

	ResourceManager* GetResourceManager() { return m_pResources; }
	SoundManager* GetSoundManager() { return m_pSoundManager; }

	std::vector<GameObject*> GenerateSequentialObjects(GameObject masterObject, const char* namePrefix, int count);

	//The alignment is too good.
	void SetActive(bool active) { m_isSceneActive = active; };
	void SetFrozen(bool frozen) { m_isSceneFrozen = frozen; };
	void SetOpaque(bool opaque) { m_isSceneOpaque = opaque; };

	bool IsSceneActive() { return m_isSceneActive; };
	bool IsSceneFrozen() { return m_isSceneFrozen; };

	void SetParentScene(Scene * parent) { m_parentScene = parent; };
};

#endif //__Scene_H__
