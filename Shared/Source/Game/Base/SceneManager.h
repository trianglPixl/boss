#pragma once
#ifndef __SCENEMANAGER_H
#define __SCENEMANAGER_H

#include <mutex>
#include <stack>

/*
	WORKFLOW:

	Add scenes to the pool at startup
	Push desired scenes onto the stack.
	When calling Scene functions on manager 
	the functions will be called on the top 
	Scene in the stack.
*/

class SceneManager
{
	static std::mutex						m_mutex;
	static std::unique_ptr<SceneManager>	m_manager;

	SceneManager() {};

	std::map<std::string, std::unique_ptr<Scene>>	m_scenes;
	std::stack<Scene *, std::vector<Scene *>>		m_stack;

public:
	static SceneManager * GetInstance();

	~SceneManager()
	{
		m_scenes.clear();
		ClearStack();
	}

	//Manager functions
	Scene * GetScene(std::string name);

	void RemoveSceneFromPool(std::string name);

	void MakeNewSceneInPool(std::string name, Scene * scene);

	void PurgeAllScenesFromPool();

	void PushSceneToStack(std::string name, bool freezePrevious = false);
	void PushSceneToStackAndSetNewParent(std::string name, bool freezePrevious = false);

	void PopStack();

	void ClearStack();

	void RebuildScene(std::string name, Scene * rS);

	void RestartScene(std::string sceneName);

	std::string GetCurrentSceneName();

	bool IsStackValid();

	//Wrappers for current scene
	void OnSurfaceChanged(unsigned int width, unsigned int height);
	void LoadContent();

	void OnEvent(Event* pEvent);
	void Update(float deltatime);
	void Draw();
	bool IsDone();

	void UpdateSceneCamerasAR(float ar);

	GameObject* GetGameObject(const char* name);
};

#endif // !__SCENEMANAGER_H
