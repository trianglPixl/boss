#include "pch.h"
#include "Scene.h"

void Scene::DrawFramebuffer(GLuint shader, GLuint source)
{
	glUseProgram(shader);
	GLuint positionLoc = glGetAttribLocation(shader, "a_Position");
	
	glVertexAttribPointer(positionLoc, 2, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(positionLoc);	

	GLuint textureLoc = glGetUniformLocation(shader, "u_Texture");

	if (textureLoc != -1)
	{
		glBindTexture(GL_TEXTURE_2D, source);
		glUniform1i(textureLoc, 0);
	}

	glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);

	glDisableVertexAttribArray(positionLoc);
	CheckForGLErrors();
}

Scene::Scene(std::string name, GameCore* pGame, ResourceManager* pResources)
{
	m_name = name;
    m_pGame = pGame;
    m_pResources = pResources;
	m_pRenderPointShader = nullptr;
	m_lights.reset(new Light[MAXLIGHTS]);
	m_pSoundManager = new SoundManager;

	m_pRenderTarget.reset(new FBODefinition());
	m_pSecondRenderTarget.reset(new FBODefinition());

	m_pRenderTarget->Setup(pGame->GetWindowWidth(), pGame->GetWindowHeight(), GL_NEAREST, GL_NEAREST, true, 16, true, false);
	m_pRenderTarget->Create();

	m_pSecondRenderTarget->Setup(pGame->GetWindowWidth(), pGame->GetWindowHeight(), GL_NEAREST, GL_NEAREST, true, 16, true, false);
	m_pSecondRenderTarget->Create();
}

Scene::~Scene()
{
    for( auto object: m_pGameObjects )
    {
        delete object.second;
    }
	delete m_pSoundManager;
}

void Scene::OnSurfaceChanged(unsigned int width, unsigned int height)
{
	m_pRenderTarget->Resize(width, height);
	m_pSecondRenderTarget->Resize(width, height);

	if (m_parentScene != nullptr && m_parentScene->IsSceneFrozen() == false)
	{
		m_parentScene->OnSurfaceChanged(width, height);
	}
}

void Scene::LoadContent()
{
}

void Scene::OnEvent(Event* pEvent)
{
}

void Scene::Update(float deltatime)
{
    CheckForGLErrors();

	if (m_parentScene != nullptr && m_parentScene->IsSceneFrozen() == false)
	{
		m_parentScene->Update(deltatime);
	}

    // update all of the Scene objects in the list.
    for( auto object: m_pGameObjects )
    {
        object.second->Update( deltatime );
    }

    CheckForGLErrors();
}

void Scene::Draw()
{
    CheckForGLErrors();

	if (m_PostprocessStack.size() > 0)
	{
		// bind postprocess FBO
		m_pRenderTarget->Bind();
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	}

    glDepthMask(GL_TRUE);
    // render all of the Scene objects in the list.
    for( int i=0; i<4; i++ )
    {
        //for( std::map<std::string, GameObject*>::iterator iterator = m_pGameObjects.begin(); iterator != m_pGameObjects.end(); iterator++ )
        for( auto object: m_pGameObjects )
        {
            object.second->Draw( i );
        }
    }
    // draw transparent layer
    glDepthMask(GL_FALSE);
    for (auto object : m_pGameObjects)
    {
        object.second->Draw(4);
    }
    glDepthMask(GL_TRUE);

	// draw postprocess
	if (m_PostprocessStack.size() > 0)
	{
		GLenum depthFunc;
		glGetIntegerv(GL_DEPTH_FUNC, (GLint*)&depthFunc);

		glDepthFunc(GL_ALWAYS);

		const float vertices[] = { -1.0f, -1.0f,
								  -1.0f, 1.0f,
								  1.0f, -1.0f,
								  1.0f, 1.0f };

		const GLuint indices[] = { 0, 1, 2,
								  2, 1, 3 };

		GLuint quadVBO;
		GLuint quadIBO;

		glGenBuffers(1, &quadVBO);
		glBindBuffer(GL_ARRAY_BUFFER, quadVBO);
		glBufferData(GL_ARRAY_BUFFER, sizeof(float) * 8, vertices, GL_STATIC_DRAW);
		
		glGenBuffers(1, &quadIBO);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, quadIBO);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(GLuint) * 6, indices, GL_STATIC_DRAW);


		bool isUsingSecondTexture = true;

		glActiveTexture(GL_TEXTURE0);

		for (int i = 0; i < m_PostprocessStack.size() - 1; i++)
		{
			GLuint sourceTexture;

			if (isUsingSecondTexture)
			{
				m_pSecondRenderTarget->Bind();
				glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
				sourceTexture = m_pRenderTarget->GetColorTextureHandle();
			}
			else
			{
				m_pRenderTarget->Bind();
				glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
				sourceTexture = m_pSecondRenderTarget->GetColorTextureHandle();
			}

			isUsingSecondTexture = !isUsingSecondTexture;

			DrawFramebuffer(m_PostprocessStack[i], sourceTexture);
		}

		glBindFramebuffer(GL_FRAMEBUFFER, 0);

		if (isUsingSecondTexture)
		{
			// we're ready to draw to the second framebuffer so we last drew to the first framebuffer - draw that to the main framebuffer
			DrawFramebuffer(m_PostprocessStack.back(), m_pRenderTarget->GetColorTextureHandle());
		}
		else
		{
			// we last drew to the second texture
			DrawFramebuffer(m_PostprocessStack.back(), m_pSecondRenderTarget->GetColorTextureHandle());
		}

		glBindBuffer(GL_ARRAY_BUFFER, 0);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
		glDeleteBuffers(1, &quadVBO);
		glDeleteBuffers(1, &quadIBO);

		glDepthFunc(GL_LESS);
	}

	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	if (m_parentScene != nullptr && m_parentScene->IsSceneFrozen() == false)
	{
		m_parentScene->Draw();
	}	

	CheckForGLErrors();
}

void Scene::Restart()
{
	m_RestartTime = (float)MyGetSystemTime();
}

float Scene::GetSceneStartTime()
{
	return m_RestartTime;
}

void Scene::AddObject(GameObject* object, std::string name)
{
	if (name.empty() == true)
	{
		name == object->GetName();
	}

	m_pGameObjects[name] = object;
}

//Checking of object in scene is camera, if so then updating their aspect ratio
void Scene::UpdateSceneCamerasAR(float ar)
{
	if (m_parentScene != nullptr && m_parentScene->IsSceneFrozen() == false)
	{
		m_parentScene->UpdateSceneCamerasAR(ar);
	}

	CameraObject * ptr = nullptr;
	for (auto it : m_pGameObjects)
	{
		//Attempt cast
		ptr = dynamic_cast<CameraObject *>(it.second);
		//If cast doens't fail then object is a Camera.

		if (ptr != NULL)
		{
			ptr->AspectRatioChanged(ar);
		}
	}
}

void Scene::RenderSceneToCubeFromPoint(FBODefinition & fbo, vec3 pos, int resolution, float renderDistance, std::string objectName)
{
	glViewport(0, 0, resolution, resolution);

	mat4 viewMats[6];
	viewMats[0].CreateLookAtViewLeftHanded(pos, vec3(0.0f, 1.0f, 0.0f), pos + vec3( 1.0f, 0.0f, 0.0f));
	viewMats[1].CreateLookAtViewLeftHanded(pos, vec3(0.0f, 1.0f, 0.0f), pos + vec3(-1.0f, 0.0f, 0.0f));
	viewMats[2].CreateLookAtViewLeftHanded(pos, vec3(0.0f, 0.0f, -1.0f), pos + vec3(0.0f, 1.0f, 0.0f));
	viewMats[3].CreateLookAtViewLeftHanded(pos, vec3(0.0f, 0.0f, 1.0f),  pos + vec3(0.0f, -1.0f, 0.0f));
	viewMats[4].CreateLookAtViewLeftHanded(pos, vec3(0.0f, 1.0f, 0.0f), pos + vec3(0.0f, 0.0f, 1.0f));
	viewMats[5].CreateLookAtViewLeftHanded(pos, vec3(0.0f, 1.0f, 0.0f), pos + vec3(0.0f, 0.0f, -1.0f));

	mat4 projMat;
	projMat.SetIdentity();
	projMat.CreatePerspectiveVFoV(90.0f, 1, 0.01f, renderDistance);

	fbo.Invalidate(true);
	fbo.Setup(resolution, resolution, GL_LINEAR, GL_LINEAR, true, 24, false, true);
	fbo.Create();

	fbo.Bind();

	glCullFace(GL_FRONT);
	projMat.m22 *= -1.0f;

	//Iterate over faces
	for (int i = 0; i < 6; i++)
	{
		fbo.SetCubeTexture(i);


		//Iterate over GameObjects
		for (auto & it : m_pGameObjects)
		{
			GameObject * object = it.second;
				Mesh * objMesh = object->GetMesh();

			//Make sure there is a mesh and that you are not drawing yourself.
			if (objMesh != nullptr && object->GetName() != objectName)
			{
				CheckForGLErrors();

				object->Draw(objMesh->GetRenderOrder(), pos, projMat, viewMats[i]);
			}

		}
	}

	glCullFace(GL_BACK);
	projMat.m22 *= -1.0f;
	glViewport(0, 0, m_pGame->GetWindowWidth(), m_pGame->GetWindowHeight());
	fbo.Unbind();
}

void Scene::RenderShadowmaps(int resolution)
{
	glViewport(0, 0, resolution, resolution);
	mat4 viewMats[6];
	glUseProgram(m_pRenderPointShader->GetProgram());

	for (unsigned int i = 0; i < ACTIVELIGHTS; i++)
	{
		if (m_lights[i].fbo.IsFullyLoaded() == false)
		{
			m_lights[i].fbo.Setup(resolution, resolution, GL_LINEAR, GL_LINEAR, true, 24, true, true);
			m_lights[i].fbo.Create();
			m_lights[i].fbo.Bind();
			m_lights[i].fbo.ClearCube(vec4(1.0f, 1.0f, 1.0f, 1.0f));
			m_lights[i].fbo.Unbind();
			CheckForGLErrors();
		}

		if (m_lights[i].castsShadow == false)
		{
			continue;
		}

		vec3 pos = m_lights[i].position.XYZ();

		viewMats[0].CreateLookAtViewLeftHanded(pos, vec3(0.0f, 1.0f, 0.0f), pos + vec3(1.0f, 0.0f, 0.0f));
		viewMats[1].CreateLookAtViewLeftHanded(pos, vec3(0.0f, 1.0f, 0.0f), pos + vec3(-1.0f, 0.0f, 0.0f));
		viewMats[2].CreateLookAtViewLeftHanded(pos, vec3(0.0f, 0.0f, -1.0f), pos + vec3(0.0f, 1.0f, 0.0f));
		viewMats[3].CreateLookAtViewLeftHanded(pos, vec3(0.0f, 0.0f, 1.0f), pos + vec3(0.0f, -1.0f, 0.0f));
		viewMats[4].CreateLookAtViewLeftHanded(pos, vec3(0.0f, 1.0f, 0.0f), pos + vec3(0.0f, 0.0f, 1.0f));
		viewMats[5].CreateLookAtViewLeftHanded(pos, vec3(0.0f, 1.0f, 0.0f), pos + vec3(0.0f, 0.0f, -1.0f));

		mat4 projMat;
		projMat.SetIdentity();
		projMat.CreatePerspectiveVFoV(90.0f, 1, 0.01f, m_lights[i].attenuationCutoff * 15.0f);

		//m_lights[i].fbo.Invalidate(true);
		//m_lights[i].fbo.Setup(resolution, resolution, GL_LINEAR, GL_LINEAR, true, 24, true, true);
		//m_lights[i].fbo.Create();

		m_lights[i].fbo.Bind();

		glCullFace(GL_FRONT);
		projMat.m22 *= -1.0f;

		//Iterate over faces
		for (int t = 0; t < 6; t++)
		{
			m_lights[i].fbo.SetCubeTexture(t, Vector4(1.0f, 1.0f, 1.0f, 1.0f));

			//Iterate over GameObjects
			for (auto & it : m_pGameObjects)
			{
				GameObject * object = it.second;
				Mesh * objMesh = object->GetMesh();

				//Make sure there is a mesh and that you are not drawing yourself.
				if (objMesh != nullptr)
				{
					
					//Empty unless overriden
					objMesh->SetupAttributes(m_pRenderPointShader);
					objMesh->SetupUniforms(m_pRenderPointShader, object->GetScale(), object->GetRotation(), object->GetPosition(), viewMats[t], projMat, 0, Vector4(1, 1, 1, 1));

					SetupCustomRenderPointUniforms( objMesh, object, pos, viewMats[t], projMat, &m_lights[i] );

					CheckForGLErrors();

					objMesh->Draw(m_pRenderPointShader);
				}

			}
		}
		glCullFace(GL_BACK);
		projMat.m22 *= -1.0f;
	}
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glViewport(0, 0, m_pGame->GetWindowWidth(), m_pGame->GetWindowHeight());
}

std::vector<GameObject*> Scene::GenerateSequentialObjects(GameObject masterObject, const char * namePrefix, int count)
{
	std::vector<GameObject*> sequence;
	sequence.reserve(count);

	std::string baseName = masterObject.GetName();
	for (int i = 0; i < count; i++)
	{
		std::string newName = baseName + std::to_string(i);
		GameObject* newObject = new GameObject(masterObject);
		m_pGameObjects[newName.c_str()] = newObject;
		sequence.push_back(newObject);
	}
	return sequence;
}