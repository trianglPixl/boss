#pragma once
#ifndef __OBJECTPOOLING_H
#include <vector>
#include <string>
#include <map>
#include <memory>

// If you want the objects to have physics you
// will need to iterate over the pool and assign them
// as the copy constuctor in gameobject does not copy
// that data

template <class T>
class ObjectPool
{
	//Here is the pool
	std::vector<std::unique_ptr<T>> m_Pool;

	//Inactive objects
	std::vector<T *> m_InactiveObjects;
	//Active object list to update or whatever.
	std::vector<T *> m_ActiveObjects;
public:
	//Use this if objects have a default constructor
	ObjectPool<T>(unsigned int numInPool, bool initActive);

	ObjectPool<T>(std::vector<T*> poolContents, bool initActive);

	//Use this if you need the objects to be instantiated.
	//Note: object may need deep copy constructor.
	ObjectPool<T>(T instance, int numInPool, bool initActive);

	virtual ~ObjectPool<T>();

	//Functions

	virtual std::vector<T*>* GetActiveObjects();

	virtual std::vector<T*>* DisableActiveObject(T * object);

	virtual std::vector<std::unique_ptr<T>>* GetMasterPool();

	virtual void ClearActivePool();

	//Returns pointer to added object
	virtual T* AddObjectToActive(); 
};

#endif // !__OBJECTPOOLING_H

template<class T>
inline ObjectPool<T>::ObjectPool(unsigned int numInPool, bool initActive)
{
	for (unsigned int i = 0; i < numInPool; i++)
	{
		m_Pool.push_back(std::unique_ptr<T>(new T()));
		if (initActive)
		{
			m_ActiveObjects.push_back(m_Pool.back().get());
		}

		else
		{
			m_InactiveObjects.push_back(m_Pool.back().get());
		}
	}
}

template<class T>
inline ObjectPool<T>::ObjectPool(std::vector<T*> poolContents, bool initActive)
{
	for (unsigned int i = 0; i < poolContents.size(); i++)
	{
		m_Pool.push_back(std::unique_ptr<T>(poolContents[i]));
		if (initActive)
		{
			m_ActiveObjects.push_back(poolContents[i]);
		}
		else
		{
			m_InactiveObjects.push_back(poolContents[i]);
		}
	}
}

template<class T>
inline ObjectPool<T>::ObjectPool(T instance, int numInPool, bool initActive)
{
	for (int i = 0; i < numInPool; i++)
	{
		m_Pool.push_back(std::unique_ptr<T>(new T(instance)));
		if (initActive)
		{
			m_ActiveObjects.push_back(m_Pool.back().get());
		}

		else
		{
			m_InactiveObjects.push_back(m_Pool.back().get());
		}
	}
}

template<class T>
inline ObjectPool<T>::~ObjectPool()
{
	m_ActiveObjects.clear();
	m_InactiveObjects.clear();

	//No need to free memory since the pointers are managed
	m_Pool.clear();
}

template<class T>
inline std::vector<T*>* ObjectPool<T>::GetActiveObjects()
{
	return &m_ActiveObjects;
}

template<class T>
inline std::vector<T*>* ObjectPool<T>::DisableActiveObject(T * object)
{
	std::vector<T*>::iterator foundObject = std::find(m_ActiveObjects.begin(), m_ActiveObjects.end(), object);
	if (foundObject == m_ActiveObjects.end())
	{
		OutputMessage("Warning: tried to remove item not in active pool\n");
	}
	else
	{
		m_InactiveObjects.push_back(object);

		m_ActiveObjects.erase(std::find(m_ActiveObjects.begin(), m_ActiveObjects.end(), object));
	}
	return &m_ActiveObjects;
}

template<class T>
inline std::vector<std::unique_ptr<T>>* ObjectPool<T>::GetMasterPool()
{
	return &m_Pool;
}

template<class T>
inline void ObjectPool<T>::ClearActivePool()
{
	while (m_ActiveObjects.size() > 0)
	{
		//m_InactiveObjects.push_back(m_ActiveObjects.back());
		//m_ActiveObjects.pop_back();
		DisableActiveObject(m_ActiveObjects.back());
	}
}

template<class T>
inline T* ObjectPool<T>::AddObjectToActive()
{
	if (m_InactiveObjects.size() == 0)
	{
		return nullptr;
	}

	T* addedObject = m_InactiveObjects.back();
	m_ActiveObjects.push_back(addedObject);
	m_InactiveObjects.pop_back();
	return addedObject;
}