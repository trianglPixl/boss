#include "pch.h"
#include "SceneManager.h"

using namespace std;

mutex SceneManager::m_mutex;
std::unique_ptr<SceneManager> SceneManager::m_manager = nullptr;

SceneManager * SceneManager::GetInstance()
{
	if (m_manager == nullptr)
	{
		lock_guard<mutex> lock(m_mutex);
		if (m_manager == nullptr)
		{
			m_manager.reset(new SceneManager());
		}
	}

	return m_manager.get();
}

Scene * SceneManager::GetScene(std::string name)
{
	try
	{
		return m_scenes.at(name).get();
	}

	catch (std::out_of_range & e)
	{
		OutputMessage(e.what());
		return nullptr;
	}

	catch(exception & e)	
	{
		//Somehow another error occured.
		OutputMessage(e.what());
		assert(false);
	}
}

void SceneManager::RemoveSceneFromPool(std::string name)
{
	m_scenes.erase(name);
}

void SceneManager::MakeNewSceneInPool(std::string name, Scene * scene)
{
	m_scenes[name].reset(scene);
}

void SceneManager::PurgeAllScenesFromPool()
{
	m_scenes.clear();
}

void SceneManager::PushSceneToStack(std::string name, bool freezePrevious)
{
	Scene * scene = GetScene(name);

	if (freezePrevious)
		m_stack.top()->SetFrozen(true);
	

	if (scene != nullptr)
		m_stack.push(scene);
}

void SceneManager::PushSceneToStackAndSetNewParent(std::string name, bool freezePrevious)
{
	Scene * scene = GetScene(name);
	
	if (freezePrevious)
		m_stack.top()->SetFrozen(true);
	
	scene->SetParentScene(m_stack.top());

	if (scene != nullptr)
		m_stack.push(scene);
}

void SceneManager::PopStack()
{
	if (!m_stack.empty())
		m_stack.pop();
}

void SceneManager::ClearStack()
{
	for (unsigned int i = 0; i < m_stack.size(); i++)
		m_stack.pop();
}

void SceneManager::RebuildScene(std::string sceneName, Scene * rS)
{
	m_scenes.at(sceneName).reset(rS);
}

void SceneManager::RestartScene(std::string sceneName)
{
	m_scenes.at(sceneName)->Restart();
}

std::string SceneManager::GetCurrentSceneName()
{
	if (!m_stack.empty())
		return m_stack.top()->GetName();
	else
		return "";
}

bool SceneManager::IsStackValid()
{
	return !m_stack.empty();
}

void SceneManager::OnSurfaceChanged(unsigned int width, unsigned int height)
{
	if (!m_stack.empty())
		m_stack.top()->OnSurfaceChanged(width, height);
}

void SceneManager::LoadContent()
{
	if (!m_stack.empty())
		m_stack.top()->LoadContent();
}

void SceneManager::OnEvent(Event * pEvent)
{
	if (!m_stack.empty())
		m_stack.top()->OnEvent(pEvent);
}

void SceneManager::Update(float deltatime)
{
	if (!m_stack.empty())
		m_stack.top()->Update(deltatime);
}

void SceneManager::Draw()
{
	if (!m_stack.empty())
		m_stack.top()->Draw();
}

bool SceneManager::IsDone()
{
	if (!m_stack.empty())
		return m_stack.top()->m_isDone;

	return false;
}

void SceneManager::UpdateSceneCamerasAR(float ar)
{
	if (!m_stack.empty())
		m_stack.top()->UpdateSceneCamerasAR(ar);
}

GameObject * SceneManager::GetGameObject(const char * name)
{
	if (!m_stack.empty())
		return m_stack.top()->GetGameObject(name);
	else
		return nullptr;
}