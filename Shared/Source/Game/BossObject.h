#pragma once
#include "pch.h"
class BossObject : public GameObject
{
public:
	BossObject(Scene* pScene, std::string name, vec3 pos, vec3 rot,
		vec3 scale, Mesh* pMesh, ShaderProgram* pShader, GLuint texture, GLuint textureNm = 0);
	~BossObject();

	void Update(float deltatime);

    void Hurt(float damage);
    void SetDamageTimer(float time);
	void SetDamageMaterials(Material vulnerableMaterial, Material invulnerableMaterial);

	void SetHealth(float health);

	void SetLaserLight(Light* pLight);
	void SetLaserJitterTweenFunction(TweenFunc tween);

    void Shoot(float deltatime);
protected:
	float m_RotationSpeed;

    float m_Health;
    float m_DamageTimer;

	Material m_VulnerableMaterial;
	Material m_InvulnerableMaterial;

	Light* m_pLaserLight;

	Mix_Chunk* m_pWeakHitSound;
	Mix_Chunk* m_pHitSound;
	Mix_Chunk* m_pDeathSound;

	TweenFunc m_pLaserLightJitter;
};