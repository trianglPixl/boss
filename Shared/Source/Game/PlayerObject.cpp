#include "pch.h"
#include "GameObjects\GameObject.h"
#include <Box2D\Box2D.h>
#include "PlayerObject.h"

PlayerObject::PlayerObject(Scene* pScene, std::string name, vec3 pos, vec3 rot, vec3 scale, Mesh* pMesh, ShaderProgram* pShader, GLuint texture)
: GameObject( pScene, name, pos, rot, scale, pMesh, pShader, texture )
{
    m_pController = 0;
}

PlayerObject::~PlayerObject()
{
}

void PlayerObject::SetController(PlayerController* pController)
{
    m_pController = pController;
}

void PlayerObject::Update(float deltatime)
{
	static float delay = 0.0f;
	delay += deltatime;

    if( m_pController == 0 || delay < 0.5f)
        return;

    GameObject::Update( deltatime );

	if (Dead == false && m_isDisabled == false)
	{
		//User pressed space
		if (m_pController->IsButtonPressed(PCB_SPACE))
		{
			m_chargingJump = true;

			//Making sure player jump is reset
			if (m_jumpTimer == 0)
			{
				if (m_pArrow != nullptr)
					m_pArrow->SetPosition(m_Position);

				//Setting angle based on wall side
				if (m_isLeft)
					m_jumpRot = 0.0f;
				else
					m_jumpRot = 180.0f;
			}
		}

		//User released space
		else
			m_chargingJump = false;

		//Add to time if charging jump
		if (m_chargingJump)
		{
			if (m_pArrow != nullptr)
				m_pArrow->SetRotation(Vector3(0.0f, 0.0f, -m_jumpRot + 90.0f));

			m_jumpTimer += deltatime;

			//Apply angle change while charging 
			if (m_isLeft)
			{
				if (m_jumpRot > 80.0f)
					m_reachedThreshold = true;

				if(m_reachedThreshold)
					m_jumpRot -= deltatime * m_AngleMult;
				else
					m_jumpRot += deltatime * m_AngleMult;

				if (m_jumpRot <= 0.0f)
				{
					m_jumpRot = 10.0f;
					m_chargingJump = false;
				}
			}

			else
			{
				if (m_jumpRot < 110.0f)
					m_reachedThreshold = true;

				if (m_reachedThreshold)
					m_jumpRot += deltatime * m_AngleMult;
				else
					m_jumpRot -= deltatime * m_AngleMult;

				if (m_jumpRot >= 180.0f)
				{
					m_jumpRot = 170.0f;
					m_chargingJump = false;
				}
			}
		}

		//Jumping
		if(!m_chargingJump && m_jumpTimer > 0)
		{
			if (m_pArrow != nullptr)
				m_pArrow->SetPosition(vec3(-1000.0f, -1000.0f, 0.0f));


			float angleInRad = (m_jumpRot * PI) / 180.0f;

			//Apply impulse
			ApplyLinearImpulse(b2Vec2(cosf(angleInRad) * m_Force, sinf(angleInRad) * m_Force), b2Vec2(0.0f, 0.0f), true);
			GetPhysicsBody()->SetGravityScale(1.0f);

			m_jumpTimer = 0;
			m_isLeft = !m_isLeft;
			m_reachedThreshold = false;
			m_isDisabled = true;
		}
	}
}

void PlayerObject::HandleWallCollision()
{
	m_isDisabled = false;
	GetPhysicsBody()->SetLinearVelocity(b2Vec2(0.0f, 0.0f));
	GetPhysicsBody()->SetGravityScale(0.0f);
}
