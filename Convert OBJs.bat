@echo off
set models=
set geometry=
FOR /F "tokens=*" %%a in ('dir Data\Models\*.obj /A:-D /b') DO (call :accumulateModels %%a)
FOR /F "tokens=*" %%a in ('dir Data\Geometry\*.obj /A:-D /b') DO (call :accumulateGeometry %%a)
@echo on
OBJConverter.exe -m %models% -g %geometry%
@echo off
GOTO :eof

:accumulateModels
 set models=Data\Models\%1 %models%
 GOTO :eof

:accumulateGeometry
 set geometry=Data\Geometry\%1 %geometry%
 GOTO :eof

:eof