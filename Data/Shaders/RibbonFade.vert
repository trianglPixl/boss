
layout(location = 1) in vec3 a_Position;
layout(location = 2) in vec3 a_Tangent;
layout(location = 3) in float a_WidthOffset;
layout(location = 4) in float a_SpawnTime;

uniform mat4 u_MVP;
uniform vec3 u_CameraPosition;
uniform float u_Width;
uniform float u_Lifetime;
uniform float u_Time;

out vec2 v_UV;
out float v_Life;
out float v_LateralPosition;

void main()
{
	vec3 viewPosition = u_CameraPosition - a_Position;
	vec3 bitangent = normalize(cross(viewPosition, a_Tangent));

    gl_Position = u_MVP * vec4(a_Position + bitangent * a_WidthOffset * u_Width / 2.0, 1.0 );
	v_UV = vec2(0.0, 0.0);
	v_Life = 1.0 - (u_Time - a_SpawnTime) / u_Lifetime;
	v_LateralPosition = a_WidthOffset;
}
