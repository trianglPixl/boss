
layout(location = 1) in vec3 a_Position;
layout(location = 2) in vec3 a_Normal;
layout(location = 3) in vec2 a_UV;
layout(location = 4) in vec4 a_Color;

uniform mat4 u_View;
uniform mat4 u_Model;
uniform mat4 u_Projection;
uniform mat4 u_MVP;

out vec3 v_Normal;
out vec3 v_UV;

void main()
{
	vec4 position = vec4(a_Position, 1);

	position = u_Model * position;

	position.w = 0;

	position = u_View * position;
	position.w = 1;

	position = u_Projection * position;

    gl_Position = position;
	
	v_Normal = (u_MVP * vec4(a_Normal, 0)).xyz;
	
	v_UV = (u_Model * vec4(a_Position, 0)).xyz;
}
