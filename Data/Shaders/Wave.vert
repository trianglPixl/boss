#define MAX_RIPPLES 30
layout(location = 1) in vec3 a_Position;
layout(location = 2) in vec2 a_UV;
layout(location = 3) in vec4 a_Color;

uniform mat4 u_MVP;
uniform mat4 u_WorldMatrix;
uniform float u_Time;

//Uniform for points, setting them manually for testing purposes
uniform vec3 u_RipplePoints[MAX_RIPPLES];
uniform float u_RippleLifetimes[MAX_RIPPLES];
uniform int u_NumFilled;

out vec2 v_UV;
out float v_Height;
out vec3 v_Pos;

const float PI = 3.14159;

const float waveAmplitude = 0.5;

//Consts for Ripple Effect
//const float DistanceModifier = 20;
//const float TimeScale = 2.0;
const float rippleAmplitude = 1.0;
const float rippleRange = 50.0;
const float rippleSize = 3.0;

void main()
{

	// Base waves
	vec3 wavePoint = a_Position;

	float xModifer = wavePoint.x + u_Time;
	float xzmodifier = wavePoint.x + wavePoint.z + u_Time;

	float waveHeight = sin( 0.1 * xzmodifier) + sin(0.2 * xModifer + 1.3) * waveAmplitude;

	// Ripple
	float rippleHeight = 0.0;
	for(int i = 0; i < min(u_NumFilled, MAX_RIPPLES); i++)
	{
		float DiffX = u_RipplePoints[i].x - wavePoint.x;
		float DiffY = u_RipplePoints[i].z - wavePoint.z;

		float dist = sqrt(DiffX * DiffX + DiffY * DiffY);
		float ripplePos = rippleRange * (1.0 - u_RippleLifetimes[i]);
		float rippleDistance = dist - ripplePos;
		
		float height = cos(clamp(rippleDistance / rippleSize, -1.0, 1.5) * PI);

		rippleHeight += height * rippleAmplitude * u_RippleLifetimes[i];
	}

	wavePoint += waveHeight + rippleHeight;

    gl_Position = u_MVP * vec4( wavePoint, 1 );

	v_UV = a_UV;
	v_Pos = wavePoint;
	v_Height = waveHeight / max(u_NumFilled, 1.0) + rippleHeight / 2.0;
}
