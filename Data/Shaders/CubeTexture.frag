precision highp float;
precision highp samplerCube;

uniform samplerCube u_TextureCube;

in vec3 v_Normal;
in vec3 v_UV;

out vec4 fragColor;

void main()
{
	//Read color value from cube texture
	vec4 color = texture( u_TextureCube, v_UV);

	fragColor = color;
}
