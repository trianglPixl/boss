precision highp float;
precision highp samplerCube;

layout(location = 1) in vec3 a_Position;

uniform mat4 u_MVP;

out vec4 v_Position;

void main()
{
	vec4 position = u_MVP * vec4( a_Position, 1.0 );
    gl_Position = position;

	v_Position = position;
}
