precision highp float;
precision highp samplerCube;

uniform float u_Near;
uniform float u_Far;

in vec4 v_Position;

out vec4 fragColor;

void main()
{

	float depth = u_Near + length(v_Position.xyz) / (u_Far - u_Near);

	fragColor = vec4(depth,depth,depth,1);
	return;
}
