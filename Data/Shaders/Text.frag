
uniform sampler2D u_Texture;
uniform vec3 u_TextColor;

in vec2 v_UV;

out vec4 fragColor;

void main()
{
	vec4 color = vec4(u_TextColor, texture2D( u_Texture, v_UV ).w);
	fragColor = color;
}
