uniform sampler2D u_Texture;

in vec2 v_UV;

const float gamma = 2.2;

out vec4 fragColor;

void main()
{
	vec4 color = texture2D(u_Texture, v_UV);
	color.rgb = pow(color.rgb, vec3(1.0 / gamma));
	fragColor = color;
	//fragColor = vec4(0.0, 1.0, 1.0, 1.0);
}
