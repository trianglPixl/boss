uniform sampler2D u_Texture;

in vec2 v_UV;

out vec4 fragColor;

void main()
{
	vec4 color = texture2D(u_Texture, v_UV);
	fragColor = color;
	//fragColor = vec4(0.0, 1.0, 1.0, 1.0);
}
