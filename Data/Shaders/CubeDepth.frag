precision highp float;
precision highp samplerCube;

#include "Data/Shaders/Functions/f_DepthRead.func"

uniform samplerCube u_TextureCube;

in vec3 v_Normal;
in vec3 v_UV;

out vec4 fragColor;

void main()
{
	//Read color value from cube texture
	vec4 color = texture( u_TextureCube, v_UV);

	float linearClamped = GetDistanceFromBuffer(color.r , 0.01, 100);
	
	linearClamped = 0.01 + linearClamped / (100 - 0.01);

	fragColor = vec4(linearClamped, linearClamped, linearClamped, 1.0);
}
