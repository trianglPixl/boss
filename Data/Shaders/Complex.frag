precision highp float;
precision highp samplerCube;
#include "Data/Shaders/Functions/f_Lighting.frag"

uniform sampler2D u_Texture;
uniform mat4	  u_Model;

out vec4 fragColor;

void main()
{
	//Lighting
	vec4 litColor = CalculateLightingFrag(u_Texture, u_Model);


	fragColor = litColor;
}
