precision highp float;
precision highp samplerCube;
#include "Data/Shaders/Functions/f_Lighting.vert"

layout(location = 1) in vec3 a_Position;
layout(location = 2) in vec3 a_Normal;
layout(location = 3) in vec2 a_UV;
layout(location = 4) in vec4 a_Color;
layout(location = 5) in vec3 a_Tangent;

uniform mat4 u_MVP;

void main()
{
	//Lighting
	CalculateLightingVert(a_Position, a_Normal, a_UV, a_Tangent);

	// Store final position in gl_Position
    gl_Position = u_MVP * vec4( a_Position, 1 );
}
