
layout(location = 1) in vec2 a_Position;
layout(location = 2) in vec4 a_Color;

uniform mat4 u_VP;

out vec4 v_Color;

void main()
{
    gl_Position = u_VP * vec4( a_Position, 0, 1 );
	v_Color = a_Color;
}
