#define PI 3.141
//uniform sampler2D u_Texture;
uniform vec4 u_Color;

//in vec3 v_Normal;
in vec2 v_UV;
in float v_Life;
in float v_LateralPosition;

out vec4 fragColor;

void main()
{
	float alpha = min(v_Life * 30.0, 1.0);
	vec4 color = mix(u_Color, vec4(0.2, 0.6, 1.0, 0.0), cos((1.0 - abs(v_LateralPosition)) * PI / 2.0));
	fragColor = vec4(color.rgb, color.a * alpha);
}
