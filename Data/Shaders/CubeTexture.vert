precision highp float;
precision highp samplerCube;

layout(location = 1) in vec3 a_Position;
layout(location = 2) in vec3 a_Normal;
layout(location = 3) in vec2 a_UV;
layout(location = 4) in vec4 a_Color;

uniform mat4 u_MVP;
uniform mat4 u_Model;

out vec3 v_Normal;
out vec3 v_UV;

void main()
{

	// Store final position in gl_Position
    gl_Position = u_MVP * vec4( a_Position, 1 );
	
	v_Normal = (u_MVP * vec4(a_Normal, 0)).xyz;
	
	v_UV = (u_Model * vec4(a_Position, 0)).xyz;
}
