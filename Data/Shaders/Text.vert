
layout(location = 1) in vec3 a_Position;
layout(location = 2) in vec2 a_UV;

uniform mat4 u_MVP;

out vec2 v_UV;

void main()
{
    gl_Position = u_MVP * vec4( a_Position, 1 );

	v_UV = a_UV;
}
