
uniform sampler2D u_Texture;

varying vec2 v_UV;
varying vec4 v_Color;

void main()
{
	vec4 color;
	color = texture2D( u_Texture, v_UV );

	color.r += v_Color.r;
	color.g += v_Color.g;
	color.b += v_Color.b;

	gl_FragColor = color;
}
