
attribute vec3 a_Position;
attribute vec2 a_UV;
attribute vec4 a_Color;

uniform mat4 u_MVP;

varying vec2 v_UV;
varying vec4 v_Color;

void main()
{

	// Store final position in gl_Position
    gl_Position = u_MVP * vec4( a_Position, 1 );

	v_UV = a_UV;
	v_Color = a_Color;
}
