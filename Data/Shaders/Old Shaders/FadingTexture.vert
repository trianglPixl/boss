
attribute vec3 a_Position;
attribute vec3 a_Normal;
attribute vec2 a_UV;
attribute vec4 a_Color;

uniform mat4 u_MVP;

varying vec3 v_Normal;
varying vec2 v_UV;

void main()
{

	// Store final position in gl_Position
    gl_Position = u_MVP * vec4( a_Position, 1 );
	
	v_Normal = (u_MVP * vec4(a_Normal, 0)).xyz;
	v_UV = a_UV;
}
