
uniform sampler2D u_Texture;

varying vec3 v_Normal;
varying vec2 v_UV;

const float brightnessThreshold = 0.3;

void main()
{
	vec3 normal = normalize(v_Normal) + 1.0;
	normal *= 0.5;
	float brightness = (1 - normal.z * 2.0);
	brightness = ceil(brightness * 5.0) / 5.0;
	// create sharp brightness edges
	//brightness *= brightness;
	//if (brightness < brightnessThreshold)
	//{
	//	brightness *= 0.5;
	//}
	//else
	//{
	//	brightness *= 1.2;
	//}
	gl_FragColor = vec4(vec3(brightness), 1.0);
}
