
uniform sampler2D u_Texture;
uniform sampler2D u_NormalMap;

uniform vec3 u_CameraPosition;

uniform mat4 u_Model;
uniform mat4 u_View;

uniform vec4 u_matAmbient;
uniform vec4 u_matSpecular;
uniform float u_matShine;

uniform int u_ActiveLights;

uniform vec4 u_lAmbient[8];
uniform vec4 u_lDiffuse[8];
uniform vec4 u_lSpecular[8];
uniform vec4 u_lPosition[8];
uniform vec3 u_lDirection[8];
uniform float u_lAngle[8];
uniform float u_lAngleCos[8];
uniform float u_lLightRadius[8];
uniform float u_lAttenuationCutoff[8];

varying vec4 v_Position;
varying vec3 v_Normal;
varying vec2 v_UV;
varying mat3 v_TBM;

void main()
{
	vec3 diffuse = vec3(0);
	vec3 specular = vec3(0);
	vec3 pAmb = vec3(0.0);
	vec3 pSpec = vec3(0);
	vec4 mapDiffuse = vec4(0);
	vec3 pDiff = vec3(0);
	vec3 worldPosition = vec3(0);
	vec3 LightPosition = vec3(0);
	vec3 dirToCam	= vec3(0);
	vec3 dirToLight = vec3(0);
	vec4 mapNorm = vec4(0);
	vec3 worldNorm = vec3(0);
	vec3 normal = vec3(0);
	vec3 halfVec = vec3(0);
	float attenval = 0.0;
	float attenuation = 0.0f;

	vec4 convertedNormal = u_Model * vec4(normalize(v_Normal), 0);

	//Calculate normal from map
	mapNorm = texture2D(u_NormalMap, v_UV);
	normal = vec3(mapNorm.x, mapNorm.y, mapNorm.z);
	normal = normalize(normal * 2.0 - 1.0);
	mapNorm = vec4(v_TBM * normal, 0);
	worldNorm = normalize((u_Model * mapNorm).xyz);

	mapDiffuse = texture2D( u_Texture, v_UV );

	vec3 matAmbient = u_matAmbient.rgb * u_matAmbient.a;
	vec3 matSpecular = u_matSpecular.rgb * u_matSpecular.a;

	for(int i = 0; i < u_ActiveLights; i++)
	{	
		//Basic lighting colors
		pAmb += matAmbient * u_lAmbient[i].rgb * u_lAmbient[i].a;
		pSpec = matSpecular * (u_lSpecular[i].rgb * u_lSpecular[i].a);
		pDiff = mapDiffuse.rgb * (u_lDiffuse[i].rgb * u_lDiffuse[i].a);

		//Positions
		worldPosition = (u_Model * v_Position).xyz;
		LightPosition = u_lPosition[i].xyz;

		//Directions
		dirToCam = normalize(u_CameraPosition - worldPosition);
		dirToLight = normalize((LightPosition - worldPosition));


		//Get reflection from incedence and normal
		halfVec = normalize(dirToLight + dirToCam);

		//Distance Values
		float Dx = LightPosition.x - worldPosition.x;
		float Dy = LightPosition.y - worldPosition.y;
		float Dz = LightPosition.z - worldPosition.z;

		float XX = Dx * Dx;
		float YY = Dy * Dy;
		float ZZ = Dz * Dz;

		//Attenuation
		float dist = sqrt(XX + YY + ZZ);
		dist -= u_lLightRadius[i];
		dist -= u_lAttenuationCutoff[i];
		attenval = (( dist  / u_lAttenuationCutoff[i]) + 1.0);
		attenuation = 1.0 / (attenval * attenval);

		attenuation = clamp(attenuation, 0.0, 1.0);


		//PointLight
		if(u_lDirection[i] == vec3(0,0,0))
		{
			vec3 temp = pDiff * (dot(worldNorm, dirToLight) * 1.2);
			diffuse += clamp(temp, 0.0, 1.0) * attenuation;

			specular += max(pSpec * pow(max(dot(worldNorm, halfVec), 0.0), u_matShine), 0.0);
		}

		//Spotlight
		else
		{
			//angle in rad between dir to light and light dir minus cos of light cone
			float angleModifier = u_lAngleCos[i] - acos(dot(dirToLight * -1.0, u_lDirection[i]));

			//HardEdges
			//angleModifier = ceil(max(angleModifier, 0.0));

			//Smooth edges
			angleModifier /= u_lAngleCos[i] * 0.5;
				
			vec3 temp = pDiff * (dot(worldNorm, dirToLight) * 1.2);
			diffuse += max(clamp(temp, 0.0, 1.0) * angleModifier, 0.0) * attenuation;

			specular += max((pSpec * pow(max(dot(worldNorm, halfVec), 0.0), u_matShine)) * angleModifier, 0.0) * attenuation;
		}
	}
	

	gl_FragColor = vec4((pAmb + (diffuse + specular)).rgb, mapDiffuse.a);
}
