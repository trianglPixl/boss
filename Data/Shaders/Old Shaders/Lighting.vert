
attribute vec3 a_Position;
attribute vec3 a_Normal;
attribute vec2 a_UV;
attribute vec4 a_Color;
attribute vec3 a_Tangent;

uniform mat4 u_MVP;

varying vec4 v_Position;

varying vec3 v_Normal;
varying vec2 v_UV;
varying mat3 v_TBM;

void main()
{

	// Store final position in gl_Position
    gl_Position = u_MVP * vec4( a_Position, 1 );
	
	v_Position = vec4( a_Position, 1 );
	v_UV = a_UV;
	v_Normal = a_Normal;
	vec3 BitTangent = cross(a_Normal, a_Tangent);
	v_TBM = mat3(a_Tangent, BitTangent, a_Normal);
}
