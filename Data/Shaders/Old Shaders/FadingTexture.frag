
uniform sampler2D u_Texture;
uniform float u_Time;
uniform float u_StartTime;

varying vec3 v_Normal;
varying vec2 v_UV;

const float brightnessThreshold = 0.3;

void main()
{
	vec3 yellow = vec3(1.0,0.9,0.1);
	float timeDiff = u_Time - u_StartTime;

	vec3 normal = normalize(v_Normal) + 1.0;
	normal *= 0.5;
	float brightness = (1 - normal.z * 2.0);
	brightness = ceil(brightness * 5.0) / 5.0;
	

	gl_FragColor = vec4(vec3(brightness) * yellow, 1.0 - timeDiff / 2.0);
}
