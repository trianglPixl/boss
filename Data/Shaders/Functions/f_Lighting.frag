
uniform sampler2D u_NormalMap;

uniform vec3 u_CameraPosition;

uniform vec4 u_matAmbient;
uniform vec4 u_matSpecular;
uniform float u_matShine;

uniform int u_ActiveLights;

uniform vec4 u_lAmbient[8];
uniform vec4 u_lDiffuse[8];
uniform vec4 u_lSpecular[8];
uniform vec4 u_lPosition[8];
uniform vec3 u_lDirection[8];
uniform float u_lAngle[8];
uniform float u_lAngleCos[8];
uniform float u_lLightRadius[8];
uniform float u_lAttenuationCutoff[8];

uniform samplerCube u_lShadowMap[8];

in vec4 v_Position;
in vec3 v_Normal;
in vec2 v_UV;
in mat3 v_TBM;

//Pass in diffuse texture and model matrix, returns new color
vec4 CalculateLightingFrag(sampler2D diffuseTexture, mat4 modelMat)
{
	vec3 diffuse = vec3(0);
	vec3 specular = vec3(0);
	vec3 pAmb = vec3(0.0);
	vec3 pSpec = vec3(0);
	vec4 mapDiffuse = vec4(0);
	vec3 pDiff = vec3(0);
	vec3 worldPosition = vec3(0);
	vec3 LightPosition = vec3(0);
	vec3 dirToCam	= vec3(0);
	vec3 dirToLight = vec3(0);
	vec4 mapNorm = vec4(0);
	vec3 worldNorm = vec3(0);
	vec3 normal = vec3(0);
	vec3 halfVec = vec3(0);
	float attenval = 0.0;
	float attenuation = 0.0;

	vec3 convertedNormal = (modelMat * vec4(normalize(v_Normal), 0)).xyz;

	//Calculate normal from map
	mapNorm = texture2D(u_NormalMap, v_UV);
	normal = vec3(mapNorm.x, mapNorm.y, mapNorm.z);
	normal = normalize(normal * 2.0 - 1.0);
	mapNorm = vec4(v_TBM * normal, 0);
	worldNorm = normalize((modelMat * mapNorm).xyz);

	mapDiffuse = texture2D( diffuseTexture, v_UV );

	vec3 matAmbient = u_matAmbient.rgb * u_matAmbient.a;
	vec3 matSpecular = u_matSpecular.rgb * u_matSpecular.a;

	for(int i = 0; i < u_ActiveLights; i++)
	{	
		//Basic lighting colors
		pAmb += matAmbient * u_lAmbient[i].rgb * u_lAmbient[i].a;
		pSpec = matSpecular * (u_lSpecular[i].rgb * u_lSpecular[i].a);
		pDiff = mapDiffuse.rgb * (u_lDiffuse[i].rgb * u_lDiffuse[i].a);

		//Positions
		worldPosition = (modelMat * v_Position).xyz;
		LightPosition = u_lPosition[i].xyz;

		//Directions
		dirToCam = normalize(u_CameraPosition - worldPosition);
		dirToLight = normalize((LightPosition - worldPosition));


		//Get reflection from incedence and normal
		halfVec = normalize(dirToLight + dirToCam);

		//Distance Values
		float Dx = LightPosition.x - worldPosition.x;
		float Dy = LightPosition.y - worldPosition.y;
		float Dz = LightPosition.z - worldPosition.z;

		float XX = Dx * Dx;
		float YY = Dy * Dy;
		float ZZ = Dz * Dz;

		//Attenuation
		float dist = sqrt(XX + YY + ZZ);
		dist -= u_lLightRadius[i];
		dist -= u_lAttenuationCutoff[i];
		attenval = (( dist  / u_lAttenuationCutoff[i]) + 1.0);
		attenuation = 1.0 / (attenval * attenval);

		attenuation = clamp(attenuation, 0.0, 1.0);


		vec4 shadowVector;

		shadowVector = texture( u_lShadowMap[i],  worldPosition - LightPosition );


		//if(shadowVector.r != 1.0)
		{
			//float shadowBias = clamp( 0.008 * tan( acos(dot (convertedNormal, dirToLight) ) ) , 0.0, 0.02);
			//float shadowBias = max(0.05 * (1.0 - dot(convertedNormal, dirToLight)), 0.004);
			float shadowBias = 0.0075;
			float shadowDistance = shadowVector.r - shadowBias; // - 0.0075// - 0.00725;// - 0.012 //0.0075;

			float far = shadowDistance * ((u_lAttenuationCutoff[i] * 15.0));

			float modDistance = length(worldPosition - LightPosition);

			//Check if far is closer than current pixel position
			if(far < modDistance)
			{
				attenuation = 0.001;
			}
		}
		//PointLight
		if(u_lDirection[i] == vec3(0,0,0))
		{
			vec3 temp = pDiff * (dot(worldNorm, dirToLight));
			diffuse += clamp(temp, 0.0, 1.0) * attenuation;

			specular += max(pSpec * pow(max(dot(worldNorm, halfVec), 0.0), u_matShine), 0.0) * attenuation;
		}

		//Spotlight
		else
		{
			//angle in rad between dir to light and light dir minus cos of light cone
			float angleModifier = u_lAngleCos[i] - acos(dot(dirToLight * -1.0, u_lDirection[i]));

			//HardEdges
			//angleModifier = ceil(max(angleModifier, 0.0));

			//Smooth edges
			angleModifier /= u_lAngleCos[i] * 0.5;
				
			vec3 temp = pDiff * (dot(worldNorm, dirToLight) * 1.2);
			diffuse += max(clamp(temp, 0.0, 1.0) * angleModifier, 0.0) * attenuation;

			specular += max((pSpec * pow(max(dot(worldNorm, halfVec), 0.0), u_matShine)) * angleModifier, 0.0) * attenuation;
		}
	}
	

	return vec4((pAmb + (diffuse + specular)).rgb, mapDiffuse.a);
}
