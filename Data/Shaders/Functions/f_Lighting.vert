out vec4 v_Position;
out vec3 v_Normal;
out vec2 v_UV;
out mat3 v_TBM;

//Pass in values from attributes
void CalculateLightingVert(vec3 position, vec3 normal, vec2 uv, vec3 tangent)
{	
	v_Position = vec4( position, 1 );
	v_UV = uv;
	v_Normal = normal;
	vec3 BiTangent = cross(normal, tangent);
	v_TBM = mat3(tangent, BiTangent, normal);
}
