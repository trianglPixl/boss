const float PI = 3.14159265359;

uniform float u_Time;
uniform sampler2D u_Texture;

in vec2 v_UV;
in float v_Height;
in vec3 v_Pos;

const float offsetFrequency1 = 0.15;
const float offsetFrequency2 = 0.2;

const float offsetAmplitude1 = 0.2;
const float offsetAmplitude2 = 0.1;

const float stretchAmplitude1 = 0.3;
const float stretchAmplitude2 = 0.3;

//const vec3 waveTint = vec3(0.1, 0.2, 0.8);
const vec3 waveTint = vec3(0.25, 0.0, 0.4);

out vec4 fragColor;

void main()
{
	vec2 uv1 = v_UV;
	vec2 uv2 = v_UV;

	uv1.x += sin((u_Time * offsetFrequency1 + uv1.y * stretchAmplitude1) * PI) * offsetAmplitude1;

	uv1 += vec2(sin(v_UV.y * 4.0 + u_Time * PI * 0.2), cos(v_UV.x * 6.0 + u_Time * PI * 0.2)) * 0.05;

	uv2.y += sin((u_Time * offsetFrequency2 + uv2.x * stretchAmplitude2) * PI) * offsetAmplitude2;

	uv2 += vec2(sin(v_UV.y * 4.0 + u_Time * PI * 0.2 + PI / 4.0), cos(v_UV.x * 6.0 + u_Time * PI * 0.2 + PI / 4.0)) * 0.05;

	vec3 color1 = texture2D( u_Texture, uv1 ).rgb;

	vec3 color2 = ((1.0 - texture2D( u_Texture, uv2 ).rgb * 0.3) * waveTint);

	float brightness = (v_Height + 3.0) * 0.25;
	//brightness = clamp(brightness, 0.0, 1.0);
	//brightness = -2.0 / (brightness + 1) + 2;
	// brightness = clamp(brightness, 0.6, 1.0);
	// brightness *= 1.5;

	brightness += 0.5;
	if (brightness < 0.0)
	{
		brightness = 100000.0;
	}

	vec3 color = color2 - (color1 * 0.6 * waveTint);

	fragColor = vec4(color * brightness, 1.0);
}