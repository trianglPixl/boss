
uniform samplerCube u_TextureCube;

in vec3 v_Normal;
in vec3 v_UV;

out vec4 fragColor;

void main()
{
	fragColor = texture( u_TextureCube, v_UV );
}
