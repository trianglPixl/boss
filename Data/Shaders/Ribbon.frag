
//uniform sampler2D u_Texture;
uniform vec4 u_Color;

//in vec3 v_Normal;
in vec2 v_UV;

out vec4 fragColor;

void main()
{
	fragColor = u_Color;
}
