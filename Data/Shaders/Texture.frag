
uniform sampler2D u_Texture;

in vec3 v_Normal;
in vec2 v_UV;

out vec4 fragColor;

void main()
{
	fragColor = texture2D( u_Texture, v_UV );
}
