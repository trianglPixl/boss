Game info:
Shoot the thing in the middle until it dies. It spins toward you with a laser and during hit invulnerability, it spins really quickly,
so watch out. You die in 4 hits. The boss dies in 10.

Menu controls:
Arrow keys: Navigate
Enter: Confirm selection

In-game controls:
Esc: Pause
WASD: Move
Space: Jump (not useful, haha)
Left click: Fire bolt


Marks breakdown:

--Game programming--
Box2D: We implemented Bullet with a mixture of raycasting and regular ol' collision checking.
Audio: Some events (bullets landing, damage being dealt, etc) make noise.
Saved game: High scores on main menu (time is score)
Camera: First-person camera that follows the player controller; some things use the camera forward vector for calculations
Scene management: Menus, everything is preloaded, flexible pause system
Tweening: The light that follows the spot that the laser is coming out of bobs forward and backward using
	a tweening function (settable using a pointer), causing the shadows in the scene to shift
Polish: Bullet is functional and those bolts don't tunnel. That was a pain to code.

--Graphics--
Lighting: Lights are placed around the scene with different colours and are contributing to ambient conditions
	- Specular is demonstrated by the boss, which switches to a glossier material when invulnerable
Rendering to texture: Shadow cubemaps are drawn and sampled later
Cubemaps: Shadow cubemaps are sampled for dynamic shadows
Polish: Fancy lighting. Laser graphic is powered by a ribbon system that takes advantage of the vertex shader to billboard a triangle strip.