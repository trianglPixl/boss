del /F /Q /S /AH ipch\*
rmdir /Q /S ipch

del /F /Q /S /AH packages\*
rmdir /Q /S packages

del /F /Q /S /AH Output\*
rmdir /Q /S Output

rem del /F /Q /S /AH .vs\*
rem rmdir /Q /S .vs

del /F /Q /S /AH Debug\*
rmdir /Q /S Debug

del /F /Q /S /AH Libraries\Bullet\Projects\BulletCollision.dir\*
rmdir /Q /S Libraries\Bullet\Projects\BulletCollision.dir

del /F /Q /S /AH Libraries\Bullet\Projects\BulletDynamics.dir\*
rmdir /Q /S Libraries\Bullet\Projects\BulletDynamics.dir

del /F /Q /S /AH Libraries\Bullet\Projects\LinearMath.dir\*
rmdir /Q /S Libraries\Bullet\Projects\LinearMath.dir

del /F /Q /S /AH Libraries\Bullet\BulletCollisionOutput\*
rmdir /Q /S Libraries\Bullet\BulletCollisionOutput

del /F /Q /S /AH Libraries\Bullet\BulletDynamicsOutput\*
rmdir /Q /S Libraries\Bullet\BulletDynamicsOutput

del /F /Q /S /AH Libraries\Bullet\LinearMathOutput\*
rmdir /Q /S Libraries\Bullet\LinearMathOutput

del /F /Q /S /AH Platforms\Android-Application\ARM
rmdir /Q /S Platforms\Android-Application\ARM

del /F /Q /S /AH Platforms\Android-Application\ARM64
rmdir /Q /S Platforms\Android-Application\ARM64

del /F /Q /S /AH Platforms\Android-Application\x64
rmdir /Q /S Platforms\Android-Application\x64

del /F /Q /S /AH Platforms\Android-Application\x86
rmdir /Q /S Platforms\Android-Application\x86

del /F /Q *.db
