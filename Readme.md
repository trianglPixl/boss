BOSS TEST
===

I've been thinking of adding features to the renderer used in the class OpenGL engine so I've transferred the project to a personal Git repo. So far, I've just been writing experimental _"just get it working"_ code since I'm lazy and it's a personal project so far.

New features (status):
---
  - Postprocessing (rudimentary, just to implement gamma correction as a post effect)
  - Gamma correction (basically works, but lighting is broken)